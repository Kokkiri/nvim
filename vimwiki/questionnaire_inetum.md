Quelle est la probabilité que vous recommandiez Inetum comme employeur ?

J'ai l'impression que mes opinions sont prises en compte au travail.

Dans l'ensemble, êtes-vous satisfait de travailler chez Inetum ?

Au travail, je sais ce que l'on attend de moi.

Quelle est la probabilité que vous restiez chez Inetum si une autre organisation vous offrait le même poste ?

Mon manager m'apporte le soutien dont j'ai besoin pour faire mon travail.

Mon manager m'encourage activement et soutient mon développement.

Je vois comment mon travail contribue à des résultats positifs pour les autres (par exemple, les clients ou les collègues).

Mes collègues sont prêts à s'entraider pour le travail en cas de besoin.

Si je fais du bon travail, je sais qu'il sera reconnu.

Un parcours s'offre à moi pour faire avancer ma carrière dans notre organisation.

Je suis satisfait(e) des efforts réalisés par Inetum en matière de diversité et d'inclusion (par exemple en termes de genre, d'ethnicité, de handicap,...).

Je pense que Inetum est une entreprise qui se caractérise par la diversité (par exemple en termes de genre, d'ethnicité, de handicap,...).

Chez Inetum, les personnes de tous horizons sont acceptées telles qu'elles sont.

La transformation et le changement organisationnels sont bien gérés chez Inetum.

Je pense que les activités de transformation et de changement planifiées chez Inetum contribuent globalement à la réussite future de l'organisation.

Les raisons de la transformation et du changement organisationnels sont bien communiquées.

On m'encourage à partager mon point de vue sur les activités de transformation et de changement mises en œuvre dans l'organisation.

PMOM (objectifs d'équipe) m'aide à contribuer efficacement aux résultats de l'organisation.

Je constate au quotidien que les nouvelles valeurs d'Inetum sont appliquées.

Mon manager offre une vision claire et une direction pendant la transformation.

Mon manager renforce les valeurs d'Inetum dans ses actions.

Mon manager crée un environnement de confiance et d'ouverture.

Je vois que Inetum Way et les nouvelles valeurs ont été intégrées dans mon équipe, grâce au soutien de mon manager.

Mon manager me tient informé(e) de l'état des transformations organisationnelles et des activités de changement.

Qu'aimeriez-vous voir amélioré dans votre environnement de travail au quotidien ? choix possible: (commentaire ou ignorer la question)

Y a-t-il autre chose que vous aimeriez partager et que vous jugez important pour votre expérience en tant qu’employé ?

le questionnaire est dit anonyme.
Je passe par un site sur lequel je suis connecté avec mon compte.
Il y a un id dans le lien du bouton "répondre au questionnaire"
Si l'id est différent pour chaque utilisateur, c'est que le questionnaire n'est pas anonyme.
