Ecorché
[muscle](https://sketchfab.com/3d-models/ecorche-male-musclenames-anatomy-33162ec759e04d2985dbbdf4ec908d66)

Mouvement
[Bodies in Motion](https://www.bodiesinmotion.photo/)

## POSEMANIAC
[pose 622](https://www.posemaniacs.com/fr/tools/viewer/poses/01H3PZPM9WQWY2BSVB18WVPATE?id=0000622#00002)
[pose 722](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCR7V7BW25Y39BSRZQW8GTQH?id=0000722#00002)
[pose 724](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCWPB922GAG7K65GY8TZSPN4?id=0000724#00001)
[pose 761](https://www.posemaniacs.com/fr/tools/viewer/poses/01HBSZ5GNC7MDB41TG7QN9QW0Z)
[pose 762](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCWR16DD4GSHJA4M7SXVHH3Q?id=0000762#00002)
[pose 763](https://www.posemaniacs.com/fr/tools/viewer/poses/01HBSZ6ZY60HTCMGM3FH60XFCR)
[pose 767](https://www.posemaniacs.com/fr/tools/viewer/poses/01HBSZ7PTHWYC3DWBX4S5QNZNK)
[pose 775](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCWR2FGZRYJMB1B7Q949VC1Y)
[pose 777](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCWR2VH606M9RDC2CYAK4VSG)
[pose 778](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCWR30JRGQKX3V61VY3QV7N6?id=0000778#00001)
[pose 780](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCWR3A8BCESAQBS0XJPT0N6Y?id=0000780#00002)
[pose 782](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCWR3GA0AE2620SNCTPZ5CB1?id=0000782#00002)
[pose 785](https://www.posemaniacs.com/fr/tools/viewer/poses/01HCWR3ZXA1AVT0AT83GFQB3FC?id=0000785#00001)
[pose 796](https://www.posemaniacs.com/fr/tools/viewer/poses/01HDRJFK5N4AX67ZDFDYN41X83?id=0000796#00002)

Mike Place - asian chix (pinterest)

### Otome game
[pose 004](https://www.posemaniacs.com/tools/viewer/poses/01GBVYFVXHBX1KKW2XTEFECQHZ) debout simple
[pose 012](https://www.posemaniacs.com/tools/viewer/poses/01GBVYF8EAY1WNG1CQT2NRSJF0) heroïne
[pose 024](https://www.posemaniacs.com/tools/viewer/poses/01GBVYGPV0FDWBDXMVZ9RZDP07) debout simple
[pose 057](https://www.posemaniacs.com/tools/viewer/poses/01GBVYEZRB3A7P4TQ2Y2BGPVWE) bras croisé
[pose 684](https://www.posemaniacs.com/tools/viewer/poses/01H5J63DMRHT445S83VBHDAH5J) main sur baton posé au sol
[pose 844](https://www.posemaniacs.com/tools/viewer/poses/01HGYNYV9QHSTT1QFJSCA4K85K) bras croisé
[pose 845](https://www.posemaniacs.com/fr/tools/viewer/poses/01HRXTNBZKFDN3VQVXG5RD3FKC) main oreille, main hanche
[pose 867](https://www.posemaniacs.com/tools/viewer/poses/01HJJH7WM1EV32WFE76S30QCBM) avant bras levé (Orion)
[pose 877](https://www.posemaniacs.com/fr/tools/viewer/poses/01HJJKEZ4FJG8TK9DANX0997BA) marche
[pose 881](https://www.posemaniacs.com/tools/viewer/poses/01HJJPESD5BS31G1202JJKRXSH) retourner
[pose 892](https://www.posemaniacs.com/tools/viewer/poses/01HM3XE9X4YG0CXMGF7FYW8HJ3) bras sous le coude
[pose 896](https://www.posemaniacs.com/fr/tools/viewer/poses/01HM3XNEAK0FFHTPJQHKDSYENN) bras ouvert
[pose 956](https://www.posemaniacs.com/tools/viewer/poses/01HRXW4W36WXR29DZ6EGZ8K1YB) main hanche, bassin penché, jambe tendu
[pose 958](https://www.posemaniacs.com/tools/viewer/poses/01HSGB7T3HA23XASFKT3GVKS74) bras relevé, main pendante, main derrière la fesse
[pose 977](https://www.posemaniacs.com/tools/viewer/poses/01HTCSR42C9WMMHABTENG91HWD) Gabriel
[pose 983](https://www.posemaniacs.com/tools/viewer/poses/01HTCTF64YTB3VJPD4WCPVDDAN) Magie Walsh
[pose 992](https://www.posemaniacs.com/fr/tools/viewer/poses/01HTVRP93CT8D9MVG08KBHG9TC) main sous le coude, main sous le menton
[pose 100004](https://www.posemaniacs.com/tools/viewer/poses/01GCDF1GTTHZBMYMYDMRKPFCZ7) bras croisé adossé

## SKELETON MODEL
[skeleton parts](https://www.turbosquid.com/fr/3d-models/female-skeleton-human-skull-3d-model/1043196)
[squelette en résine](https://fr.aliexpress.com/w/wholesale-human-body-skeleton-model.html)
[posetrainer](https://pose-trainer.com/)

## AI IMAGE GENERATOR
https://perchance.org/ai-text-to-image-generator

