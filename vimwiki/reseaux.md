[adresse IP et masques de sous-réseaux](https://www.youtube.com/watch?v=RnpSaDSSjR4)

**outils**
filius
Gns3
prometheus ( monitoring )
# LES TYPES DE RÉSEAUX
#### Common Terminology

|Network Type|Definition|
|---|---|
|Wide Area Network (WAN)|Internet|
|Local Area Network (LAN)|Internal Networks (Ex: Home or Office)|
|Wireless Local Area Network (WLAN)|Internal Networks accessible over Wi-Fi|
|Virtual Private Network (VPN)|Connects multiple network sites to one `LAN`|

#### WAN

Le réseau WAN (Wide Area Network) est communément appelé `Internet`. Lorsqu'il s'agit d'équipements de réseau, nous avons souvent une adresse WAN et une adresse LAN. L'adresse WAN est l'adresse à laquelle on accède généralement par l'internet. Cela dit, elle n'englobe pas l'internet ; un réseau étendu n'est qu'un grand nombre de réseaux locaux reliés entre eux. De nombreuses grandes entreprises ou agences gouvernementales disposent d'un "réseau étendu interne" (également appelé intranet, réseau aérien, etc.). D'une manière générale, la principale façon d'identifier si le réseau est un WAN est d'utiliser un protocole de routage spécifique au WAN tel que BGP et si le schéma IP utilisé n'est pas conforme à la RFC 1918 (10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16).

#### LAN / WLAN

Les réseaux locaux (LAN) et les réseaux locaux sans fil (WLAN) attribuent généralement des adresses IP destinées à un usage local (RFC 1918, 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16). Dans certains cas, comme dans certaines universités ou certains hôtels, une adresse IP routable (internet) peut vous être attribuée lorsque vous rejoignez leur réseau local, mais cela est beaucoup moins courant. Il n'y a aucune différence entre un LAN et un WLAN, si ce n'est que les WLAN introduisent la possibilité de transmettre des données sans câbles. Il s'agit principalement d'une désignation de sécurité.

#### VPN

Il existe trois principaux types de `Virtual Private Networks` (`VPN`), mais tous trois ont le même objectif : donner à l'utilisateur l'impression d'être branché sur un autre réseau.

###### Site-To-Site VPN

Le client et le serveur sont des dispositifs de réseau, généralement des `routeurs` ou des `pare-feux`, et partagent des plages entières de réseau. Cette méthode est le plus souvent utilisée pour relier des réseaux d'entreprise sur Internet, ce qui permet à plusieurs sites de communiquer sur Internet comme s'il s'agissait d'un réseau local.

###### Remote Access VPN

Cela implique que l'ordinateur du client crée une interface virtuelle qui se comporte comme s'il était sur le réseau d'un client.Hack The Box utilise `OpenVPN`, qui crée un adaptateur TUN nous permettant d'accéder aux laboratoires. Lors de l'analyse de ces VPN, il est important de prendre en compte la table de routage créée lors de la connexion au VPN. Si le VPN ne crée des routes que pour des réseaux spécifiques (ex : 10.10.10.0/24), il s'agit d'un `Split-Tunnel VPN`, ce qui signifie que la connexion Internet ne sort pas du VPN. Cette solution est idéale pour Hack The Box car elle permet d'accéder au laboratoire sans avoir à se soucier de la confidentialité de la surveillance de votre connexion Internet. Cependant, pour une entreprise, les VPN `split-tunnel` ne sont généralement pas idéaux car si la machine est infectée par un logiciel malveillant, les méthodes de détection basées sur le réseau ne fonctionneront probablement pas car ce trafic sort de l'Internet.

###### SSL VPN

Il s'agit essentiellement d'un VPN qui s'effectue dans notre navigateur web et qui devient de plus en plus courant car les navigateurs web sont de plus en plus capables de faire n'importe quoi. En général, ils diffusent des applications ou des sessions de travail entières dans votre navigateur web.Le Pwnbox de HackTheBox en est un bon exemple.

## Book Terms

|Network Type|Definition|
|---|---|
|Global Area Network (GAN)|Global network (the Internet)|
|Metropolitan Area Network (MAN)|Regional network (multiple LANs)|
|Wireless Personal Area Network (WPAN)|Personal network (Bluetooth)|

#### GAN

Un réseau mondial tel qu'`Internet` est connu sous le nom de `Global Area Network` (`GAN`). Cependant, Internet n'est pas le seul réseau informatique de ce type.Les entreprises actives au niveau international entretiennent également des réseaux isolés qui s'étendent sur plusieurs `WAN` et connectent les ordinateurs de l'entreprise dans le monde entier. Les `GAN` utilisent l'infrastructure en fibres de verre des réseaux étendus et les interconnectent par des câbles sous-marins internationaux ou des transmissions par satellite.

#### MAN

Le `Metropolitan Area Network` (`MAN`) est un réseau de télécommunications à large bande qui relie plusieurs `LAN` géographiquement proches. En règle générale, il s'agit de succursales individuelles d'une entreprise connectées à un `MAN` via des lignes louées.On utilise des routeurs performants et des connexions à haute performance basées sur des fibres de verre, qui permettent un débit de données nettement supérieur à celui de l'Internet. La vitesse de transmission entre deux nœuds distants est comparable à la communication au sein d'un `LAN`.

Les opérateurs de réseaux internationaux fournissent l'infrastructure des `MAN`. Les villes câblées en tant que `Metropolitan Area Network` peuvent être intégrées au niveau supra-régional dans des `Wide Area Networks` (`WAN`) et au niveau international dans des `Global Area Networks` (`GAN`).

#### PAN / WPAN

Les appareils modernes tels que les smartphones, les tablettes, les ordinateurs portables ou les ordinateurs de bureau peuvent être connectés de manière ad hoc pour former un réseau permettant l'échange de données.

Cela peut se faire par câble sous la forme d'un `Personal Area Network` (`PAN`).

La variante sans fil `Wireless Personal Area Network` (`WPAN`) est basée sur les technologies Bluetooth ou Wireless USB.Un `Personal Area Network` établi via Bluetooth est appelé `Piconet`. Les `PAN` et `WPAN` ne s'étendent généralement que sur quelques mètres et ne conviennent donc pas pour connecter des appareils dans des pièces séparées ou même des bâtiments.

Dans le contexte de l'`Internet of Things` (`IoT`), les réseaux `WPAN` sont utilisés pour communiquer ou controler des applications avec de faibles débits de données. Des protocoles tels que Insteon, Z-Wave et ZigBee ont été explicitement conçus pour les maisons intelligentes et la domotique.

# LES TOPOLOGIES DE MISES EN RÉSEAUX
Nous pouvons diviser l'ensemble de la topologie du réseau en trois zones :
#### 1. Connections

|`Wired connections`|`Wireless connections`|
|---|---|
|Coaxial cabling|Wi-Fi|
|Glass fiber cabling|Cellular|
|Twisted-pair cabling|Satellite|
|and others|and others|

---

#### 2. Nodes - Network Interface Controller (NICs)

	Repeaters
	Hubs
	Bridges
	Switches
	Router/Modem
	Gateways
	Firewalls

#### 3. Classifications

	Point-to-Point
	Bus
	Star
	Ring
	Mesh
	Tree
	Hybrid
	Daisy Chain

#### Point-To-Point Topology
![image](https://academy.hackthebox.com/storage/modules/34/redesigned/topo_p2p.png)
Les topologies `point à point` constituent le modèle de base de la téléphonie traditionnelle et ne doivent pas être confondues avec l'architecture `P2P` (`Peer-to-Peer`).

#### Bus Topology

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/topo_bus.png)
Tous les hôtes sont reliés par un support de transmission dans la topologie du bus. Chaque hôte a accès au support de transmission et aux signaux qui y sont transmis. Il n'y a pas de composant central du réseau qui contrôle les processus qui s'y déroulent. Le support de transmission peut être, par exemple, un `câble coaxial`.

Comme le support est partagé avec tous les autres, `un seul hôte peut envoyer des données`, et tous les autres ne peuvent que les recevoir et les évaluer pour voir si elles lui sont destinées.

#### Star Topology

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/topo_star.png)
La topologie en étoile est un composant de réseau qui maintient une connexion avec tous les hôtes. Chaque hôte est connecté au `composant central du réseau` via une liaison distincte. Il s'agit généralement d'un routeur, d'un concentrateur ou d'un commutateur. Ceux-ci gèrent `la fonction de transfert` des paquets de données. Pour ce faire, les paquets de données sont reçus et transmis à leur destination. Le trafic de données sur le composant central du réseau peut être très élevé puisque toutes les données et connexions passent par lui.

#### Ring Topology

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/topo_ring.png)
La topologie `physique` de l'anneau est telle que chaque hôte ou nœud est connecté à l'anneau par deux câbles :

- l'un pour les signaux `entrants` et l'autre pour les signaux sortants.
- l'autre pour les signaux `sortants`.

Cela signifie qu'un câble arrive à chaque hôte et qu'un câble repart. La topologie en anneau ne nécessite généralement pas de composant réseau actif. Le contrôle et l'accès au support de transmission sont régis par un protocole auquel toutes les stations adhèrent.

Une topologie en anneau `logique` est basée sur une topologie physique en étoile, où un distributeur au niveau du nœud simule l'anneau en transmettant d'un port à l'autre.

L'information est transmise dans un sens prédéterminé. Généralement, l'accès au support de transmission se fait de manière séquentielle d'une station à l'autre à l'aide d'un système de récupération à partir de la station centrale ou d'un `token`. Un `token` est un schéma binaire qui traverse continuellement un réseau en anneau dans une direction et qui fonctionne selon le `processus de réclamation des tokens`.

#### Mesh Topology

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/topo_mesh.png)
Dans les réseaux maillés, de nombreux nœuds décident des connexions au niveau `physique` et du routage au niveau `logique`. Par conséquent, les structures maillées n'ont pas de topologie fixe. Il existe deux structures de base à partir du concept de base : la structure `entièrement maillée` et la structure `partiellement maillée`.

Chaque hôte est connecté à tous les autres hôtes du réseau dans une `structure entièrement maillée`. Cela signifie que les hôtes sont maillés les uns avec les autres. Cette technique est principalement utilisée dans les réseaux étendus `WAN` ou les réseaux locaux `MAN` pour garantir une fiabilité et une largeur de bande élevées.

Dans cette configuration, les nœuds importants du réseau, tels que les routeurs, peuvent être mis en réseau les uns avec les autres. Si un routeur tombe en panne, les autres peuvent continuer à fonctionner sans problème et le réseau peut absorber la panne grâce aux nombreuses connexions.

Chaque nœud d'une topologie entièrement maillée possède les mêmes fonctions de routage et connaît les nœuds voisins avec lesquels il peut communiquer en fonction de la proximité de la passerelle du réseau et de la charge de trafic.

Dans la `structure partiellement maillée`, les points d'extrémité ne sont reliés que par une seule connexion. Dans ce type de topologie de réseau, certains nœuds sont connectés à exactement un autre nœud, et d'autres nœuds sont connectés à deux ou plusieurs autres nœuds avec une connexion point à point.

#### Tree Topology

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/topo_tree.png)
La topologie en arbre est une topologie en étoile étendue que les réseaux locaux plus étendus ont dans cette structure. Elle est particulièrement utile lorsque plusieurs topologies sont combinées. Cette topologie est souvent utilisée, par exemple, dans les grands bâtiments d'entreprise.

Il existe à la fois des structures arborescentes logiques, selon l'`arbre de propagation`, et des structures arborescentes physiques. Les réseaux modernes modulaires, basés sur un câblage structuré avec une hiérarchie de concentrateurs, ont également une structure arborescente. Les topologies arborescentes sont également utilisées pour les `réseaux à large bande` et les `réseaux urbains` (`MAN`).

#### Hybrid Topology

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/topo_hybrid.png)
Les réseaux hybrides combinent deux topologies ou plus, de sorte que le réseau résultant ne présente aucune topologie standard. Par exemple, un réseau arborescent peut représenter une topologie hybride dans laquelle des réseaux en étoile sont connectés via des réseaux de bus interconnectés. Toutefois, un réseau arborescent relié à un autre réseau arborescent reste topologiquement un réseau arborescent. Une topologie hybride est toujours créée lorsque `deux topologies de réseau de base différentes` sont interconnectées.

#### Daisy Chain Topology

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/topo_daisy-chain.png)
Dans la topologie en guirlande, plusieurs hôtes sont connectés en plaçant un câble d'un nœud à l'autre.

Étant donné qu'il s'agit d'une chaîne de connexions, on parle également de configuration en guirlande dans laquelle plusieurs composants matériels sont connectés en série. Ce type de réseau est souvent utilisé dans les technologies d'automatisation (`CAN`).

Le chaînage en marguerite est basé sur la disposition physique des nœuds, contrairement aux procédures à tokens, qui sont structurelles mais peuvent être rendues indépendantes de la disposition physiqiue. Le signal est envoyé vers et depuis un composant via ses nœuds précédents jusqu'au système informatique.

# PROXY

On parle de proxy lorsqu'un appareil ou un service se place au milieu d'une connexion et joue le rôle de médiateur. Le `médiateur` est l'élément d'information critique car il signifie que l'appareil au milieu doit être en mesure d'inspecter le contenu du trafic.S'il n'est pas capable de jouer le rôle de `médiateur`, l'appareil est techniquement une `passerelle` et non un proxy.

Pour en revenir à la question précédente, le commun des mortels a une idée erronée de ce qu'est un proxy, car il utilise très probablement un VPN pour masquer sa localisation, ce qui, techniquement, n'est pas un proxy.La plupart des gens pensent que chaque fois qu'une adresse IP change, il s'agit d'un proxy, et dans la plupart des cas, il est probablement préférable de ne pas les corriger car il s'agit d'une idée fausse courante et inoffensive.

Si vous avez du mal à vous en souvenir, sachez que les proxys fonctionnent presque toujours à la couche 7 du modèle `OSI`. Il existe de nombreux types de services proxy, mais les principaux sont les suivants :

    Proxy dédié / Proxy direct
    Proxy inversé
    Proxy transparent

#### PROXY DÉDIÉ / PROXY DE TRANSFERT

Le `Forward Proxy` est ce que la plupart des gens imaginent être un proxy. Il s'agit du cas où un client fait une demande à un ordinateur, et que cet ordinateur exécute la demande.

Par exemple, dans un réseau d'entreprise, les ordinateurs sensibles peuvent ne pas avoir d'accès direct à l'internet.Pour accéder à un site web, ils doivent passer par un proxy (ou filtre web).Cela peut constituer une ligne de défense incroyablement puissante contre les logiciels malveillants, car non seulement ils doivent contourner le filtre web (ce qui est facile), mais ils doivent également connaître le proxy ou utiliser un C2 non traditionnel (un moyen pour les logiciels malveillants de recevoir des informations sur les tâches à accomplir). Si l'organisation n'utilise que `FireFox`, la probabilité d'obtenir un logiciel malveillant sensible au proxy est improbable.

Les navigateurs Web tels qu'Internet Explorer, Edge ou Chrome obéissent tous aux paramètres "Proxy système" par défaut. Si le logiciel malveillant utilise WinSock (API native de Windows), il sera probablement compatible avec le proxy sans code supplémentaire. Firefox n'utilise pas `WinSock` mais `libcurl`, ce qui lui permet d'utiliser le même code sur n'importe quel système d'exploitation. Cela signifie que le logiciel malveillant devrait rechercher Firefox et extraire les paramètres du proxy, ce qu'il est très peu probable qu'il fasse.

Les logiciels malveillants pourraient également utiliser le DNS comme mécanisme C2, mais si une organisation surveille le DNS (ce qui est facilement réalisable à l'aide de `Sysmon`), ce type de trafic devrait être rapidement détecté.

Un autre exemple de proxy de transfert est Burp Suite, car la plupart des gens l'utilisent pour transférer des requêtes HTTP. Cependant, cette application est le couteau suisse des proxy HTTP et peut être configurée pour être un proxy inverse ou transparent !

#### PROXY DE TRANSFERT ( FORWARD PROXY )

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/forward_proxy.png)

#### PROXY INVERSÉ ( REVERSE PROXY )

Comme vous l'avez peut-être deviné, un `reverse proxy` est l'inverse d'un `forward proxy`. Au lieu d'être conçu pour filtrer les requêtes sortantes, il filtre les requêtes entrantes. L'objectif le plus courant d'un proxy inverse est d'écouter une adresse et de la transmettre à un réseau fermé.

De nombreuses organisations utilisent `CloudFlare`, qui dispose d'un réseau robuste capable de résister à la plupart des attaques `DDOS`. En utilisant Cloudflare, les organisations ont un moyen de filtrer la quantité (et le type) de trafic qui est envoyé à leurs serveurs web.

Les testeurs de pénétration configureront des proxys inversés sur les terminaux infectés. Le terminal infecté écoutera sur un port et renverra tout client qui se connecte à ce port vers l'attaquant par le biais du terminal infecté. Cela permet de contourner les pare-feu ou d'échapper à la journalisation. Les organisations peuvent avoir des `systèmes de détection d'intrusion` (`IDS`) qui surveillent les requêtes web externes. Si l'attaquant accède à l'organisation via SSH, un reverse proxy peut envoyer des requêtes web à travers le tunnel SSH et échapper à l'IDS.

Un autre proxy inverse courant est `ModSecurity`, une `Web Application Firewall` (`WAF`). Les Web Application Firewalls inspectent les requêtes web à la recherche de contenu malveillant et bloquent la requête si elle est malveillante. Si vous voulez en savoir plus, nous vous recommandons de lire le [ModSecurity Core Rule Set] (https://owasp.org/www-project-modsecurity-core-rule-set/), car c'est un excellent point de départ. Cloudflare peut également agir en tant que WAF, mais pour ce faire, il faut les laisser décrypter le trafic HTTPS, ce que certaines organisations ne souhaitent pas.

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/reverse_proxy.png)

---

## (Non-) Transparent Proxy

Tous ces services proxy agissent soit de manière `transparente`, soit de manière `non transparente`.

Avec un `proxy transparent`, le client ne connaît pas son existence. Le proxy transparent intercepte les demandes de communication du client vers l'Internet et agit comme une instance de substitution. Pour l'extérieur, le proxy transparent, comme le proxy non transparent, agit comme un partenaire de communication.

S'il s'agit d'un `proxy non transparent`, nous devons être informés de son existence. À cette fin, nous et le logiciel que nous voulons utiliser recevons une configuration spéciale du proxy qui garantit que le trafic vers l'internet est d'abord adressé au proxy. Si cette configuration n'existe pas, nous ne pouvons pas communiquer via le proxy. Toutefois, étant donné que le proxy fournit généralement le seul chemin de communication vers d'autres réseaux, la communication vers l'internet est généralement coupée sans une configuration de proxy correspondante.

# MODÈLES DE MISE EN RÉSEAU ( NETWORKING MODELS )

Deux modèles de réseau décrivent la communication et le transfert de données d'un hôte à l'autre : le modèle `ISO/OSI` et le modèle `TCP/IP`. Il s'agit d'une représentation simplifiée de ce que l'on appelle les `couches`, qui représentent les bits transférés dans un contenu lisible pour nous.

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/net_models4.png)

## LE MODÈLE OSI

Le modèle `OSI`, souvent appelé modèle de couche `ISO/OSI`, est un modèle de référence qui peut être utilisé pour décrire et définir la communication entre les systèmes. Le modèle de référence comporte `sept` couches individuelles, chacune ayant des tâches clairement séparées.

Le terme `OSI` signifie `Open Systems Interconnection`, publié par l'`Union internationale des télécommunications` (`UIT`) et l'`Organisation internationale de normalisation` (`ISO`). Par conséquent, le modèle `OSI` est souvent appelé le modèle de couche `ISO/OSI`.

## LE MODÈLE TCP/IP

`TCP/IP` (`Transmission Control Protocol`/`Internet Protocol`) est un terme générique désignant de nombreux protocoles de réseau. Ces protocoles sont responsables de la commutation et du transport des paquets de données sur l'internet. Internet est entièrement basé sur la famille de protocoles `TCP/IP`. Cependant, `TCP/IP` ne se réfère pas seulement à ces deux protocoles, mais est généralement utilisé comme terme générique pour toute une famille de protocoles.

Par exemple, `ICMP` (`Internet Control Message Protocol`) ou `UDP` (`User Datagram Protocol`) appartiennent à la famille de protocoles. La famille de protocoles fournit les fonctions nécessaires au transport et à la commutation de paquets de données dans un réseau privé ou public.

---

## ISO/OSI vs. TCP/IP

Le `TCP/IP` est un protocole de communication qui permet aux hôtes de se connecter à l'Internet. Il fait référence au `Protocole de contrôle de transmission` utilisé dans et par les applications sur l'Internet. Contrairement à `OSI`, il permet d'alléger les règles à suivre, à condition de respecter les directives générales.

L'interface utilisateur, quant à elle, est une passerelle de communication entre le réseau et les utilisateurs finaux. Le modèle `OSI` est généralement appelé modèle de référence parce qu'il est plus ancien. Il est également connu pour son protocole strict et ses limitations.`TCP/IP` (`Transmission Control Protocol`/`Internet Protocol`) est un terme générique pour de nombreux protocoles de réseau. Ces protocoles sont responsables de la commutation et du transport des paquets de données sur l'internet. Internet est entièrement basé sur la famille de protocoles `TCP/IP`. Cependant, `TCP/IP` ne se réfère pas seulement à ces deux protocoles, mais est généralement utilisé comme terme générique pour toute une famille de protocoles.

Par exemple, `ICMP` (`Internet Control Message Protocol`) ou `UDP` (`User Datagram Protocol`) appartiennent à la famille de protocoles. La famille de protocoles fournit les fonctions nécessaires au transport et à la commutation de paquets de données dans un réseau privé ou public.

## TRANSFERTS DE PAQUETS

Dans un système en couches, les dispositifs d'une couche échangent des données dans un format différent appelé `protocol data unit` (`PDU`). Par exemple, lorsque nous voulons naviguer sur un site web sur notre ordinateur, le logiciel du serveur distant transmet d'abord les données demandées à la couche d'application. Les données sont traitées couche par couche, chaque couche remplissant les fonctions qui lui sont assignées. Les données sont ensuite transférées via la couche physique du réseau jusqu'à ce que le serveur de destination ou un autre appareil les reçoive. Les données sont à nouveau acheminées à travers les couches, chaque couche effectuant les opérations qui lui sont assignées jusqu'à ce que le logiciel destinataire utilise les données.

![image](https://academy.hackthebox.com/storage/modules/34/redesigned/net_models_pdu2.png)

Au cours de la transmission, chaque couche ajoute un `en-tête` à l'`UPD` de la couche supérieure, qui contrôle et identifie le paquet. Ce processus est appelé `encapsulation`. L'en-tête et les données forment ensemble le PDU pour la couche suivante. Le processus se poursuit jusqu'à la `couche physique` ou la `couche réseau`, où les données sont transmises au récepteur. Le récepteur inverse le processus et décompresse les données de chaque couche avec les informations de l'en-tête. L'application utilise ensuite les données. Ce processus se poursuit jusqu'à ce que toutes les données aient été envoyées et reçues.

![image](https://academy.hackthebox.com/storage/modules/34/packet_transfer.png)

Pour nous, testeurs de pénétration, les deux modèles de référence sont utiles. Avec `TCP/IP`, nous pouvons rapidement comprendre comment la connexion entière est établie, et avec `ISO`, nous pouvons la démonter pièce par pièce et l'analyser en détail. C'est souvent le cas lorsque nous pouvons écouter et intercepter un trafic réseau spécifique. Nous devons alors analyser ce trafic en conséquence, ce qui est expliqué plus en détail dans le module `Network Traffic Analysis` (Analyse du Trafic Réseau). Nous devons donc nous familiariser avec les deux modèles de référence, les comprendre et les intérioriser de la meilleure façon possible.

# LE MODÈLE OSI
|Layer|Function|
|---|---|
|`7.Application`|Cette couche contrôle notamment l'entrée et la sortie des données et fournit les fonctions de l'application.|
|`6.Presentation`|La tâche de la couche de présentation consiste à transférer la présentation des données dépendant du système sous une forme indépendante de l'application.|
|`5.Session`|La couche session contrôle la connexion logique entre deux systèmes et empêche, par exemple, les ruptures de connexion ou d'autres problèmes.|
|`4.Transport`|La couche 4 est utilisée pour le contrôle de bout en bout des données transférées. La couche transport peut détecter et éviter les situations de congestion et segmenter les flux de données.|
|`3.Network`|Sur la couche réseau, les connexions sont établies dans les réseaux à commutation de circuits et les paquets de données sont transmis dans les réseaux à commutation de paquets. Les données sont transmises sur l'ensemble du réseau, de l'expéditeur au destinataire.|
|`2.Data Link`|La tâche principale de la couche 2 est de permettre des transmissions fiables et sans erreur sur le support correspondant. À cette fin, les flux binaires de la couche 1 sont divisés en blocs ou en trames.|
|`1.Physical`|Les techniques de transmission utilisées sont, par exemple, les signaux électriques, les signaux optiques ou les ondes électromagnétiques. Au travers de la couche 1, la transmission s'effectue sur des lignes de transmission câblées ou sans fil.|

Les couches `2 à 4` sont `orientées transport`, et les couches `5 à 7` sont `orientées application`.

# LE MODÈLE TCP/IP

Le modèle `TCP/IP` est également un modèle de référence en couches, souvent appelé `suite de protocoles Internet`. Le terme `TCP/IP` désigne les deux protocoles `Transmission Control Protocol` (`TCP`) et `Internet Protocol` (`IP`). Le protocole `IP` se situe dans la `couche réseau` (`couche 3`) et le protocole `TCP` dans la `couche transport` (`couche 4`) du modèle `OSI`.

Avec `TCP/IP`, chaque application peut transférer et échanger des données sur n'importe quel réseau, quel que soit l'endroit où se trouve le récepteur. Le protocole `IP` garantit que le paquet de données atteint sa destination, tandis que le protocole `TCP` contrôle le transfert de données et assure la connexion entre le flux de données et l'application. La principale différence entre `TCP/IP` et `OSI` est le nombre de couches, dont certaines ont été combinées.

![[Pasted image 20230801104457.png]]

|**Layer**|**Function**|
|---|---|
|`4.Application`|La couche application permet aux applications d'accéder aux services des autres couches et définit les protocoles utilisés par les applications pour échanger des données.|
|`3.Transport`|La couche transport est chargée de fournir des services de session (TCP) et de datagramme (UDP) à la couche application.|
|`2.Internet`|La couche Internet est responsable de l'adressage des hôtes, des fonctions de conditionnement et de routage.|
|`1.Link`|La couche liaison est chargée de placer les paquets TCP/IP sur le support du réseau et de recevoir les paquets correspondants du support du réseau. Le TCP/IP est conçu pour fonctionner indépendamment de la méthode d'accès au réseau, du format de trame et du support.|

Les tâches les plus importantes du `TCP/IP` sont:

|**Task**|**Protocol**|**Description**|
|---|---|---|
|`Logical Addressing`|`IP`|En raison du grand nombre d'hôtes dans différents réseaux, il est nécessaire de structurer la topologie du réseau et l'adressage logique. Dans le cadre du TCP/IP, le protocole IP prend en charge l'adressage logique des réseaux et des nœuds. Les paquets de données n'atteignent que le réseau où ils sont censés se trouver. Les méthodes pour y parvenir sont les "classes de réseau", le "sous-réseau" et le "CIDR".|
|`Routing`|`IP`|Pour chaque paquet de données, le nœud suivant est déterminé dans chaque nœud sur le chemin entre l'expéditeur et le destinataire. De cette manière, un paquet de données est acheminé vers son destinataire, même si sa localisation est inconnue de l'expéditeur.|
|`Error & Control Flow`|`TCP`|L'expéditeur et le destinataire sont fréquemment en contact l'un avec l'autre par le biais d'une connexion virtuelle. C'est pourquoi des messages de contrôle sont envoyés en permanence pour vérifier si la connexion est toujours établie.|
|`Application Support`|`TCP`|Les ports TCP et UDP constituent une abstraction logicielle permettant de distinguer les applications spécifiques et leurs liens de communication.|
|`Name Resolution`|`DNS`|Le DNS assure la résolution des noms par le biais de noms de domaine entièrement qualifiés (FQDN) dans les adresses IP, ce qui nous permet d'atteindre l'hôte désiré avec le nom spécifié sur l'internet.

# COUCHE RÉSEAUX
La `couche réseau` (`couche 3`) de l'`OSI` contrôle l'échange de paquets de données, car ceux-ci ne peuvent pas être acheminés directement vers le destinataire et doivent donc être dotés de nœuds de routage.

En résumé, il est responsable des fonctions suivantes:

- `Logical Addressing`
- `Routing`

Les protocoles sont définis dans chaque couche de l'`OSI`, et ces protocoles représentent un ensemble de règles pour la communication dans la couche respective. Ils sont transparents par rapport aux protocoles des couches supérieures ou inférieures. Certains protocoles remplissent les tâches de plusieurs couches et s'étendent sur deux couches ou plus. Les protocoles les plus utilisés sur cette couche sont les suivants:

- `IPv4` / `IPv6`
- `IPsec`
- `ICMP`
- `IGMP`
- `RIP`
- `OSPF`

# ADRESSES IP

Each host in the network located can be identified by the so-called `Media Access Control` address (`MAC`). This would allow data exchange within this one network. If the remote host is located in another network, knowledge of the `MAC` address is not enough to establish a connection. Addressing on the Internet is done via the `IPv4` and/or `IPv6` address, which is made up of the `network address` and the `host address`.

- `IPv4` / `IPv6` - describes the unique postal address and district of the receiver's building.
- `MAC` - describes the exact floor and apartment of the receiver.

## IPv4 Structure

The most common method of assigning IP addresses is `IPv4`, which consists of a `32`-bit binary number combined into `4 bytes` consisting of `8`-bit groups (`octets`) ranging from `0-255`. These are converted into more easily readable decimal numbers, separated by dots and represented as dotted-decimal notation.

Thus an IPv4 address can look like this:

|**Notation**|**Presentation**|
|---|---|
|Binary|0111 1111.0000 0000.0000 0000.0000 0001|
|Decimal|127.0.0.1|

Each network interface (network cards, network printers, or routers) is assigned a unique IP address.

The `IPv4` format allows 4,294,967,296 unique addresses. The IP address is divided into a `host part` and a `network part`. The `router` assigns the `host part` of the IP address at home or by an administrator. The respective `network administrator` assigns the `network part`. On the Internet, this is `IANA`, which allocates and manages the unique IPs.

In the past, further classification took place here. The IP network blocks were divided into `classes A - E`. The different classes differed in the host and network shares' respective lengths.

|**`Class`**|**Network Address**|**First Address**|**Last Address**|**Subnetmask**|**CIDR**|**Subnets**|**IPs**|
|---|---|---|---|---|---|---|---|
|`A`|1.0.0.0|1.0.0.1|127.255.255.255|255.0.0.0|/8|127|16,777,214 + 2|
|`B`|128.0.0.0|128.0.0.1|191.255.255.255|255.255.0.0|/16|16,384|65,534 + 2|
|`C`|192.0.0.0|192.0.0.1|223.255.255.255|255.255.255.0|/24|2,097,152|254 + 2|
|`D`|224.0.0.0|224.0.0.1|239.255.255.255|Multicast|Multicast|Multicast|Multicast|
|`E`|240.0.0.0|240.0.0.1|255.255.255.255|reserved|reserved|reserved|reserved|

---

## Subnet Mask

A further separation of these classes into small networks is done with the help of `subnetting`. This separation is done using the `netmasks`, which is as long as an IPv4 address. As with classes, it describes which bit positions within the IP address act as `network part` or `host part`.

|**Class**|**Network Address**|**First Address**|**Last Address**|**`Subnetmask`**|**CIDR**|**Subnets**|**IPs**|
|---|---|---|---|---|---|---|---|
|**A**|1.0.0.0|1.0.0.1|127.255.255.255|`255.0.0.0`|/8|127|16,777,214 + 2|
|**B**|128.0.0.0|128.0.0.1|191.255.255.255|`255.255.0.0`|/16|16,384|65,534 + 2|
|**C**|192.0.0.0|192.0.0.1|223.255.255.255|`255.255.255.0`|/24|2,097,152|254 + 2|
|**D**|224.0.0.0|224.0.0.1|239.255.255.255|`Multicast`|Multicast|Multicast|Multicast|
|**E**|240.0.0.0|240.0.0.1|255.255.255.255|`reserved`|reserved|reserved|reserved|

---

## Network and Gateway Addresses

The `two` additional `IPs` added in the `IPs column` are reserved for the so-called `network address` and the `broadcast address`. Another important role plays the `default gateway`, which is the name for the IPv4 address of the `router` that couples networks and systems with different protocols and manages addresses and transmission methods. It is common for the `default gateway` to be assigned the first or last assignable IPv4 address in a subnet. This is not a technical requirement, but has become a de-facto standard in network environments of all sizes.

|**Class**|**Network Address**|**`First Address`**|**Last Address**|**Subnetmask**|**CIDR**|**Subnets**|**`IPs`**|
|---|---|---|---|---|---|---|---|
|A|1.0.0.0|`1.0.0.1`|127.255.255.255|255.0.0.0|/8|127|16,777,214 `+ 2`|
|B|128.0.0.0|`128.0.0.1`|191.255.255.255|255.255.0.0|/16|16,384|65,534 `+ 2`|
|C|192.0.0.0|`192.0.0.1`|223.255.255.255|255.255.255.0|/24|2,097,152|254 `+ 2`|
|D|224.0.0.0|`224.0.0.1`|239.255.255.255|Multicast|Multicast|Multicast|Multicast|
|E|240.0.0.0|`240.0.0.1`|255.255.255.255|reserved|reserved|reserved|reserved|

---

## Broadcast Address

The `broadcast` IP address's task is to connect all devices in a network with each other. `Broadcast` in a network is a message that is transmitted to all participants of a network and does not require any response. In this way, a host sends a data packet to all other participants of the network simultaneously and, in doing so, communicates its `IP address`, which the receivers can use to contact it. This is the `last IPv4` address that is used for the `broadcast`.

|**Class**|**Network Address**|**First Address**|**`Last Address`**|**Subnetmask**|**CIDR**|**Subnets**|**IPs**|
|---|---|---|---|---|---|---|---|
|A|1.0.0.0|1.0.0.1|`127.255.255.255`|255.0.0.0|/8|127|16,777,214 + 2|
|B|128.0.0.0|128.0.0.1|`191.255.255.255`|255.255.0.0|/16|16,384|65,534 + 2|
|C|192.0.0.0|192.0.0.1|`223.255.255.255`|255.255.255.0|/24|2,097,152|254 + 2|
|D|224.0.0.0|224.0.0.1|`239.255.255.255`|Multicast|Multicast|Multicast|Multicast|
|E|240.0.0.0|240.0.0.1|`255.255.255.255`|reserved|reserved|reserved|reserved|

---

## Binary system

The binary system is a number system that uses only two different states that are represented into two numbers (`0` and `1`) opposite to the decimal-system (0 to 9).

An IPv4 address is divided into 4 octets, as we have already seen. Each `octet` consists of `8 bits`. Each position of a bit in an octet has a specific decimal value. Let's take the following IPv4 address as an example:

- IPv4 Address: `192.168.10.39`

Here is an example of how the `first octet` looks like:

#### 1st Octet - Value: 192

1st Octet - Value: 192

```shell-session
Values:         128  64  32  16  8  4  2  1
Binary:           1   1   0   0  0  0  0  0
```

If we calculate the sum of all these values for each octet where the bit is set to `1`, we get the sum:

|**Octet**|**Values**|**Sum**|
|---|---|---|
|1st|128 + 64 + 0 + 0 + 0 + 0 + 0 + 0|= `192`|
|2nd|128 + 0 + 32 + 0 + 8 + 0 + 0 + 0|= `168`|
|3rd|0 + 0 + 0 + 0 + 8 + 0 + 2 + 0|= `10`|
|4th|0 + 0 + 32 + 0 + 0 + 4 + 2 + 1|= `39`|

The entire representation from binary to decimal would look like this:

#### IPv4 - Binary Notation

IPv4 - Binary Notation

```shell-session
Octet:             1st         2nd         3rd         4th
Binary:         1100 0000 . 1010 1000 . 0000 1010 . 0010 0111
Decimal:           192    .    168    .     10    .     39
```

- IPv4 Address: `192.168.10.39`

This addition takes place for each octet, which results in a decimal representation of the `IPv4 address`. The subnet mask is calculated in the same way.

#### IPv4 - Decimal to Binary

IPv4 - Decimal to Binary

```shell-session
Values:         128  64  32  16  8  4  2  1
Binary:           1   1   1   1  1  1  1  1
```

|**Octet**|**Values**|**Sum**|
|---|---|---|
|1st|128 + 64 + 32 + 16 + 8 + 4 + 2 + 1|= `255`|
|2nd|128 + 64 + 32 + 16 + 8 + 4 + 2 + 1|= `255`|
|3rd|128 + 64 + 32 + 16 + 8 + 4 + 2 + 1|= `255`|
|4th|0 + 0 + 0 + 0 + 0 + 0 + 0 + 0|= `0`|

#### Subnet Mask

Subnet Mask

```shell-session
Octet:             1st         2nd         3rd         4th
Binary:         1111 1111 . 1111 1111 . 1111 1111 . 0000 0000
Decimal:           255    .    255    .    255    .     0
```

- IPv4 Address: `192.168.10.39`
    
- Subnet mask: `255.255.255.0`
    

---

## CIDR

`Classless Inter-Domain Routing` (`CIDR`) is a method of representation and replaces the fixed assignment between IPv4 address and network classes (A, B, C, D, E). The division is based on the subnet mask or the so-called `CIDR suffix`, which allows the bitwise division of the IPv4 address space and thus into `subnets` of any size. The `CIDR suffix` indicates how many bits from the beginning of the IPv4 address belong to the network. It is a notation that represents the `subnet mask` by specifying the number of `1`-bits in the subnet mask.

Let us stick to the following IPv4 address and subnet mask as an example:

- IPv4 Address: `192.168.10.39`
    
- Subnet mask: `255.255.255.0`
    

Now the whole representation of the IPv4 address and the subnet mask would look like this:

- CIDR: `192.168.10.39/24`

The CIDR suffix is, therefore, the sum of all ones in the subnet mask.

Subnet Mask

```shell-session
Octet:             1st         2nd         3rd         4th
Binary:         1111 1111 . 1111 1111 . 1111 1111 . 0000 0000 (/24)
Decimal:           255    .    255    .    255    .     0
```

# SOUS-RÉSEAUX

The division of an address range of IPv4 addresses into several smaller address ranges is called `subnetting`.

A subnet is a logical segment of a network that uses IP addresses with the same network address. We can think of a subnet as a labeled entrance on a large building corridor. For example, this could be a glass door that separates various departments of a company building. With the help of subnetting, we can create a specific subnet by ourselves or find out the following outline of the respective network:

- `Network address`
- `Broadcast address`
- `First host`
- `Last host`
- `Number of hosts`

Let us take the following IPv4 address and subnet mask as an example:

- IPv4 Address: `192.168.12.160`
- Subnet Mask: `255.255.255.192`
- CIDR: `192.168.12.160/26`

---

We already know that an IP address is divided into the `network part` and the `host part`.

#### Network Part

|**Details of**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**Decimal**|
|---|---|---|---|---|---|
|IPv4|`1100 0000`|`1010 1000`|`0000 1100`|`10`10 0000|192.168.12.160`/26`|
|Subnet mask|`1111 1111`|`1111 1111`|`1111 1111`|`11`00 0000|`255.255.255.192`|
|Bits|/8|/16|/24|/32||

In subnetting, we use the subnet mask as a template for the IPv4 address. From the `1`-bits in the subnet mask, we know which bits in the IPv4 address `cannot` be changed. These are `fixed` and therefore determine the "main network" in which the subnet is located.

#### Host Part

|**Details of**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**Decimal**|
|---|---|---|---|---|---|
|IPv4|1100 0000|1010 1000|0000 1100|10`10 0000`|192.168.12.160/26|
|Subnet mask|1111 1111|1111 1111|1111 1111|11`00 0000`|255.255.255.192|
|Bits|/8|/16|/24|/32||

The bits in the `host part` can be changed to the `first` and `last` address. The first address is the `network address`, and the last address is the `broadcast address` for the respective subnet.

The `network address` is vital for the delivery of a data packet. If the `network address` is the same for the source and destination address, the data packet is delivered within the same subnet. If the network addresses are different, the data packet must be routed to another subnet via the `default gateway`.

The `subnet mask` determines where this separation occurs.

#### Separation Of Network & Host Parts

|**Details of**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**Decimal**|
|---|---|---|---|---|---|
|IPv4|1100 0000|1010 1000|0000 1100|10`\|`10 0000|192.168.12.160/26|
|Subnet mask|`1111 1111`|`1111 1111`|`1111 1111`|`11\|`00 0000|255.255.255.192|
|Bits|/8|/16|/24|/32||

---

#### Network Address

So if we now set all bits to `0` in the `host part` of the IPv4 address, we get the respective subnet's `network address`.

|**Details of**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**Decimal**|
|---|---|---|---|---|---|
|IPv4|1100 0000|1010 1000|0000 1100|10`\|00 0000`|`192.168.12.128`/26|
|Subnet mask|`1111 1111`|`1111 1111`|`1111 1111`|`11\|`00 0000|255.255.255.192|
|Bits|/8|/16|/24|/32||

---

#### Broadcast Address

If we set all bits in the `host part` of the IPv4 address to `1`, we get the `broadcast address`.

|**Details of**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**Decimal**|
|---|---|---|---|---|---|
|IPv4|1100 0000|1010 1000|0000 1100|10`\|11 1111`|`192.168.12.191`/26|
|Subnet mask|`1111 1111`|`1111 1111`|`1111 1111`|`11\|`00 0000|255.255.255.192|
|Bits|/8|/16|/24|/32||

Since we now know that the IPv4 addresses `192.168.12.128` and `192.168.12.191` are assigned, all other IPv4 addresses are accordingly between `192.168.12.129-190`. Now we know that this subnet offers us a total of `64 - 2` (network address & broadcast address) or `62` IPv4 addresses that we can assign to our hosts.

|**Hosts**|**IPv4**|
|---|---|
|Network Address|`192.168.12.128`|
|First Host|`192.168.12.129`|
|Other Hosts|`...`|
|Last Host|`192.168.12.190`|
|Broadcast Address|`192.168.12.191`|

---

## Subnetting Into Smaller Networks (Sous-réseau en réseaux plus petits)

Supposons maintenant qu'en tant qu'administrateurs, nous ayons été chargés de diviser le sous-réseau qui nous a été attribué en 4 sous-réseaux supplémentaires. Il est donc essentiel de savoir que nous ne pouvons diviser les sous-réseaux que sur la base du système binaire.

|**Exponent**|**Value**|
|---|---|
|2`^0`|= 1|
|2`^1`|= 2|
|2`^2`|= 4|
|2`^3`|= 8|
|2`^4`|= 16|
|2`^5`|= 32|
|2`^6`|= 64|
|2`^7`|= 128|
|2`^8`|= 256|

---

Nous pouvons donc diviser les `64 hôtes` que nous connaissons par `4`. Le `4` est égal à l'exposant 2`^2` dans le système binaire, nous trouvons donc le nombre de bits pour le masque de sous-réseau par lequel nous devons l'étendre. Nous connaissons donc les paramètres suivants :

- Sous-réseau : `192.168.12.128/26`
- Sous-réseaux requis : `4`

Maintenant, nous augmentons/expansons notre masque de sous-réseau de `2 bits` de `/26` à `/28`, et il ressemble à ceci :

|**Details of**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**Decimal**|
|---|---|---|---|---|---|
|IPv4|1100 0000|1010 1000|0000 1100|1000`\|` 0000|192.168.12.128`/28`|
|Subnet mask|`1111 1111`|`1111 1111`|`1111 1111`|`1111\|` 0000|`255.255.255.240`|
|Bits|/8|/16|/24|/32||

Ensuite, nous pouvons diviser les `64` adresses IPv4 disponibles en `4 parties` :

|**Hosts**|**Math**|**Subnets**|**Host range for each subnet**|
|---|---|---|---|
|64|/|4|= `16`|

Nous savons donc quelle sera la taille de chaque sous-réseau. A partir de maintenant, nous partons de l'adresse réseau qui nous a été donnée (192.168.12.128) et nous ajoutons les hôtes `16` `4` fois :

|**Subnet No.**|**Network Address**|**First Host**|**Last Host**|**Broadcast Address**|**CIDR**|
|---|---|---|---|---|---|
|1|`192.168.12.128`|192.168.12.129|192.168.12.142|`192.168.12.143`|192.168.12.128/28|
|2|`192.168.12.144`|192.168.12.145|192.168.12.158|`192.168.12.159`|192.168.12.144/28|
|3|`192.168.12.160`|192.168.12.161|192.168.12.174|`192.168.12.175`|192.168.12.160/28|
|4|`192.168.12.176`|192.168.12.177|192.168.12.190|`192.168.12.191`|192.168.12.176/28|
## Sous-réseau mental

Il peut sembler que le sous-réseau implique beaucoup de mathématiques, mais chaque octet se répète et tout est une puissance de deux, de sorte qu'il n'y a pas besoin de beaucoup de mémorisation. La première chose à faire est d'identifier l'octet qui change.

|**1er octet**|**2e octet**|**3e octet**|**4e octet**|
|---|---|---|---|
|/8|/16|/24|/32|

Il est possible d'identifier l'octet de l'adresse IP qui peut changer en se souvenant de ces quatre chiffres. Étant donné l'adresse réseau : `192.168.1.1/25`, il est immédiatement évident que 192.168.2.4 ne serait pas dans le même réseau parce que le sous-réseau `/25` signifie que seul le quatrième octet peut changer.

La partie suivante identifie la taille de chaque sous-réseau, mais en divisant huit par le réseau et en regardant le `reste`. Cette méthode est également appelée `opération modulo (%)` et est très utilisée en cryptologie. Dans notre exemple précédent de `/25`, `(25 % 8)` serait 1. En effet, 8 entre trois fois dans 25 (8 * 3 = 24). Il reste un 1, qui est le bit de réseau réservé au masque de réseau. Chaque octet d'une adresse IP contient au total huit bits. Si l'on en utilise un pour le masque de réseau, l'équation devient 2^(8-1) ou 2^7, 128. Le tableau ci-dessous contient tous les nombres.

|**Remainder**|**Number**|**Exponential Form**|**Division Form**|
|---|---|---|---|
|0|256|2^8|256|
|1|128|2^7|256/2|
|2|64|2^6|256/2/2|
|3|32|2^5|256/2/2/2|
|4|16|2^4|256/2/2/2/2|
|5|8|2^3|256/2/2/2/2/2|
|6|4|2^2|256/2/2/2/2/2/2|
|7|2|2^1|256/2/2/2/2/2/2/2|

En se souvenant des puissances de deux jusqu'à huit, le calcul devient instantané. Toutefois, en cas d'oubli, il peut être plus rapide de se souvenir de diviser 256 en deux fois le nombre de fois du reste.

La partie délicate de cette opération est d'obtenir la plage d'adresses IP réelle, car 0 est un nombre et n'est pas nul dans le domaine des réseaux. Donc dans notre `/25` avec 128 adresses IP, la première plage est `192.168.1.0-127`. La première adresse est le réseau, et la dernière est l'adresse de diffusion, ce qui signifie que l'espace IP utilisable serait `192.168.1.1-126`. Si notre adresse IP est supérieure à 128, l'espace IP utilisable sera 192.168.129-254 (128 est l'adresse réseau et 255 est l'adresse de diffusion).

# ADRESSES MAC

Chaque hôte d'un réseau possède sa propre adresse `Media Access Control` (`MAC`) de 48 bits (`6 octets`), représentée au format hexadécimal. Le `MAC` est l'adresse physique de nos interfaces réseau. Il existe plusieurs normes différentes pour l'adresse MAC :

- Ethernet (IEEE 802.3)
- Bluetooth (IEEE 802.15)
- WLAN (IEEE 802.11)

L'adresse MAC s'adresse à la connexion physique (carte réseau, adaptateur Bluetooth ou WLAN) d'un hôte. Chaque carte réseau a son adresse MAC individuelle, qui est configurée une fois au niveau du matériel du fabricant, mais qui peut toujours être modifiée, au moins temporairement.

Voyons un exemple d'adresse MAC :

Adresse MAC :

- `DE:AD:BE:EF:13:37`
- `DE-AD-BE-EF-13-37`
- `DEAD.BEEF.1337`

|**Représentation**|**1er Octet**|**2ème Octet**|**3ème Octet**|**4ème Octet**|**5ème Octet**|**6ème Octet**|
|---|---|---|---|---|---|---|
|Binaire|1101 1110|1010 1101|1011 1110|1110 1111|0001 0011|0011 0111|
|Hex|DE|AD|BE|EF|13|37|

Lorsqu'un paquet IP est livré, il doit être adressé sur la `couche 2` à l'adresse physique de l'hôte de destination ou au routeur / NAT, qui est responsable du routage. Chaque paquet possède une `adresse d'expéditeur` et une `adresse de destination`.

L'adresse MAC se compose d'un total de 6 octets. La première moitié (`3 octets` / `24 bits`) est le soi-disant `Organization Unique Identifier` (`OUI`) défini par le `Institute of Electrical and Electronics Engineers` (`IEEE`) pour les fabricants respectifs.

|**Représentation**|**1er octet**|**2e octet**|**3e octet**|**4e octet**|**5e octet**|**6e octet**|
|---|---|---|---|---|---|---|
|Binaire|`1101 1110`|`1010 1101`|`1011 1110`|1110 1111|0001 0011|0011 0111|
|Hex|`DE`|`AD`|`BE`|EF|13|37|


La dernière moitié de l'adresse MAC est appelée `partie d'adresse individuelle` ou `contrôleur d'interface réseau` (`NIC`), attribuée par le fabricant. Le fabricant ne définit cette séquence de bits qu'une seule fois et garantit ainsi que l'adresse complète est unique.

|**Représentation**|**1er octet**|**2e octet**|**3e octet**|**4e octet**|**5e octet**|**6e octet**|
|---|---|---|---|---|---|---|
|Binaire|1101 1110|1010 1101|1011 1110|`1110 1111`|`0001 0011`|`0011 0111`|
|Hex|DE|AD|BE|`EF`|`13`|`37`|

Si un hôte avec l'adresse IP cible est situé dans le même sous-réseau, la livraison est effectuée directement à l'adresse physique de l'ordinateur cible. Cependant, si cet hôte appartient à un sous-réseau différent, la trame Ethernet est adressée à l'`adresse MAC` du routeur responsable (`passerelle par défaut`). Si l'adresse de destination de la trame Ethernet correspond à sa propre `adresse de couche 2`, le routeur transmet la trame aux couches supérieures. Le `protocole de résolution d'adresses` (`ARP`) est utilisé dans l'IPv4 pour déterminer les adresses MAC associées aux adresses IP.

Comme pour les adresses IPv4, il existe également certaines zones réservées pour l'adresse MAC. Il s'agit, par exemple, de la plage locale de l'adresse MAC.

|**Plage locale**|
|---|
|0`2`:00:00:00:00:00|
|0`6`:00:00:00:00:00|
|0`A`:00:00:00:00:00|
|0`E`:00:00:00:00:00|

En outre, les deux derniers bits du premier octet peuvent jouer un autre rôle essentiel. Le dernier bit peut avoir deux états, 0 et 1, comme nous le savons déjà. Le dernier bit identifie l'adresse MAC comme `Unicast` (`0`) ou `Multicast` (`1`). Avec `unicast`, cela signifie que le paquet envoyé n'atteindra qu'un seul hôte spécifique.

#### MAC Unicast

|**Representation**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**5th Octet**|**6th Octet**|
|---|---|---|---|---|---|---|
|Binaire|1101 111`0`|1010 1101|1011 1110|1110 1111|0001 0011|0011 0111|
|Hex|D`E`|AD|BE|EF|13|37|

Avec `multicast`, le paquet est envoyé une seule fois à tous les hôtes du réseau local, qui décident alors d'accepter ou non le paquet en fonction de leur configuration. L'adresse `multicast` est une adresse unique, tout comme l'adresse `broadcast`, qui a des valeurs d'octets fixes. Dans un réseau, l'adresse `broadcast` représente un appel diffusé, où les paquets de données sont transmis simultanément d'un point à tous les membres du réseau. Il est principalement utilisé lorsque l'adresse du destinataire du paquet n'est pas encore connue. Les protocoles `ARP` (`pour les adresses MAC`) et DHCP (`pour les adresses IPv4`) en sont un exemple.

Les valeurs définies de chaque octet sont marquées en vert.
#### MAC Multicast

|**Représentation**|**1er Octet**|**2ème Octet**|**3ème Octet**|**4ème Octet**|**5ème Octet**|**6ème Octet**|
|---|---|---|---|---|---|---|
|Binaire|`0000 0001`|`0000 0000`|`0101 1110`|1110 1111|0001 0011|0011 0111|
|Hex|`01`|`00`|`5E`|EF|13|37|

#### MAC Broadcast

|**Représentation**|**1er Octet**|**2ème Octet**|**3ème Octet**|**4ème Octet**|**5ème Octet**|**6ème Octet**|
|---|---|---|---|---|---|---|
|Binaire|`1111 1111`|`1111 1111`|`1111 1111`|`1111 1111`|`1111 1111`|`1111 1111`|`1111 1111`|
|Hex|`FF`|`FF`|`FF`|`FF`|`FF`|`FF`|`FF`||`FF`||`FF`||||||||||||||||||||||

L'avant-dernier bit du premier octet indique s'il s'agit d'une `OUI globale`, définie par l'IEEE, ou d'une adresse MAC `administrée localement`.

#### Global OUI

|**Representation**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**5th Octet**|**6th Octet**|
|---|---|---|---|---|---|---|
|Binary|1101 11`0`0|1010 1101|1011 1110|1110 1111|0001 0011|0011 0111|
|Hex|D`C`|AD|BE|EF|13|37|

#### Locally Administrated

|**Representation**|**1st Octet**|**2nd Octet**|**3rd Octet**|**4th Octet**|**5th Octet**|**6th Octet**|
|---|---|---|---|---|---|---|
|Binary|1101 11`1`0|1010 1101|1011 1110|1110 1111|0001 0011|0011 0111|
|Hex|D`E`|AD|BE|EF|13|37|

## Address Resolution Protocol

Les adresses MAC peuvent être modifiées, manipulées ou usurpées et ne doivent donc pas être considérées comme le seul moyen de sécurité ou d'identification. Les administrateurs de réseaux doivent mettre en œuvre des mesures de sécurité supplémentaires, telles que la segmentation du réseau et des protocoles d'authentification forts, afin de se protéger contre les attaques potentielles.

Il existe plusieurs vecteurs d'attaque qui peuvent potentiellement être exploités par l'utilisation des adresses MAC :

- `L'usurpation d'adresse MAC` (`MAC spoofing`) : Il s'agit de modifier l'adresse MAC d'un appareil pour qu'elle corresponde à celle d'un autre appareil, généralement pour obtenir un accès non autorisé à un réseau.

- `L'inondation d'adresse MAC` (`MAC flooding`) : Il s'agit d'envoyer de nombreux paquets avec des adresses MAC différentes à un commutateur de réseau, ce qui l'amène à atteindre la capacité de sa table d'adresses MAC et l'empêche de fonctionner correctement.

- `Filtrage des adresses MAC` : Certains réseaux peuvent être configurés pour n'autoriser l'accès qu'à des appareils ayant des adresses MAC spécifiques que nous pourrions potentiellement exploiter en tentant d'accéder au réseau à l'aide d'une adresse MAC usurpée.

## Address Resolution Protocol

[Address Resolution Protocol](https://en.wikipedia.org/wiki/Address_Resolution_Protocol) (`ARP`) est un protocole de réseau. Il s'agit d'un élément important de la communication réseau utilisé pour résoudre une adresse IP de la couche réseau (couche 3) en une adresse MAC de la couche liaison (couche 2). Il associe l'adresse IP d'un hôte à l'adresse MAC correspondante afin de faciliter la communication entre les appareils d'un [réseau local](https://en.wikipedia.org/wiki/Local_area_network) (`LAN`). Lorsqu'un appareil d'un réseau local souhaite communiquer avec un autre appareil, il envoie un message de diffusion contenant l'adresse IP de destination et sa propre adresse MAC. L'appareil ayant l'adresse IP correspondante répond avec sa propre adresse MAC, et les deux appareils peuvent alors communiquer directement en utilisant leurs adresses MAC. Ce processus est connu sous le nom de résolution ARP.

L'ARP est un élément important du processus de communication réseau car il permet aux appareils d'envoyer et de recevoir des données en utilisant des adresses MAC plutôt que des adresses IP, ce qui peut s'avérer plus efficace. Deux types de messages de requête peuvent être utilisés :
#### ARP Request

Lorsqu'un appareil souhaite communiquer avec un autre appareil sur un réseau local, il envoie une requête ARP pour résoudre l'adresse IP de l'appareil de destination en fonction de son adresse MAC. La requête est diffusée à tous les appareils du réseau local et contient l'adresse IP de l'appareil de destination. L'appareil dont l'adresse IP correspond répond en indiquant son adresse MAC.
#### ARP Reply

Lorsqu'un appareil reçoit une requête ARP, il envoie une réponse ARP à l'appareil demandeur avec son adresse MAC. Le message de réponse contient les adresses IP et MAC de l'appareil demandeur et de l'appareil répondant.
#### Tshark Capture of ARP Requests

Capture de requêtes ARP par Tshark

```shell-session
1   0.000000 10.129.12.100 -> 10.129.12.255 ARP 60  Who has 10.129.12.101?  Tell 10.129.12.100
2   0.000015 10.129.12.101 -> 10.129.12.100 ARP 60  10.129.12.101 is at AA:AA:AA:AA:AA:AA

3   0.000030 10.129.12.102 -> 10.129.12.255 ARP 60  Who has 10.129.12.103?  Tell 10.129.12.102
4   0.000045 10.129.12.103 -> 10.129.12.102 ARP 60  10.129.12.103 is at BB:BB:BB:BB:BB:BB
```

The "`who has`" message in the first and third lines indicates that a device is requesting the MAC address for the specified IP address, while the second and fourth lines show the ARP reply with the MAC address of the destination device.

However, it is also vulnerable to attacks, such as [ARP Spoofing](https://en.wikipedia.org/wiki/ARP_spoofing), which can be used to intercept or manipulate traffic on the network. However, to protect against such attacks, it is important to implement security measures such as firewalls and intrusion detection systems.

`ARP spoofing`, also known as `ARP cache poisoning` or `ARP poison routing`, is an attack that can be done using tools like [Ettercap](https://github.com/Ettercap/ettercap) or [Cain & Abel](https://github.com/xchwarze/Cain) in which we send falsified ARP messages over a LAN. The goal is to associate our MAC address with the IP address of a legitimate device on the company's network, effectively allowing us to intercept traffic intended for the legitimate device. For example, this could look like the following:

Tshark Capture of ARP Requests

```shell-session
1   0.000000 10.129.12.100 -> 10.129.12.101 ARP 60  10.129.12.100 is at AA:AA:AA:AA:AA:AA
2   0.000015 10.129.12.100 -> 10.129.12.255 ARP 60  Who has 10.129.12.101?  Tell 10.129.12.100
3   0.000030 10.129.12.101 -> 10.129.12.100 ARP 60  10.129.12.101 is at BB:BB:BB:BB:BB:BB
4   0.000045 10.129.12.100 -> 10.129.12.101 ARP 60  10.129.12.100 is at AA:AA:AA:AA:AA:AA
```

The first and fourth lines show us (`10.129.12.100`) sending falsified ARP messages to the target, associating its MAC address with its IP address (`10.129.12.101`). The second and third lines show the target sending an ARP request and replying to our MAC address. This indicates that we have poisoned the target's ARP cache and that all traffic intended for the target will now be sent to our MAC address.

We can use ARP poisoning to perform various activities, such as stealing sensitive information, redirecting traffic, or launching MITM attacks. However, to protect against ARP spoofing, it is important to use secure network protocols, such as IPSec or SSL, and to implement security measures, such as firewalls and intrusion detection systems.
# ADRESSES IPv6

`IPv6` est le successeur d'IPv4. Contrairement à l'IPv4, l'adresse `IPv6` est longue de `128` bits. Le préfixe identifie les parties hôte et réseau. L'`IANA` (`Internet Assigned Numbers Authority`) est responsable de l'attribution des adresses IPv4 et IPv6 et des parties du réseau qui leur sont associées. À long terme, l'`IPv6` devrait remplacer complètement l'IPv4, qui est encore majoritairement utilisé sur l'internet. En principe, IPv4 et IPv6 peuvent toutefois être disponibles simultanément (`Dual Stack`).

L'IPv6 suit systématiquement le principe de bout en bout et fournit des adresses IP accessibles au public pour tous les appareils finaux, sans qu'il soit nécessaire de recourir à la NAT. Par conséquent, une interface peut avoir plusieurs adresses IPv6, et il existe des adresses IPv6 spéciales auxquelles plusieurs interfaces sont assignées.

IPv6 est un protocole doté de nombreuses nouvelles fonctionnalités, qui présente également de nombreux autres avantages par rapport à IPv4 :

    Espace d'adressage plus grand
    Auto-configuration des adresses (SLAAC)
    Plusieurs adresses IPv6 par interface
    Routage plus rapide
    Cryptage de bout en bout (IPsec)
    Paquets de données jusqu'à 4 GByte

|Features|IPv4|IPv6|
|---|---|---|
|Longueur des bits|32 bits|128 bits|
|Couche OSI|Couche réseau|Couche réseau|
|Plage d'adressage|~ 4,3 milliards|~ 340 undecillion|
|Représentation|Binaire|Hexadécimale|
|Notation du préfixe|10.10.10.0/24|fe80::dd80:b1a9:6687:2d3b/64|
|Adressage dynamique|DHCP|SLAAC / DHCPv6|
|IPsec|Facultatif|Obligatoire|

Il existe quatre types différents d'adresses IPv6 :

|Type|Description|
|---|---|
|Unicast|Adresses pour une seule interface|
|Anycast|Adresses pour plusieurs interfaces, où seule l'une d'entre elles reçoit le paquet|
|Multicast|Adresses pour plusieurs interfaces, où toutes reçoivent le même paquet|
|Broadcast|N'existe pas et est réalisé avec des adresses multicast|
## Système hexadécimal

Le `système hexadécimal` (`hex`) est utilisé pour rendre la représentation binaire plus lisible et plus compréhensible. On ne peut représenter que `10` (`0-9`) états avec le système décimal et `2` (`0/1`) avec le système binaire en utilisant un seul caractère. Contrairement aux systèmes binaire et décimal, le système hexadécimal permet de représenter `16` états (`0-F`) à l'aide d'un seul caractère.

|Décimal|Hex|Binaire|
|---|---|---|
|1|1|0001|
|2|2|0010|
|3|3|0011|
|4|4|0100|
|5|5|0101|
|6|6|0110|
|7|7|0111|
|8|8|1000|
|9|9|1001|
|10|A|1010|
|11|B|1011|
|12|C|1100|
|13|D|1101|
|14|E|1110|
|15|F|1111|

Prenons un exemple avec une adresse IPv4, et voyons comment l'adresse IPv4 (192.168.12.160) serait représentée en hexadécimal.

|Représentation|1er octet|2e octet|3e octet|4e octet|
|---|---|---|---|---|
|Binaire|1100 0000|1010 1000|0000 1100|1010 0000|
|Hex|C0|A8|0C|A0|
|Décimal|192|168|12|160|

Au total, l'adresse IPv6 se compose de 16 octets. En raison de sa longueur, une adresse IPv6 est représentée en notation hexadécimale. Les 128 bits sont donc divisés en 8 blocs multipliés par 16 bits (ou 4 nombres hexadécimaux). Les quatre nombres hexadécimaux sont regroupés et séparés par deux points ( :) au lieu d'un simple point (.) comme dans IPv4. Pour simplifier la notation, nous omettons les 4 premiers zéros dans les blocs, et nous pouvons les remplacer par deux deux-points (: :).

Une adresse IPv6 peut se présenter comme suit :

    IPv6 complet : fe80:0000:0000:0000:dd80:b1a9:6687:2d3b/64
    IPv6 courte : fe80::dd80:b1a9:6687:2d3b/64

Une adresse IPv6 se compose de deux parties :

    le préfixe de réseau (partie réseau)
    Identificateur d'interface également appelé suffixe (partie hôte)

Le préfixe de réseau identifie le réseau, le sous-réseau ou la plage d'adresses. L'identificateur d'interface est formé à partir de l'adresse MAC de 48 bits (dont nous parlerons plus tard) de l'interface et est converti en une adresse de 64 bits au cours du processus. La longueur du préfixe par défaut est /64. Cependant, d'autres préfixes typiques sont /32, /48 et /56. Si nous voulons utiliser nos réseaux, nous obtenons un préfixe plus court (par exemple /56) que /64 auprès de notre fournisseur.

La notation des adresses IPv6 mentionnée ci-dessus a été définie dans la RFC 5952 :

    Tous les caractères alphabétiques sont toujours écrits en minuscules.
    Tous les premiers zéros d'un bloc sont toujours omis.
    Un ou plusieurs blocs consécutifs de 4 zéros (hex) sont raccourcis par deux points (: :).
    Le raccourcissement par deux points (: :) ne peut être effectué qu'une seule fois en partant de la gauche.

# TERMINOLOGIE CLÉ DES RÉSEAUX

Voici une liste des protocoles les plus courants et de leurs descriptions, même s'ils sont très différents les uns des autres. Cette liste est incomplète. Cependant, il est conseillé de l'enrichir au fur et à mesure que l'on apprend de nouveaux protocoles.

|Protocole|Acronyme|Description|
|---|---|---|
|Wired Equivalent Privacy|**WEP**|Le WEP est un type de protocole de sécurité couramment utilisé pour sécuriser les réseaux sans fil.|
|Secure Shell|**SSH**|Protocole réseau sécurisé utilisé pour se connecter à un système distant et y exécuter des commandes.|
|Protocole de transfert de fichiers|**FTP**|Protocole réseau utilisé pour transférer des fichiers d'un système à un autre.|
|Simple Mail Transfer Protocol|**SMTP**|Protocole utilisé pour envoyer et recevoir des courriers électroniques.|
|Hypertext Transfer Protocol|**HTTP**|Protocole client-serveur utilisé pour envoyer et recevoir des données sur l'internet.|
|Server Message Block|**SMB**|Protocole utilisé pour partager des fichiers, des imprimantes et d'autres ressources sur un réseau.|
|Network File System|**NFS**|Protocole utilisé pour accéder à des fichiers sur un réseau|
|Simple Network Management Protocol|**SNMP**|Protocole utilisé pour gérer les périphériques de réseau|
|Wi-Fi Protected Access|**WPA**|Le WPA est un protocole de sécurité sans fil qui utilise un mot de passe pour protéger les réseaux sans fil contre les accès non autorisés.|
|Temporal Key Integrity Protocol|**TKIP**|Le TKIP est également un protocole de sécurité utilisé dans les réseaux sans fil, mais il est moins sûr.|
|Network Time Protocol|**NTP**|Il est utilisé pour synchroniser les ordinateurs d'un réseau.|
|Réseau local virtuel|**VLAN**|Il s'agit d'un moyen de segmenter un réseau en plusieurs réseaux logiques.|
|VLAN Trunking Protocol|**VTP**|Le VTP est un protocole de couche 2 utilisé pour établir et maintenir un réseau local virtuel (VLAN) couvrant plusieurs commutateurs.|
|Routing Information Protocol|**RIP**|Le RIP est un protocole de routage à vecteur de distance utilisé dans les réseaux locaux (LAN) et les réseaux étendus (WAN).|
|Open Shortest Path First|**OSPF**|Il s'agit d'un protocole de passerelle intérieure (IGP) pour l'acheminement du trafic au sein d'un système autonome (AS) unique dans un réseau IP (Internet Protocol).|
|Protocole de routage de passerelle intérieure|**IGRP**|L'IGRP est un protocole de passerelle intérieure propriétaire de Cisco conçu pour le routage au sein de systèmes autonomes.|
|Enhanced Interior Gateway Routing Protocol|**EIGRP**|Il s'agit d'un protocole de routage avancé à vecteur de distance utilisé pour acheminer le trafic IP au sein d'un réseau.|
|Pretty Good Privacy|**PGP**|PGP est un programme de cryptage utilisé pour sécuriser les courriers électroniques, les fichiers et d'autres types de données.|
|Network News Transfer Protocol|**NNTP**|NNTP est un protocole utilisé pour distribuer et récupérer des messages dans des groupes de discussion sur l'internet.|
|Cisco Discovery Protocol|**CDP**|Il s'agit d'un protocole propriétaire développé par Cisco Systems qui permet aux administrateurs de réseaux de découvrir et de gérer les périphériques Cisco connectés au réseau.|
|Hot Standby Router Protocol|**HSRP**|HSRP est un protocole utilisé dans les routeurs Cisco pour assurer la redondance en cas de défaillance d'un routeur ou d'un autre périphérique du réseau.|
|Virtual Router Redundancy Protocol|**VRRP**|Il s'agit d'un protocole utilisé pour assurer l'affectation automatique des routeurs IP (Internet Protocol) disponibles aux hôtes participants.|
|Spanning Tree Protocol|**STP**|Le STP est un protocole réseau utilisé pour garantir une topologie sans boucle dans les réseaux Ethernet de couche 2.|
|Terminal Access Controller Access-Control System|**TACACS**|TACACS est un protocole qui fournit une authentification, une autorisation et une comptabilité centralisées pour l'accès au réseau.|
|Session Initiation Protocol|**SIP**|Il s'agit d'un protocole de signalisation utilisé pour établir et terminer des sessions vocales, vidéo et multimédia en temps réel sur un réseau IP.|
|Voice Over IP|**VOIP**|VOIP est une technologie qui permet de passer des appels téléphoniques sur l'internet.|
|Extensible Authentication Protocol|**EAP**|EAP est un cadre d'authentification qui prend en charge plusieurs méthodes d'authentification, telles que les mots de passe, les certificats numériques, les mots de passe à usage unique et l'authentification par clé publique.|
|Lightweight Extensible Authentication Protocol|**LEAP**|LEAP est un protocole d'authentification sans fil propriétaire développé par Cisco Systems. Il est basé sur le protocole d'authentification extensible (EAP) utilisé dans le protocole point à point (PPP).|
|Protected Extensible Authentication Protocol|**PEAP**|PEAP est un protocole de sécurité qui fournit un tunnel crypté pour les réseaux sans fil et d'autres types de réseaux.|
|Systems Management Server|**SMS**|SMS est une solution de gestion des systèmes qui aide les organisations à gérer leurs réseaux, leurs systèmes et leurs appareils mobiles.|
|Microsoft Baseline Security Analyzer|**MBSA**|Il s'agit d'un outil de sécurité gratuit de Microsoft qui est utilisé pour détecter les vulnérabilités potentielles en matière de sécurité dans les ordinateurs, les réseaux et les systèmes Windows.|
|Supervisory Control and Data Acquisition|**SCADA**|Il s'agit d'un type de système de contrôle industriel utilisé pour surveiller et contrôler les processus industriels, tels que ceux de la fabrication, de la production d'énergie et du traitement de l'eau et des déchets.|
|Réseau privé virtuel|**VPN**|Le VPN est une technologie qui permet aux utilisateurs de créer une connexion sécurisée et cryptée à un autre réseau sur l'internet.|
|Internet Protocol Security|**IPsec**|IPsec est un protocole utilisé pour assurer une communication sécurisée et cryptée sur un réseau. Il est couramment utilisé dans les réseaux privés virtuels (VPN) pour créer un tunnel sécurisé entre deux appareils.|
|Point-to-Point Tunneling Protocol|**PPTP**|Il s'agit d'un protocole utilisé pour créer un tunnel sécurisé et crypté pour l'accès à distance.|
|Network Address Translation|**NAT**|NAT est une technologie qui permet à plusieurs appareils d'un réseau privé de se connecter à l'internet à l'aide d'une seule adresse IP publique. Le NAT fonctionne en traduisant les adresses IP privées des appareils sur le réseau en une seule adresse IP publique, qui est ensuite utilisée pour se connecter à l'internet.|
|Carriage Return Line Feed|**CRLF**|Combine deux caractères de contrôle pour indiquer la fin d'une ligne et le début d'une nouvelle pour certains formats de fichiers texte.|
|Asynchronous JavaScript and XML |**AJAX**|Technique de développement web qui permet de créer des pages web dynamiques en utilisant JavaScript et XML/JSON.|
|Internet Server Application Programming Interface|**ISAPI**|Permet de créer des extensions web orientées performances pour les serveurs web à l'aide d'un ensemble d'API.|
|Uniform Resource Identifier|**URI**|Syntaxe utilisée pour identifier une ressource sur l'internet.|
|Uniform Resource Locator|**URL**|Sous-ensemble de l'URI qui identifie une page web ou une autre ressource sur Internet, y compris le protocole et le nom de domaine.|
|Internet Key Exchange|**IKE**|IKE est un protocole utilisé pour établir une connexion sécurisée entre deux ordinateurs. Il est utilisé dans les réseaux privés virtuels (VPN) pour assurer l'authentification et le cryptage de la transmission des données, protégeant ainsi les données contre les écoutes et les manipulations extérieures.|
|Generic Routing Encapsulation|**GRE**|Ce protocole est utilisé pour encapsuler les données transmises dans le tunnel VPN.|
|Remote Shell|**RSH**|C'est un programme sous Unix qui permet d'exécuter des commandes et des programmes sur un ordinateur distant.|


# PROTOCOLES COMMUNS

Les protocoles Internet sont des règles et des lignes directrices normalisées, définies dans des RFC, qui précisent comment les appareils d'un réseau doivent communiquer entre eux. Ils garantissent que les appareils d'un réseau peuvent échanger des informations de manière cohérente et fiable, quels que soient le matériel et les logiciels utilisés. Pour que les appareils communiquent sur un réseau, ils doivent être connectés par un canal de communication, tel qu'une connexion câblée ou sans fil. Les appareils échangent ensuite des informations à l'aide d'un ensemble de protocoles normalisés qui définissent le format et la structure des données transmises. Les deux principaux types de connexions utilisés sur les réseaux sont le [**protocole de contrôle de transmission**](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) (**TCP**) et le [**protocole de datagramme utilisateur**](https://en.wikipedia.org/wiki/User_Datagram_Protocol) (**UDP**).

Nous devons traiter et connaître les différents protocoles les plus utilisés. Comme nous l'avons déjà appris, ces protocoles sont à la base de toutes les communications entre nos appareils et les ordinateurs des réseaux. Nous avons compilé ci-dessous un grand nombre de ces protocoles que nous aborderons tout au long des modules. Mieux nous les comprendrons, plus nous pourrons travailler efficacement avec eux.
#### Protocole de contrôle de transmission

Le **TCP** est un **protocole orienté connexion** qui établit une connexion virtuelle entre deux appareils avant de transmettre des données à l'aide d'une [**Three-Way-Handshake**](https://en.wikipedia.org/wiki/Transmission_Control_Protocol#Connection_establishment) (**poignée de main à trois voies**). Cette connexion est maintenue jusqu'à ce que le transfert de données soit terminé, et les appareils peuvent continuer à envoyer des données dans les deux sens tant que la connexion est active.

Par exemple, lorsque nous saisissons une URL dans notre navigateur web, celui-ci envoie une requête HTTP au serveur hébergeant le site web en utilisant le protocole **TCP**. Le serveur répond en renvoyant le code HTML du site web au navigateur à l'aide de **TCP**. Le navigateur utilise ensuite ce code pour afficher le site web sur notre écran. Ce processus repose sur l'établissement d'une connexion **TCP** entre le navigateur et le serveur web, qui est maintenue jusqu'à la fin du transfert de données. Par conséquent, le **TCP** est fiable mais plus lent que l'UDP car il nécessite des frais généraux supplémentaires pour établir et maintenir la connexion.

|**Protocol**|**Acronym**|**Port**|**Description**|
|---|---|---|---|
|Telnet|**Telnet**|**23**|Remote login service|
|Secure Shell|**SSH**|**22**|Secure remote login service|
|Simple Network Management Protocol|**SNMP**|**161-162**|Manage network devices|
|Hyper Text Transfer Protocol|**HTTP**|**80**|Used to transfer webpages|
|Hyper Text Transfer Protocol Secure|**HTTPS**|**443**|Used to transfer secure webpages|
|Domain Name System|**DNS**|**53**|Lookup domain names|
|File Transfer Protocol|**FTP**|**20-21**|Used to transfer files|
|Trivial File Transfer Protocol|**TFTP**|**69**|Used to transfer files|
|Network Time Protocol|**NTP**|**123**|Synchronize computer clocks|
|Simple Mail Transfer Protocol|**SMTP**|**25**|Used for email transfer|
|Post Office Protocol|**POP3**|**110**|Used to retrieve emails|
|Internet Message Access Protocol|**IMAP**|**143**|Used to access emails|
|Server Message Block|**SMB**|**445**|Used to transfer files|
|Network File System|**NFS**|**111**, **2049**|Used to mount remote systems|
|Bootstrap Protocol|**BOOTP**|**67**, **68**|Used to bootstrap computers|
|Kerberos|**Kerberos**|**88**|Used for authentication and authorization|
|Lightweight Directory Access Protocol|**LDAP**|**389**|Used for directory services|
|Remote Authentication Dial-In User Service|**RADIUS**|**1812**, **1813**|Used for authentication and authorization|
|Dynamic Host Configuration Protocol|**DHCP**|**67**, **68**|Used to configure IP addresses|
|Remote Desktop Protocol|**RDP**|**3389**|Used for remote desktop access|
|Network News Transfer Protocol|**NNTP**|**119**|Used to access newsgroups|
|Remote Procedure Call|**RPC**|**135**, **137-139**|Used to call remote procedures|
|Identification Protocol|**Ident**|**113**|Used to identify user processes|
|Internet Control Message Protocol|**ICMP**|**0-255**|Used to troubleshoot network issues|
|Internet Group Management Protocol|**IGMP**|**0-255**|Used for multicasting|
|Oracle DB (Default/Alternative) Listener|**oracle-tns**|**1521**/**1526**|The Oracle database default/alternative listener is a service that runs on the database host and receives requests from Oracle clients.|
|Ingres Lock|**ingreslock**|**1524**|Ingres database is commonly used for large commercial applications and as a backdoor that can execute commands remotely via RPC.|
|Squid Web Proxy|**http-proxy**|**3128**|Squid web proxy is a caching and forwarding HTTP web proxy used to speed up a web server by caching repeated requests.|
|Secure Copy Protocol|**SCP**|**22**|Securely copy files between systems|
|Session Initiation Protocol|**SIP**|**5060**|Used for VoIP sessions|
|Simple Object Access Protocol|**SOAP**|**80**, **443**|Used for web services|
|Secure Socket Layer|**SSL**|**443**|Securely transfer files|
|TCP Wrappers|**TCPW**|**113**|Used for access control|
|Network Time Protocol|**NTP**|**123**|Synchronize computer clocks|
|Internet Security Association and Key Management Protocol|**ISAKMP**|**500**|Used for VPN connections|
|Microsoft SQL Server|**ms-sql-s**|**1433**|Used for client connections to the Microsoft SQL Server.|
|Kerberized Internet Negotiation of Keys|**KINK**|**892**|Used for authentication and authorization|
|Open Shortest Path First|**OSPF**|**520**|Used for routing|
|Point-to-Point Tunneling Protocol|**PPTP**|**1723**|Is used to create VPNs|
|Remote Execution|**REXEC**|**512**|This protocol is used to execute commands on remote computers and send the output of commands back to the local computer.|
|Remote Login|**RLOGIN**|**513**|This protocol starts an interactive shell session on a remote computer.|
|X Window System|**X11**|**6000**|It is a computer software system and network protocol that provides a graphical user interface (GUI) for networked computers.|
|Relational Database Management System|**DB2**|**50000**|RDBMS is designed to store, retrieve and manage data in a structured format for enterprise applications such as financial systems, customer relationship management (CRM) systems.|

#### Protocole de datagramme utilisateur

En revanche, **UDP** est un **protocole sans connexion**, ce qui signifie qu'il n'établit pas de connexion virtuelle avant de transmettre des données. Au lieu de cela, il envoie les paquets de données à la destination sans vérifier s'ils ont été reçus.

Par exemple, lorsque nous diffusons ou regardons une vidéo sur une plateforme comme YouTube, les données vidéo sont transmises à notre appareil à l'aide d'**UDP**. En effet, la vidéo peut tolérer une certaine perte de données et la vitesse de transmission est plus importante que la fiabilité. Si quelques paquets de données vidéo sont perdus en cours de route, cela n'aura pas d'impact significatif sur la qualité globale de la vidéo. L'**UDP** est donc plus rapide que le TCP, mais moins fiable, car il n'y a aucune garantie que les paquets atteindront leur destination.

|**Protocol**|**Acronym**|**Port**|**Description**|
|---|---|---|---|
|Domain Name System|**DNS**|**53**|It is a protocol to resolve domain names to IP addresses.|
|Trivial File Transfer Protocol|**TFTP**|**69**|It is used to transfer files between systems.|
|Network Time Protocol|**NTP**|**123**|It synchronizes computer clocks in a network.|
|Simple Network Management Protocol|**SNMP**|**161**|It monitors and manages network devices remotely.|
|Routing Information Protocol|**RIP**|**520**|It is used to exchange routing information between routers.|
|Internet Key Exchange|**IKE**|**500**|Internet Key Exchange|
|Bootstrap Protocol|**BOOTP**|**68**|It is used to bootstrap hosts in a network.|
|Dynamic Host Configuration Protocol|**DHCP**|**67**|It is used to assign IP addresses to devices in a network dynamically.|
|Telnet|**TELNET**|**23**|It is a text-based remote access communication protocol.|
|MySQL|**MySQL**|**3306**|It is an open-source database management system.|
|Terminal Server|**TS**|**3389**|It is a remote access protocol used for Microsoft Windows Terminal Services by default.|
|NetBIOS Name|**netbios-ns**|**137**|It is used in Windows operating systems to resolve NetBIOS names to IP addresses on a LAN.|
|Microsoft SQL Server|**ms-sql-m**|**1434**|Used for the Microsoft SQL Server Browser service.|
|Universal Plug and Play|**UPnP**|**1900**|It is a protocol for devices to discover each other on the network and communicate.|
|PostgreSQL|**PGSQL**|**5432**|It is an object-relational database management system.|
|Virtual Network Computing|**VNC**|**5900**|It is a graphical desktop sharing system.|
|X Window System|**X11**|**6000-6063**|It is a computer software system and network protocol that provides GUI on Unix-like systems.|
|Syslog|**SYSLOG**|**514**|It is a standard protocol to collect and store log messages on a computer system.|
|Internet Relay Chat|**IRC**|**194**|It is a real-time Internet text messaging (chat) or synchronous communication protocol.|
|OpenPGP|**OpenPGP**|**11371**|It is a protocol for encrypting and signing data and communications.|
|Internet Protocol Security|**IPsec**|**500**|IPsec is also a protocol that provides secure, encrypted communication. It is commonly used in VPNs to create a secure tunnel between two devices.|
|Internet Key Exchange|**IKE**|**11371**|It is a protocol for encrypting and signing data and communications.|
|X Display Manager Control Protocol|**XDMCP**|**177**|XDMCP is a network protocol that allows a user to remotely log in to a computer running the X11.|

#### ICMP

L' [**Internet Control Message Protocol**](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol) (**ICMP**) est un protocole utilisé par les appareils pour communiquer entre eux sur Internet à diverses fins, notamment pour signaler des erreurs et fournir des informations sur l'état de l'appareil. Il envoie des requêtes et des messages entre les appareils, qui peuvent être utilisés pour signaler des erreurs ou fournir des informations d'état.
#### Requêtes ICMP

Une requête est un message envoyé par un appareil à un autre pour demander des informations ou effectuer une action spécifique. Un exemple de requête ICMP est la requête **ping**, qui teste la connectivité entre deux appareils. Lorsqu'un appareil envoie une requête ping à un autre appareil, ce dernier répond par un message de **réponse ping**.
#### Messages ICMP

Un message ICMP peut être une requête ou une réponse. Outre les requêtes et les réponses ping, ICMP prend en charge d'autres types de messages, tels que les messages d'erreur, **destination unreachable**, et **time exceeded**. Ces messages sont utilisés pour communiquer divers types d'informations et d'erreurs entre les appareils du réseau.

Par exemple, si un appareil essaie d'envoyer un paquet à un autre appareil et que le paquet ne peut pas être délivré, l'appareil peut utiliser ICMP pour renvoyer un message d'erreur à l'expéditeur. L'ICMP a deux versions différentes :

    ICMPv4 : pour IPv4 uniquement
    ICMPv6 : pour IPv6 uniquement.

ICMPv4 est la version originale d'ICMP, développée pour être utilisée avec IPv4. Elle est encore largement utilisée et constitue la version la plus courante de l'ICMP. En revanche, ICMPv6 a été développé pour IPv6. Elle comprend des fonctionnalités supplémentaires et est conçue pour remédier à certaines des limitations de l'ICMPv4.

|**Request Type**|**Description**|
|---|---|
|`Echo Request`|This message tests whether a device is reachable on the network. When a device sends an echo request, it expects to receive an echo reply message. For example, the tools `tracert` (Windows) or `traceroute` (Linux) always send ICMP echo requests.|
|`Timestamp Request`|This message determines the time on a remote device.|
|`Address Mask Request`|This message is used to request the subnet mask of a device.|

|**Message Type**|**Description**|
|---|---|
|`Echo reply`|This message is sent in response to an echo request message.|
|`Destination unreachable`|This message is sent when a device cannot deliver a packet to its destination.|
|`Redirect`|A router sends this message to inform a device that it should send its packets to a different router.|
|`time exceeded`|This message is sent when a packet has taken too long to reach its destination.|
|`Parameter problem`|This message is sent when there is a problem with a packet's header.|
|`Source quench`|This message is sent when a device receives packets too quickly and cannot keep up. It is used to slow down the flow of packets.|

Un autre élément crucial de l'ICMP est le champ [**Time-To-Live**](https://en.wikipedia.org/wiki/Time_to_live) (**TTL**) dans l'en-tête du paquet ICMP qui limite la durée de vie du paquet lorsqu'il voyage à travers le réseau. Il empêche les paquets de circuler indéfiniment sur le réseau en cas de boucles de routage. Chaque fois qu'un paquet passe par un routeur, celui-ci décrémente **la valeur TTL de 1**. Lorsque la valeur TTL atteint **0**, le routeur rejette le paquet et renvoie un message ICMP **Time Exceeded** à l'expéditeur.

Nous pouvons également utiliser le **TTL** pour déterminer le nombre de sauts qu'un paquet a effectués et la distance approximative qui le sépare de sa destination. Par exemple, si un paquet a un **TTL** de 10 et qu'il lui faut 5 sauts pour atteindre sa destination, on peut en déduire que la destination se trouve à environ 5 sauts. Par exemple, si nous voyons un ping avec une valeur **TTL** de **122**, cela peut signifier que nous avons affaire à un système Windows (**TTL 128** par défaut) qui se trouve à 6 sauts.

Cependant, il est également possible de deviner le système d'exploitation en se basant sur la valeur **TTL** par défaut utilisée par l'appareil. Chaque système d'exploitation a généralement une valeur **TTL** par défaut lors de l'envoi de paquets. Cette valeur est définie dans l'en-tête du paquet et est décrémentée de 1 à chaque fois que le paquet passe par un routeur. Par conséquent, l'examen de la valeur **TTL** par défaut d'un appareil permet de déduire le système d'exploitation utilisé par l'appareil. C'est le cas, par exemple, des systèmes Windows (2000/XP/2003) : Les systèmes Windows (**2000/XP/2003/Vista/10**) ont généralement une valeur **TTL** par défaut de 128, tandis que les systèmes macOS et Linux ont généralement une valeur **TTL** par défaut de 64 et la valeur **TTL** par défaut de Solaris de 255. Toutefois, il est important de noter que l'utilisateur peut modifier ces valeurs, qui doivent donc être considérées comme indépendantes d'un moyen définitif de déterminer le système d'exploitation d'un appareil.
Voix sur IP

Le [**Voice over Internet Protocol**](https://www.fcc.gov/general/voice-over-internet-protocol-voip) (**VoIP**) est une méthode de transmission de communications vocales et multimédias. Elle permet par exemple de passer des appels téléphoniques en utilisant une connexion internet à large bande au lieu d'une ligne téléphonique traditionnelle, comme Skype, Whatsapp, Google Hangouts, Slack, Zoom, et d'autres.

Les ports VoIP les plus courants sont **TCP/5060** et **TCP/5061**, qui sont utilisés pour le [**protocole d'initiation de session**](https://en.wikipedia.org/wiki/Session_Initiation_Protocol) (**SIP**). Toutefois, le port **TCP/1720** peut également être utilisé par certains systèmes VoIP pour le [**protocole H.323**](https://en.wikipedia.org/wiki/H.323), un ensemble de normes pour la communication multimédia sur des réseaux à base de paquets. Néanmoins, le protocole SIP est plus largement utilisé que le protocole H.323 dans les systèmes VoIP.

Néanmoins, SIP est un protocole de signalisation permettant d'initier, de maintenir, de modifier et de terminer des sessions en temps réel impliquant la vidéo, la voix, la messagerie et d'autres applications et services de communication entre deux ou plusieurs points d'extrémité sur l'internet. Il utilise donc des requêtes et des méthodes entre les points d'extrémité. Les requêtes et méthodes SIP les plus courantes sont les suivantes :

|Méthode|Description|
|---|---|
|**INVITE**|Initie une session ou invite un autre point d'extrémité à participer.|
|**ACK**|Confirme la réception d'une demande INVITE.|
|**BYE**|Met fin à une session.|
|**CANCEL**|Annule une demande INVITE en attente.|
|**REGISTER**|Enregistre un agent utilisateur (UA) SIP auprès d'un serveur SIP.|
|**OPTIONS**|Demande des informations sur les capacités d'un serveur SIP ou d'un agent utilisateur, telles que les types de médias pris en charge.|
#### Divulgation d'informations

Cependant, le protocole SIP nous permet d'énumérer les utilisateurs existants en vue d'attaques potentielles. Cette opération peut être réalisée à diverses fins, par exemple pour déterminer la disponibilité d'un utilisateur, obtenir des informations sur les capacités ou les services de l'utilisateur, ou effectuer ultérieurement des attaques par force brute sur les comptes d'utilisateurs.

L'une des manières possibles d'énumérer les utilisateurs est la requête SIP **OPTIONS**. Il s'agit d'une méthode utilisée pour demander des informations sur les capacités d'un serveur SIP ou d'agents utilisateurs, telles que les types de médias qu'il prend en charge, les codecs qu'il peut décoder et d'autres détails. La requête **OPTIONS** permet de demander des informations à un serveur SIP ou à un agent utilisateur, ou de tester sa connectivité et sa disponibilité.

Au cours de notre analyse, il est possible de découvrir un fichier **SEPxxxx.cnf**, où **xxxx** est un identifiant unique, est un fichier de configuration utilisé par Cisco Unified Communications Manager, anciennement connu sous le nom de Cisco CallManager, pour définir les réglages et les paramètres d'un téléphone IP Cisco Unified. Le fichier spécifie le modèle du téléphone, la version du micrologiciel, les paramètres du réseau et d'autres détails.

# RÉSEAUX SANS FIL

Les réseaux sans fil sont des réseaux informatiques qui utilisent des connexions de données sans fil entre les nœuds du réseau. Ces réseaux permettent à des appareils tels que les ordinateurs portables, les smartphones et les tablettes de communiquer entre eux et avec l'internet sans avoir besoin de connexions physiques telles que des câbles.

Les réseaux sans fil utilisent la technologie des radiofréquences (**RF**) pour transmettre les données entre les appareils. Chaque appareil d'un réseau sans fil dispose d'un adaptateur sans fil qui convertit les données en signaux **RF** et les envoie par voie hertzienne. Les autres appareils du réseau reçoivent ces signaux avec leurs propres adaptateurs sans fil, et les données sont ensuite reconverties sous une forme utilisable. Les réseaux sans fil peuvent fonctionner sur différentes distances, en fonction de la technologie utilisée. Par exemple, un **réseau local** (**LAN**) qui couvre une petite zone, comme une maison ou un petit bureau, peut utiliser une technologie sans fil appelée **WiFi**, qui a une portée de quelques centaines de pieds. En revanche, un réseau étendu sans fil (**WWAN**) peut utiliser une technologie de télécommunication mobile telle que les données cellulaires (**3G, 4G LTE, 5G**), qui peut couvrir une zone beaucoup plus vaste, telle qu'une ville ou une région entière.

Par conséquent, pour se connecter à un réseau sans fil, un appareil doit se trouver à portée du réseau et être configuré avec les paramètres réseau corrects, tels que le nom du réseau et le mot de passe. Une fois connectés, les appareils peuvent communiquer entre eux et avec l'internet, ce qui permet aux utilisateurs d'accéder à des ressources en ligne et d'échanger des données.

La communication entre les appareils s'effectue par radiofréquence dans les bandes **2,4 GHz** ou **5 GHz** d'un réseau WiFi. Lorsqu'un appareil, tel qu'un ordinateur portable, souhaite envoyer des données sur le réseau, il communique d'abord avec le [point d'accès sans fil](https://en.wikipedia.org/wiki/Wireless_access_point) (**WAP**) (**Wireless access point**) pour demander l'autorisation de transmettre. Le point d'accès sans fil est un dispositif central, comme un routeur, qui connecte le réseau sans fil à un réseau câblé et contrôle l'accès au réseau. Une fois l'autorisation accordée par le WAP, le dispositif de transmission envoie les données sous forme de signaux RF, qui sont reçus par les adaptateurs sans fil des autres dispositifs du réseau. Les données sont ensuite reconverties sous une forme utilisable et transmises à l'application ou au système approprié.

La puissance du signal RF et la distance qu'il peut parcourir sont influencées par des facteurs tels que la puissance de l'émetteur, la présence d'obstacles et la densité du bruit RF dans l'environnement. Pour garantir une communication fiable, les réseaux WiFi utilisent donc des techniques telles que la transmission par étalement de spectre et la correction d'erreurs pour surmonter ces difficultés.
#### Connexion WiFi

L'appareil doit également être configuré avec les paramètres réseau corrects, tels que le nom du réseau / [Service Set Identifier](https://www.geeksforgeeks.org/service-set-identifier-ssid-in-computer-network/) (**SSID**) et le **mot de passe**. Pour se connecter au routeur, l'ordinateur portable utilise un protocole de réseau sans fil appelé [IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11). Ce protocole définit les détails techniques de la communication des appareils sans fil entre eux et avec les WAP. Lorsqu'un appareil souhaite rejoindre un réseau WiFi, il envoie une demande au WAP pour lancer le processus de connexion. Cette demande est connue sous le nom de **trame de demande de connexion** (**connection request frame**) ou de **demande d'association** (**association request**) et est envoyée à l'aide du protocole de réseau sans fil **IEEE 802.11**. La trame de demande de connexion contient divers champs d'information, notamment les suivants, mais sans s'y limiter :

|Champs d'information|Description|
|---|---|
|**Adresse MAC**|Identifiant unique de l'adaptateur sans fil de l'appareil.|
|**SSID**|Nom du réseau, également connu sous le nom de Service Set Identifier du réseau WiFi.|
|**Supported data rates**|Liste des débits de données que l'appareil peut communiquer.|
|**Supported channels**|Liste des canaux (fréquences) sur lesquels l'appareil peut communiquer.|
|**Supported security protocols**|Liste des protocoles de sécurité que l'appareil est capable d'utiliser, tels que WPA2/WPA3.|

L'appareil utilise ensuite ces informations pour configurer son adaptateur sans fil et se connecter au WAP. Une fois la connexion établie, l'appareil peut communiquer avec le WAP et d'autres périphériques réseau. Il peut également accéder à Internet et à d'autres ressources en ligne via le WAP, qui agit comme une passerelle vers le réseau câblé. Toutefois, le **SSID** peut être caché en désactivant la diffusion. Cela signifie que les appareils qui recherchent ce WAP spécifique ne seront pas en mesure d'identifier son **SSID**. Néanmoins, le **SSID** peut toujours être trouvé dans le paquet d'authentification.

Outre le protocole **IEEE 802.11**, d'autres protocoles et technologies de réseau peuvent également être utilisés, comme TCP/IP, DHCP et WPA2, dans un réseau WiFi pour effectuer des tâches telles que l'attribution d'adresses IP aux appareils, l'acheminement du trafic entre les appareils et la fourniture de la sécurité.

####  WEP Challenge-Response Handshake

La poignée de main défi-réponse est un processus qui permet d'établir une connexion sécurisée entre un WAP et un appareil client dans un réseau sans fil qui utilise le protocole de sécurité WEP. Il s'agit d'échanger des paquets entre le WAP et le périphérique client afin d'authentifier le périphérique et d'établir une connexion sécurisée.

|Step|Who|Description|
|---|---|---|
|1|**Le client**|envoie un paquet de demande d'association au WAP pour demander l'accès.|
|2|**Le WAP**|répond par un paquet de réponse d'association au client, qui inclut une chaîne de défi.|
|3|**Le client**|calcule une réponse à la chaîne de défi et une clé secrète partagée et la renvoie au WAP.|
|4|**L'AMP**|calcule la réponse attendue au défi avec la même clé secrète partagée et envoie un paquet de réponse d'authentification au client.|

Néanmoins, certains paquets peuvent se perdre, c'est pourquoi la somme de contrôle **CRC** a été intégrée. Le [contrôle de redondance cyclique](https://en.wikipedia.org/wiki/Cyclic_redundancy_check) (**CRC**) est un mécanisme de détection d'erreurs utilisé dans le protocole WEP pour protéger contre la corruption des données dans les communications sans fil. Une valeur CRC est calculée pour chaque paquet transmis sur le réseau sans fil en fonction des données du paquet. Elle est utilisée pour vérifier l'intégrité des données. Lorsque le dispositif de destination reçoit le paquet, la valeur CRC est recalculée et comparée à la valeur d'origine. Si les valeurs correspondent, les données ont été transmises sans erreur. En revanche, si les valeurs ne correspondent pas, les données ont été corrompues et doivent être retransmises.

La conception du mécanisme **CRC** présente une faille qui nous permet de décrypter un seul paquet **sans connaître la clé de cryptage**. En effet, la valeur **CRC** est calculée à partir des **données en clair** du paquet et non des données cryptées. Dans le WEP, la valeur **CRC** est incluse dans l'en-tête du paquet avec les données cryptées. Lorsque le dispositif de destination reçoit le paquet, la valeur **CRC** est recalculée et comparée à la valeur d'origine pour s'assurer que les données ont été transmises sans erreur. Cependant, nous pouvons utiliser le **CRC** pour déterminer les données en clair dans le paquet, même si les données sont cryptées.

#### Fonctions de sécurité

Les réseaux WiFi sont dotés de plusieurs fonctions de sécurité qui les protègent contre les accès non autorisés et garantissent la confidentialité et l'intégrité des données transmises sur le réseau. Parmi les principales fonctions de sécurité, on peut citer, sans s'y limiter, les suivantes

    le chiffrement
    Contrôle d'accès
    Pare-feu
###### Chiffrement

Nous pouvons utiliser différents algorithmes de cryptage pour protéger la confidentialité des données transmises sur les réseaux sans fil. Les algorithmes de cryptage les plus courants dans les réseaux WiFi sont [Wired Equivalent Privacy](https://en.wikipedia.org/wiki/Wired_Equivalent_Privacy) (**WEP**), [WiFi Protected Access 2](https://en.wikipedia.org/wiki/Wi-Fi_Protected_Access#WPA2) (**WPA2**) et **WiFi Protected Access 3** (**WPA3**).
###### Contrôle d'accès

Les réseaux WiFi sont configurés par défaut pour permettre aux appareils autorisés de rejoindre le réseau à l'aide de méthodes d'authentification spécifiques. Toutefois, ces méthodes peuvent être modifiées en exigeant un mot de passe ou un identifiant unique (tel qu'une adresse MAC) pour identifier les appareils autorisés.
###### Pare-feu

Un pare-feu est un système de sécurité qui contrôle le trafic réseau entrant et sortant sur la base de règles de sécurité prédéterminées. Par exemple, les routeurs WiFi sont souvent équipés de pare-feu intégrés qui peuvent bloquer le trafic entrant en provenance d'Internet et protéger contre divers types de cyber-menaces.

#### Protocoles de cryptage

[Wired Equivalent Privacy](https://en.wikipedia.org/wiki/Wired_Equivalent_Privacy) (**WEP**) et [WiFi Protected Access](https://en.wikipedia.org/wiki/Wi-Fi_Protected_Access) (**WPA**) sont des protocoles de cryptage qui sécurisent les données transmises sur un réseau WiFi. Le WPA peut utiliser différents algorithmes de cryptage, notamment [Advanced Encryption Standard](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) (**AES**).
#### WEP

**WEP** utilise une clé de **40 bits** ou de **104 bits** pour crypter les données, tandis que **WPA utilisant AES** utilise une clé de **128 bits**. Les clés plus longues permettent un cryptage plus robuste et sont plus résistantes aux attaques. Cependant, il est vulnérable à diverses attaques qui peuvent permettre à un pirate de décrypter les données transmises sur le réseau. En outre, le WEP n'est pas compatible avec les nouveaux appareils et systèmes d'exploitation et n'est généralement plus considéré comme sûr. Enfin, le **WEP** utilise l'algorithme de cryptage **RC4 cipher**, ce qui le rend vulnérable aux attaques.

Cependant, le WEP utilise une "clé partagée" pour l'authentification, ce qui signifie que la même clé est utilisée pour le cryptage et l'authentification. Il existe deux versions du protocole WEP :

- **WEP-40**/**WEP-64**
- **WEP-104**

Le **WEP-40**, également connu sous le nom de **WEP-64**, utilise une clé (secrète) de **40 bits**, tandis que le **WEP-104** utilise une clé de **104 bits**. La clé est divisée en un [vecteur d'initialisation] (https://en.wikipedia.org/wiki/Initialization_vector) (**IV**) et une **clé secrète**.

Le **IV** est une petite valeur incluse dans l'en-tête du paquet avec les données cryptées et est utilisé pour créer la clé pour **WEP-40** et **WEP-104** et est inclus pour s'assurer que chaque clé est unique. La clé secrète est une série de bits aléatoires utilisés pour crypter les données. Cependant, le **WEP-104** a une **clé secrète** de 80 bits de long. Examinons le tableau suivant pour voir clairement les différences :

|**Protocol**|**IV**|**Secret Key**|
|---|---|---|
|**WEP-40**/**WEP-64**|24-bit|40-bit|
|**WEP-104**|24-bit|80-bit|

Cependant, comme l'IV du WEP est relativement petit, nous pouvons le forcer brutalement, essayer toutes les combinaisons de caractères possibles et déterminer la valeur correcte. Nous pouvons ensuite l'utiliser pour décrypter les données contenues dans le paquet. Cela nous permet d'accéder aux données transmises par le réseau sans fil et de compromettre potentiellement la sécurité du réseau.

#### WPA

Le WPA offre le niveau de sécurité le plus élevé et n'est pas sensible aux mêmes types d'attaques que le WEP. En outre, le WPA utilise des méthodes d'authentification plus sûres, telles qu'une [Pre-Shared Key](https://en.wikipedia.org/wiki/Pre-shared_key) (**PSK**) ou un serveur d'authentification 802.1X, qui offrent une meilleure protection contre les accès non autorisés. Le WPA est compatible avec la plupart des appareils et des systèmes d'exploitation, même si les anciens appareils ne le prennent pas en charge. Tous les réseaux sans fil, en particulier dans les infrastructures critiques comme les bureaux, devraient généralement mettre en œuvre au moins le cryptage **WPA2** ou même **WPA3**.

#### Protocoles d'authentification

Le [Lightweight Extensible Authentication Protocol](https://en.wikipedia.org/wiki/Lightweight_Extensible_Authentication_Protocol) (**LEAP**) et le [Protected Extensible Authentication Protocol](https://en.wikipedia.org/wiki/Protected_Extensible_Authentication_Protocol) (**PEAP**) sont des protocoles d'authentification utilisés pour sécuriser les réseaux sans fil. Ils fournissent une méthode sûre d'authentification des dispositifs sur un réseau sans fil et sont souvent utilisés en conjonction avec le WEP ou le WPA pour fournir une couche de sécurité supplémentaire.

LEAP et PEAP sont tous deux basés sur le [Extensible Authentication Protocol](https://en.wikipedia.org/wiki/Extensible_Authentication_Protocol) (**EAP**), un cadre d'authentification utilisé dans divers contextes de réseau. Cependant, une différence clé entre **LEAP** et **PEAP** est la façon dont ils sécurisent le processus d'authentification.

- **LEAP** utilise une **clé partagée** pour l'authentification, ce qui signifie que la **même clé** est utilisée pour le **chiffrement et l'authentification**.

Cela peut nous permettre d'accéder assez facilement au réseau si la clé est compromise.

Cependant, **PEAP** utilise une méthode d'authentification plus sûre appelée tunneled [Transport Layer Security](https://en.wikipedia.org/wiki/Transport_Layer_Security) (**TLS**). Cette méthode établit une connexion sécurisée entre l'appareil et le WAP à l'aide d'un "certificat numérique", et un tunnel crypté protège le processus d'authentification. Cette méthode offre une protection plus solide contre les accès non autorisés et est plus résistante aux attaques.

---

#### TACACS+

Dans un réseau sans fil, lorsqu'un point d'accès sans fil (WAP) envoie une demande d'authentification à un serveur [Terminal Access Controller Access-Control System Plus](https://www.ciscopress.com/articles/article.asp?p=422947&seqNum=4) (**TACACS+**), il est probable que le **paquet de demande complet** soit crypté pour protéger la confidentialité et l'intégrité de la demande.

TACACS+ est un protocole utilisé pour authentifier et autoriser les utilisateurs à accéder aux périphériques du réseau, tels que les routeurs et les commutateurs. Lorsqu'un WAP envoie une demande d'authentification à un serveur **TACACS+**, la demande inclut généralement les informations d'identification de l'utilisateur et d'autres informations sur la session.

Le cryptage de la demande d'authentification permet de s'assurer que ces informations sensibles ne sont pas visibles par des parties non autorisées qui pourraient être en mesure d'intercepter la demande. En même temps, elle est transmise sur le réseau. Cela permet également d'empêcher la falsification de la demande ou son remplacement par une demande malveillante de son cru.

Plusieurs méthodes de cryptage peuvent être utilisées pour crypter la demande d'authentification, telles que **SSL**/**TLS** ou **IPSec**. La méthode de cryptage spécifique utilisée peut dépendre de la configuration du serveur **TACACS+** et des capacités du WAP.

#### Attaque de dissociation

Une [attaque de dissociation](https://www.makeuseof.com/what-are-disassociation-attacks/) est un type d'attaque de **tous** les réseaux sans fil qui vise à perturber la communication entre un WAP et ses clients en envoyant des trames de dissociation à un ou plusieurs clients.

Le WAP utilise des trames de dissociation pour déconnecter un client du réseau. Lorsqu'un WAP envoie une trame de dissociation à un client, celui-ci se déconnecte du réseau et doit se reconnecter pour continuer à utiliser le réseau.

Nous pouvons lancer l'attaque depuis l'**intérieur** ou l'**extérieur** du réseau, en fonction de notre emplacement et des mesures de sécurité du réseau. L'objectif de cette attaque est de perturber la communication entre le WAP et ses clients, ce qui entraîne la déconnexion des clients et peut causer des désagréments ou des perturbations pour les utilisateurs. Nous pouvons également l'utiliser comme précurseur d'autres attaques, telles qu'une attaque MITM, en forçant les clients à se reconnecter au réseau et en les exposant potentiellement à d'autres attaques.

#### Durcissement des réseaux sans fil

Il existe de nombreuses façons de protéger les réseaux sans fil. Toutefois, certains exemples doivent être pris en compte pour renforcer considérablement la sécurité des réseaux sans fil. Il s'agit, entre autres, de ce qui suit :

- Désactiver la diffusion
- Accès protégé WiFi
- Filtrage MAC
- Déployer EAP-TLS

###### Désactivation de la diffusion

La désactivation de la diffusion du SSID est une mesure de sécurité qui peut aider à renforcer un WAP en rendant plus difficile la découverte et la connexion au réseau. Lorsque le SSID est diffusé, il est inclus dans les trames de balise régulièrement transmises par le WAP pour annoncer la disponibilité du réseau. En désactivant la diffusion du SSID, le WAP ne transmettra pas de trames de balises et le réseau ne sera pas visible pour les appareils qui ne sont pas déjà connectés au réseau.

###### WPA

Là encore, le WPA fournit un cryptage et une authentification puissants pour les communications sans fil, ce qui permet de se protéger contre l'accès non autorisé au réseau et l'interception de données sensibles. Le WPA comprend deux versions principales :

1. WPA-Personnel
2. WPA-Entreprise

WPA-Personal, conçu pour les réseaux domestiques et les petites entreprises, et WPA-Enterprise, conçu pour les grandes organisations et utilisant un serveur d'authentification centralisé (par exemple, RADIUS ou TACACS+) pour vérifier l'identité du client.

###### Filtrage MAC

Le filtrage MAC est une mesure de sécurité qui permet à un WAP d'accepter ou de rejeter les connexions d'appareils spécifiques en fonction de leur adresse MAC. En configurant le WAP pour qu'il n'accepte que les connexions provenant d'appareils dont l'adresse MAC est approuvée, il est possible d'empêcher les appareils non autorisés de se connecter au réseau.

###### Déploiement d'EAP-TLS

EAP-TLS est un protocole de sécurité utilisé pour authentifier et crypter les communications sans fil. Il utilise des certificats numériques et l'infrastructure à clé publique pour vérifier l'identité des clients et établir des connexions sécurisées. Le déploiement d'EAP-TLS peut aider à renforcer un WAP en fournissant une authentification et un cryptage forts pour les communications sans fil, ce qui peut protéger contre l'accès non autorisé au réseau et l'interception de données sensibles.

# VIRTUAL PRIVATE NETWORKS

Un **réseau privé virtuel** (**VPN**) est une technologie qui permet d'établir une connexion sécurisée et cryptée entre un réseau privé et un appareil distant. La machine distante peut ainsi accéder directement au réseau privé, offrant un accès sécurisé et confidentiel aux ressources et aux services du réseau. Par exemple, un administrateur d'un autre site doit gérer les serveurs internes afin que les employés puissent continuer à utiliser les services internes. De nombreuses entreprises limitent l'accès aux serveurs, de sorte que les clients ne peuvent accéder à ces serveurs qu'à partir du réseau local. C'est là que le VPN entre en jeu : l'administrateur se connecte au serveur VPN via l'internet, s'authentifie et crée ainsi un tunnel crypté de sorte que les autres ne puissent pas lire le transfert de données. En outre, l'ordinateur de l'administrateur se voit attribuer une adresse IP locale (interne) qui lui permet d'accéder aux serveurs internes et de les gérer. Les administrateurs utilisent couramment les VPN pour fournir un accès à distance sécurisé et rentable au réseau de l'entreprise. Le VPN utilise généralement les ports **TCP/1723** pour les connexions VPN [Point-to-Point Tunneling Protocol](https://www.lifewire.com/pptp-point-to-point-tunneling-protocol-818182) **PPTP** et **UDP/500** pour les connexions VPN [IKEv1](https://www.cisco.com/c/en/us/support/docs/security-vpn/ipsec-negotiation-ike-protocols/217432-understand-ipsec-ikev1-protocol.html) et [IKEv2](https://nordvpn.com/blog/ikev2ipsec/).

Cela permet aux employés d'accéder au réseau et à ses ressources, telles que le courrier électronique et les serveurs de fichiers, à partir d'emplacements distants, comme leur domicile ou lors de leurs déplacements. Les administrateurs utilisent les VPN pour plusieurs raisons. Les VPN cryptent la connexion entre l'appareil distant et le réseau privé, ce qui rend beaucoup plus difficile l'interception et le vol d'informations sensibles par des pirates. L'ensemble de la communication est ainsi plus sûre.

Une autre raison est que les VPN permettent aux employés d'accéder au réseau privé et à ses ressources à distance, où qu'ils se trouvent, dès lors qu'ils disposent d'une connexion internet. Cela est particulièrement utile pour les employés qui doivent travailler à distance, comme ceux qui voyagent ou travaillent à domicile. En outre, les VPN peuvent être plus rentables que d'autres solutions d'accès à distance, telles que les lignes louées ou les connexions dédiées, car ils utilisent l'internet public pour connecter les utilisateurs distants au réseau privé.

En outre, nous pouvons utiliser les VPN pour connecter plusieurs sites distants, tels que des succursales, à un seul réseau privé, ce qui facilite la gestion et l'accès aux ressources du réseau. Cependant, plusieurs composants et conditions sont nécessaires pour qu'un VPN fonctionne :

|Requirement|Description|
|---|---|
|**VPN Client**|est installé sur l'appareil distant et est utilisé pour établir et maintenir une connexion VPN avec le serveur VPN. Par exemple, il peut s'agir d'un client OpenVPN.|
|**VPN Server**|Il s'agit d'un ordinateur ou d'un périphérique réseau chargé d'accepter les connexions VPN des clients VPN et d'acheminer le trafic entre les clients VPN et le réseau privé.|
|**Encryption**|Les connexions VPN sont cryptées à l'aide d'une variété d'algorithmes et de protocoles de cryptage, tels que AES et IPsec, afin de sécuriser la connexion et de protéger les données transmises.|
|**Authentication**|Le serveur VPN et le client doivent s'authentifier mutuellement en utilisant un secret partagé, un certificat ou une autre méthode d'authentification pour établir une connexion sécurisée.|

Le client et le serveur VPN utilisent ces ports pour établir et maintenir la connexion VPN. Au niveau de la couche TCP/IP, une connexion VPN utilise généralement le protocole [Encapsulating Security Payload](https://www.ibm.com/docs/en/i/7.4?topic=protocols-encapsulating-security-payload) (**ESP**) pour crypter et authentifier le trafic VPN. Cela permet au client et au serveur VPN d'échanger des données sur l'internet public en toute sécurité.

#### IPsec

[Internet Protocol Security](https://www.cloudflare.com/learning/network-layer/what-is-ipsec/) (**IPsec**) est un protocole de sécurité réseau qui assure le cryptage et l'authentification des communications Internet. Il fonctionne en chiffrant les données utiles de chaque paquet IP et en ajoutant un "en-tête d'authentification" (**AH**), qui est utilisé pour vérifier l'intégrité et l'authenticité du paquet. IPsec utilise une combinaison de deux protocoles pour assurer le cryptage et l'authentification :

1. [Authentication Header](https://www.ibm.com/docs/en/i/7.1?topic=protocols-authentication-header) (**AH**) : Ce protocole assure l'intégrité et l'authenticité des paquets IP mais ne fournit pas de cryptage. Il ajoute un en-tête d'authentification à chaque paquet IP, qui contient une somme de contrôle cryptographique pouvant être utilisée pour vérifier que le paquet n'a pas été altéré.

2. Le protocole [Encapsulating Security Payload](https://www.ibm.com/docs/en/i/7.4?topic=protocols-encapsulating-security-payload) (**ESP**) : Ce protocole assure le cryptage et l'authentification facultative des paquets IP. Il crypte les données utiles de chaque paquet IP et ajoute éventuellement un en-tête d'authentification, similaire à AH.

IPsec peut être utilisé dans deux modes.

|Mode|Description|
|---|---|
|**Transport Mode**|Dans ce mode, IPsec chiffre et authentifie les données utiles de chaque paquet IP, mais ne chiffre pas l'en-tête IP. Ce mode est généralement utilisé pour sécuriser les communications de bout en bout entre deux hôtes.|
|**Tunnel Mode**| Avec ce mode, IPsec chiffre et authentifie l'intégralité du paquet IP, y compris l'en-tête IP. Ce mode est généralement utilisé pour créer un tunnel VPN entre deux réseaux.|

Par exemple, un administrateur peut placer un pare-feu entre les deux. Pour faciliter le trafic VPN IPsec entre un client VPN situé à l'extérieur du pare-feu et un serveur VPN situé à l'intérieur, le pare-feu doit autoriser les protocoles suivants :

|Protocol|Port|Description|
|---|---|---|
|**Internet Protocol** (**IP**)|**UDP/50-51**|Il s'agit du protocole principal qui constitue la base de toutes les communications Internet. Il est utilisé pour acheminer les paquets de données entre le client VPN et le serveur VPN.|
|**Internet Key Exchange** (**IKE**)|**UDP/500**|IKE est un protocole utilisé pour établir et maintenir une communication sécurisée entre le client VPN et le serveur VPN. Il est basé sur l'algorithme d'échange de clés Diffie-Hellman et est utilisé pour négocier et établir des clés secrètes partagées qui peuvent être utilisées pour crypter et décrypter le trafic VPN.|
|**Encapsulating Security Payload**(**ESP**)|**UDP/4500**|ESP est également un protocole qui fournit le cryptage et l'authentification pour les datagrammes IP. Il est utilisé pour crypter le trafic VPN entre le client VPN et le serveur VPN, en utilisant les clés qui ont été négociées avec IKE.|

Ces protocoles sont nécessaires pour faciliter le trafic VPN IPsec car ils fournissent la sécurité et le cryptage requis pour une communication sécurisée sur l'internet public. Sans ces protocoles, le trafic VPN serait vulnérable à l'interception et à la falsification.

#### PPTP

[Point-to-Point Tunneling Protocol](https://www.vpnranks.com/blog/pptp-vs-l2tp/) (**PPTP**) est également un protocole réseau qui permet la création de VPN et fonctionne en établissant un tunnel sécurisé entre le client VPN et le serveur, puis en encapsulant les données transmises dans ce tunnel.

Il s'agit d'une extension du PPTP et il est mis en œuvre dans de nombreux systèmes d'exploitation. En raison de vulnérabilités connues, le PPTP n'est plus considéré comme sûr aujourd'hui. Le PPTP peut être utilisé pour tunneliser des protocoles tels que IP, IPX ou NetBEUI via IP. En raison de vulnérabilités connues, le PPTP n'est plus considéré comme sûr et a été largement remplacé par d'autres protocoles VPN tels que **L2TP/IPsec**, **IPsec/IKEv2**, ou **OpenVPN**. Depuis 2012, cependant, le PPTP n'est plus considéré comme sûr parce que la méthode d'authentification MSCHAPv2 utilise le DES poussiéreux et peut donc être facilement craqué avec du matériel spécial.

# INFORMATIONS SPÉCIFIQUES AU VENDEUR

[Cisco IOS](https://www.cisco.com/c/en/us/products/ios-nx-os-software/ios-technologies/index.html) est le système d'exploitation des périphériques de réseau Cisco tels que les routeurs et les commutateurs. Il fournit les fonctions et les services nécessaires à la gestion et à l'exploitation des périphériques de réseau. Ce système d'exploitation est disponible en différentes versions qui varient en termes de fonctionnalités, de support et de performances. Il offre plusieurs fonctionnalités nécessaires au fonctionnement des réseaux modernes, telles que, mais sans s'y limiter :

- la prise en charge de l'IPv6
- la qualité de service (QoS)
- des fonctions de sécurité telles que le cryptage et l'authentification
- des fonctions de virtualisation telles que le service de réseau local privé virtuel (VPLS)
- le routage et le transfert virtuels (VRF).

Cisco IOS peut être géré de plusieurs manières, en fonction de l'appareil réseau et du matériel utilisé. La méthode la plus couramment utilisée est l'interface de ligne de commande (**CLI**), qui peut également être gérée dans l'interface utilisateur graphique (**GUI**). En outre, il prend en charge divers protocoles et services réseau nécessaires à l'exploitation du réseau. Ces protocoles et services sont les suivants

|Type de protocole|Description|
|---|---|
|**Routing protocols**|tels que [OSPF](https://en.wikipedia.org/wiki/Open_Shortest_Path_First) et [BGP](https://en.wikipedia.org/wiki/Border_Gateway_Protocol) sont utilisés pour acheminer les paquets de données sur un réseau.|
|**Switching protocols**|tels que [VLAN Trunking Protocol](https://en.wikipedia.org/wiki/VLAN_Trunking_Protocol) (**VTP**) et [Spanning Tree Protocol](https://en.wikipedia.org/wiki/Spanning_Tree_Protocol) (**STP**) sont utilisés pour configurer et gérer les commutateurs d'un réseau. Les protocoles de commutation tels que [VLAN Trunking Protocol](https://en.wikipedia.org/wiki/VLAN_Trunking_Protocol) (**VTP**) sont utilisés pour configurer et gérer les commutateurs d'un réseau.|
|**Network services**|tels que le [Dynamic Host Configuration Protocol](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) (**DHCP**) sont utilisés pour fournir automatiquement aux clients du réseau des adresses IP et d'autres configurations réseau.|
|Security features|telles que les [Listes de contrôle d'accès](https://en.wikipedia.org/wiki/Access-control_list) (**ACLs**), qui sont utilisées pour contrôler l'accès aux ressources du réseau et prévenir les menaces à la sécurité.|

Dans Cisco IOS, différents types de mots de passe sont utilisés à diverses fins, par exemple :

|Password Type|Description|
|---|---|
|**User**|Le mot de passe [user](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/security/s1/sec-s1-cr-book/sec-cr-t2.html#wp2992613898) est utilisé pour se connecter à Cisco IOS. Il est utilisé pour restreindre l'accès à l'appareil réseau et à ses fonctions.|
|**Enable Password**|Le [enable password](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/security/d1/sec-d1-cr-book/sec-cr-e1.html#wp3884449514) permet d'accéder au mode "enable". Le mode "enable" est le mode dans lequel vous avez accès aux fonctions et paramètres avancés.|
|**Secret**|Le [secret](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/security/s1/sec-s1-cr-book/sec-cr-s1.html#wp2622423174) est un mot de passe qui permet de sécuriser l'accès à certaines fonctions et à certains services. Il est souvent utilisé pour restreindre l'accès aux outils et services de gestion à distance.|
|**Enable Secret**|Le [enable secret](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/security/d1/sec-d1-cr-book/sec-cr-e1.html#wp3438133060) est un mot de passe extra-sécurisé utilisé pour sécuriser l'accès au mode "activation", et ils sont stockés de manière cryptée pour fournir une protection supplémentaire.|

Nous recommandons vivement de consulter les ressources externes fournies pour comprendre les mécanismes de cryptage de Cisco IOS et la manière dont ils sont utilisés.

Les appareils Cisco IOS peuvent être configurés pour SSH ou Telnet. Il est donc possible d'y accéder à distance. Nous pouvons déterminer à partir de la réponse que nous recevons qu'il s'agit bien d'un Cisco IOS, car il répond avec le message **User Access Verification** (vérification de l'accès utilisateur).

###### Cisco IOS

```shell-session
tchadoyeon@htb[/htb]$ telnet 10.129.10.2

Trying 10.129.10.2...
Connected to 10.129.10.2.
Escape character is '^]'.


User Access Verification

Password:
```

#### VLANs

Un [réseau local virtuel](https://en.wikipedia.org/wiki/VLAN) (**VLAN**) est une connexion LAN virtuelle qui regroupe des dispositifs sur un réseau. Les VLAN sont couramment utilisés pour segmenter le trafic réseau et améliorer les fonctions de sécurité. Nous pouvons configurer et gérer les VLAN dans Cisco IOS et [ajouter des balises VLAN](https://www.practicalnetworking.net/stand-alone/configuring-vlans/) (ID VLAN) aux paquets de données pour les classer dans les VLAN. La balise VLAN est un champ de la trame Ethernet utilisé pour indiquer à quel VLAN appartient un paquet de données. Cisco IOS fait la distinction entre le trafic réseau étiqueté VLAN et le trafic réseau non étiqueté VLAN.

#### VLAN-Tagged

Le trafic réseau étiqueté VLAN contient des étiquettes VLAN et est utilisé pour acheminer et segmenter les paquets de données entre les commutateurs.

#### Non-VLAN-Tagged

Le trafic réseau sans étiquette VLAN, quant à lui, est un trafic réseau qui ne contient pas d'étiquette VLAN et qui est utilisé pour acheminer et segmenter les paquets de données à l'intérieur d'un commutateur. Tout le trafic réseau sans balise VLAN est placé dans le **VLAN1**.

#### Cisco Discovery Protocol

Le protocole de découverte Cisco (CDP) est un protocole réseau de couche 2 de Cisco utilisé par les appareils Cisco tels que les routeurs, les commutateurs et les ponts pour recueillir des informations sur d'autres appareils Cisco directement connectés. Ces informations peuvent être utilisées pour découvrir et suivre la topologie du réseau et aider à gérer et dépanner le réseau. Ce protocole est généralement activé dans les appareils Cisco, mais il peut être désactivé s'il n'est pas nécessaire ou s'il doit être désactivé pour des raisons de sécurité.

###### Trafic réseau CDP

```session shell
22:14:11.563654 CDPv2, ttl : 180s, checksum : 0xebc1 (incorrect -> 0x8b71), length : 180
        Device-ID (0x01), longueur : 14 octets : 'router.inlanefreight.loc'
        Adresses (0x02), longueur : 8 octets :
                IPv4 (0x01), longueur : 4 : 10.129.100.1
        Port-ID (0x03), longueur : 9 octets : 'Ethernet0/0'
        Capacité (0x04), longueur : 4 : (0x00000010) : Router
        Version String (0x05), longueur : 27 octets : 'Cisco IOS Software, C880 Software'
        Platform (0x06), length : 26 bytes : 'Cisco 881 (MPC8300) processor'
```


Le message affiché contient des informations sur l'appareil lui-même, telles que le nom de l'appareil, l'adresse IP, le nom du port et la fonctionnalité du routeur, ainsi que des informations sur le système d'exploitation et la plate-forme matérielle de l'appareil. En outre, nous pouvons voir dans la première ligne du **CDPv2** que nous avons affaire au **Cisco Discovery Protocol**.

À titre de comparaison, nous pouvons examiner un autre protocole appelé Spanning Tree Protocol (**STP**). Le **STP** est un protocole réseau qui garantit qu'il n'y a pas de boucles dans un réseau avec de multiples connexions entre les commutateurs. Il n'y a pas de boucles et il empêche les paquets de données de circuler dans une boucle et d'encombrer le réseau.

###### Trafic réseau STP

```session shell
22:14:11.563654 STP 802.1w, Rapid STP, Flags [Learn, Forward], bridge-id 8001.00:11:22:33:44:55.8000, length 43
        root-id 8001.AA:AA:AA:AA:AA:AA, cost 0, port-id 8001, message-age 0.00s, max-age 20.00s, hello-time 2.00s, forward-delay 15.00s
```

Dans cet exemple, nous voyons qu'un message **STP** a été envoyé contenant des informations sur le commutateur racine, l'adresse MAC du commutateur racine, l'ID du port sur lequel le message a été envoyé, et d'autres paramètres de configuration tels que le temps de vieillissement maximum (maximum aging time), le temps de bonjour (hello time), et le délai d'acheminement (forward delay).


# MÉCANISMES D'ÉCHANGE DE CLÉS
Les méthodes d'échange de clés sont utilisées pour échanger des [clés cryptographiques](https://www.cloudflare.com/learning/ssl/what-is-a-cryptographic-key/) entre deux parties en toute sécurité. Il s'agit d'un élément essentiel de nombreux protocoles cryptographiques, car la sécurité du cryptage utilisé pour protéger la communication repose sur le secret des clés. Il existe de nombreuses méthodes d'échange de clés, chacune ayant des caractéristiques et des atouts uniques. Certaines méthodes d'échange de clés sont plus sûres que d'autres, et la méthode appropriée dépend des circonstances et des exigences spécifiques de la situation.

Ces méthodes fonctionnent généralement en permettant aux deux parties de se mettre d'accord sur une **clé secrète partagée** par le biais d'un canal de communication non sécurisé qui crypte la communication entre elles. Pour ce faire, on utilise généralement une forme d'opération mathématique, telle qu'un calcul basé sur les propriétés d'une fonction mathématique ou une série de manipulations simples de la clé.
#### Diffie-Hellman

Une méthode courante d'échange de clés est l'échange de clés [Diffie-Hellman](https://www.comparitech.com/blog/information-security/diffie-hellman-key-exchange/), qui permet à deux parties de se mettre d'accord sur une clé secrète partagée sans communication préalable ni partage d'informations privées. Elle repose sur le concept de deux parties générant une clé secrète partagée qui peut être utilisée pour crypter et décrypter des messages entre elles. Il est souvent utilisé comme base pour établir des canaux de communication sécurisés, comme dans le protocole [Transport Layer Security](https://www.cloudflare.com/learning/ssl/transport-layer-security-tls/) (**TLS**) qui est utilisé pour protéger le trafic web.

L'une des principales limites de l'échange de clés **Diffie-Hellman** est qu'il est vulnérable aux **attaques MITM**. Dans une attaque MITM, nous interceptons la communication entre les deux parties et nous nous faisons passer pour l'une d'entre elles, en générant une clé secrète différente et en trompant les deux parties pour qu'elles l'utilisent. Cela permet à l'attaquant de lire et de modifier les messages envoyés entre les parties.

Une autre limitation est que la génération de la clé secrète partagée nécessite une quantité relativement importante de **puissance PC**. Cela peut rendre son utilisation peu pratique dans les appareils de faible puissance ou dans les situations où la clé doit être générée rapidement.

#### RSA

Une autre méthode d'échange de clés est l'algorithme [Rivest-Shamir-Adleman](https://www.venafi.com/blog/how-diffie-hellman-key-exchange-different-rsa) (**RSA**), qui utilise les propriétés des grands nombres premiers pour générer une clé secrète partagée. Cette méthode repose sur le fait qu'il est relativement facile de multiplier de grands nombres premiers entre eux, mais qu'il est difficile de ramener le nombre résultant à ses facteurs premiers. Outre ces deux méthodes, il en existe d'autres que nous devons examiner. Il est également largement utilisé dans de nombreuses autres applications et protocoles qui requièrent une communication sécurisée et une protection des données, y compris, mais sans s'y limiter, dans les domaines suivants

- le chiffrement et la signature des messages pour assurer la confidentialité et l'authentification

- la protection des données en transit sur les réseaux, comme dans les protocoles [Secure Socket Layer](https://www.cloudflare.com/learning/ssl/what-is-ssl/) (**SSL**) et **TLS**.

- Générer et vérifier les signatures numériques, qui sont utilisées pour assurer l'authenticité et l'intégrité des documents électroniques et d'autres données numériques.

- l'authentification des utilisateurs et des dispositifs, comme dans le protocole [Public Key Cryptography for Initial Authentication in Kerberos](https://www.ietf.org/rfc/rfc4556.txt) (**PKINIT**) utilisé par le système d'authentification du réseau Kerberos

- la protection des informations sensibles, comme le cryptage des données personnelles et des documents confidentiels.


#### ECDH

[Elliptic curve Diffie-Hellman](https://medium.com/swlh/understanding-ec-diffie-hellman-9c07be338d4a) (**ECDH**) est une variante de l'échange de clés Diffie-Hellman qui utilise la cryptographie à courbe elliptique pour générer la clé secrète partagée. Il présente l'avantage d'être plus efficace et plus sûr que l'algorithme Diffie-Hellman original, notamment en ce qui concerne les aspects suivants

- l'établissement de canaux de communication sécurisés, comme dans le protocole TLS

- assurer la confidentialité des communications passées, ce qui garantit qu'elles ne peuvent pas être révélées même si les clés privées sont compromises

- l'authentification des utilisateurs et des appareils, comme dans le protocole [Internet Key Exchange](https://docs.oracle.com/cd/E19683-01/816-7264/6md9iem1g/index.html) (**IKE**) utilisé dans les VPN.


#### ECDSA

L'[Algorithme de signature numérique à courbe elliptique](https://www.hypr.com/security-encyclopedia/elliptic-curve-digital-signature-algorithm) (**ECDSA**) utilise la cryptographie à courbe elliptique pour générer des signatures numériques qui peuvent authentifier les parties impliquées dans l'échange de clés.

Résumons et comparons ces algorithmes :

|Algorithm|Acronym|Security|
|---|---|---|
|Diffie-Hellman|**DH**|Relativement sûr et efficace sur le plan informatique|
|Rivest-Shamir-Adleman|**RSA**|Largement utilisé et considéré comme sûr, mais à forte intensité de calcul.|
|Diffie-Hellman à courbes elliptiques|**ECDH**|offre une sécurité accrue par rapport au Diffie-Hellman traditionnel.|
|Algorithme de signature numérique à courbe elliptique|**ECDSA**|Affiche une sécurité et une efficacité accrues pour la génération de signatures numériques|

## Internet Key Exchange

Le [Internet Key Exchange](https://www.hypr.com/security-encyclopedia/internet-key-exchange) (**IKE**) est un protocole utilisé pour établir et maintenir des sessions de communication sécurisées, telles que celles utilisées dans les VPN. Il utilise une combinaison de l'algorithme d'échange de clés **Diffie-Hellman** et **d'autres techniques cryptographiques** pour échanger des clés en toute sécurité et négocier des paramètres de sécurité. Il s'agit d'ailleurs d'un élément clé de nombreuses solutions VPN, car il permet l'échange sécurisé de clés et d'autres informations de sécurité entre le client et le serveur VPN. Cela permet au VPN d'établir un tunnel crypté à travers lequel les données peuvent être transmises en toute sécurité.

IKE peut également être utilisé à d'autres fins, comme l'authentification des utilisateurs et des appareils. Il est généralement utilisé en conjonction avec d'autres protocoles et algorithmes, tels que l'algorithme RSA pour l'échange de clés et les signatures numériques, et le [Advanced Encryption Standard](https://www.geeksforgeeks.org/advanced-encryption-standard-aes/) (**AES**) pour le cryptage des données.

IKE fonctionne soit en "mode principal", soit en "mode agressif". Ces modes déterminent la séquence et les paramètres du processus d'échange de clés et peuvent affecter la sécurité et les performances de la session IKE.

#### Mode principal

Le **main mode** est le mode par défaut de **IKE** et est généralement considéré comme **plus sûr** que le mode agressif. Le processus d'échange de clés est réalisé en trois phases dans le mode principal, chacune échangeant un ensemble différent de paramètres de sécurité et de clés. Cela permet une plus grande flexibilité et une meilleure sécurité, mais peut également entraîner des performances plus lentes par rapport au mode agressif.

#### Mode agressif

Le **mode agressif** est un mode alternatif pour **IKE** qui fournit des performances plus rapides en réduisant le nombre d'allers-retours et d'échanges de messages nécessaires pour l'échange de clés. Dans ce mode, le processus d'échange de clés est effectué en deux phases, tous les paramètres de sécurité et les clés étant échangés au cours de la première phase. Ce mode permet d'obtenir des performances plus rapides mais peut également réduire la sécurité de la session IKE par rapport au mode principal, car le **mode agressif** n'assure pas la protection de l'identité.

#### Clés pré-partagées

Dans IKE, une **Pre-Shared Key** (**PSK**) est une valeur secrète partagée entre les deux parties impliquées dans l'échange de clés. Cette clé est utilisée pour authentifier les parties et établir un secret partagé qui crypte les communications ultérieures. L'utilisation d'une PSK est facultative dans l'IKE, et la décision d'en utiliser une ou non dépend des exigences et des contraintes spécifiques de la situation. Cependant, si un **PSK** est utilisé, il doit être échangé de manière sécurisée entre les deux parties avant que le processus d'échange de clés ne commence. Cela peut se faire par le biais d'un canal sécurisé hors bande, tel qu'un canal de communication séparé, ou par l'échange physique de la clé.

Le principal avantage de l'utilisation d'une PSK est qu'elle fournit une couche de sécurité supplémentaire en permettant aux parties de s'authentifier l'une l'autre. Cependant, l'utilisation d'une PSK présente également des limites et des inconvénients. Par exemple, il peut être difficile d'échanger la clé en toute sécurité, et si la clé est compromise par une attaque MITM, la sécurité de la session IKE peut être compromise.

# PROTOCOLES D'AUTHENTIFICATION


De nombreux protocoles d'authentification sont utilisés dans les réseaux pour vérifier l'identité des appareils et des utilisateurs. Ces protocoles sont essentiels car ils fournissent un moyen sûr et normalisé de "vérifier l'identité" des utilisateurs, des appareils et des autres entités d'un réseau. Sans protocoles d'authentification, il serait difficile d'identifier de manière sûre et fiable les entités d'un réseau, ce qui permettrait aux pirates d'obtenir facilement un accès non autorisé et de compromettre potentiellement le réseau.

Les protocoles d'authentification fournissent également un moyen d'échanger des informations en toute sécurité entre les entités d'un réseau, ce dont nous parlerons brièvement. Cela est important pour garantir la confidentialité et l'intégrité des données sensibles et pour empêcher les accès non autorisés et d'autres menaces pour la sécurité. Examinons les protocoles d'authentification les plus couramment utilisés :

|Protocole|Description|
|---|---|
|**Kerberos**|protocole d'authentification basé sur le centre de distribution de clés (KDC) qui utilise des tickets dans les environnements de domaine.|
|**SRP**|Il s'agit d'un protocole d'authentification basé sur un mot de passe qui utilise la cryptographie pour se protéger contre les écoutes et les attaques de type " man-in-the-middle ".|
|**SSL**|est un protocole cryptographique utilisé pour les communications sécurisées sur un réseau informatique.|
|**TLS**|est un protocole cryptographique qui assure la sécurité des communications sur Internet. C'est le successeur de SSL.|
|**OAuth**|est une norme ouverte d'autorisation qui permet aux utilisateurs d'accorder à des tiers l'accès à leurs ressources web sans partager leurs mots de passe.|
|**OpenID**|est un protocole d'authentification décentralisé qui permet aux utilisateurs d'utiliser une seule identité pour se connecter à plusieurs sites web.|
|**SAML**|(Security Assertion Markup Language) est une norme basée sur XML pour l'échange sécurisé de données d'authentification et d'autorisation entre les parties.| Le "SAML" est une norme basée sur XML pour l'échange sécurisé de données d'authentification et d'autorisation entre les parties.|
|**2FA**|Le système d'authentification de l'utilisateur est une méthode d'authentification qui utilise une combinaison de deux facteurs différents pour vérifier l'identité d'un utilisateur.|
|**FIDO**|La Fast IDentity Online Alliance est un consortium d'entreprises qui travaillent à l'élaboration de normes ouvertes pour l'authentification forte.|
|**PKI**|est un système d'échange sécurisé d'informations basé sur l'utilisation de clés publiques et privées pour le cryptage et les signatures numériques.|
|**SSO**|L'authentification forte est une méthode d'authentification qui permet à un utilisateur d'utiliser un seul ensemble d'informations d'identification pour accéder à plusieurs applications.|
|**MFA**|est une méthode d'authentification qui utilise plusieurs facteurs, tels que quelque chose que l'utilisateur connaît (un mot de passe), quelque chose que l'utilisateur possède (un téléphone), ou quelque chose que l'utilisateur est (des données biométriques), pour vérifier son identité.|
|**PAP**|est un protocole d'authentification simple qui envoie le mot de passe de l'utilisateur en clair sur le réseau.|
|**CHAP**|Protocole d'authentification qui utilise une poignée de main à trois voies pour vérifier l'identité d'un utilisateur.|
|**EAP**|cadre de prise en charge de plusieurs méthodes d'authentification, permettant l'utilisation de diverses technologies pour vérifier l'identité d'un utilisateur.|
|**SSH**|Il s'agit d'un protocole réseau pour la communication sécurisée entre un client et un serveur. Il peut être utilisé pour l'accès à la ligne de commande à distance et l'exécution de commandes à distance, ainsi que pour le transfert sécurisé de fichiers. SSH utilise le cryptage pour se protéger contre les écoutes et autres attaques et peut également être utilisé pour l'authentification.|
|**HTTPS**|Il s'agit d'une version sécurisée du protocole HTTP utilisé pour la communication sur Internet. HTTPS utilise SSL/TLS pour crypter les communications et fournir une authentification, garantissant que des tiers ne peuvent pas intercepter et lire les données transmises. Il est largement utilisé pour les communications sécurisées sur l'internet, en particulier pour la navigation sur le web.|
|**LEAP**|est un protocole d'authentification sans fil développé par Cisco. Il utilise EAP pour fournir une authentification mutuelle entre un client sans fil et un serveur et utilise l'algorithme de cryptage RC4 pour crypter la communication entre les deux. Malheureusement, LEAP est vulnérable aux attaques par dictionnaire et à d'autres failles de sécurité et a été largement remplacé par des protocoles plus sûrs tels que EAP-TLS et PEAP.|
|**PEAP**|est un protocole de tunnel sécurisé utilisé pour les réseaux sans fil et câblés. Il est basé sur EAP et utilise TLS pour crypter la communication entre un client et un serveur. PEAP utilise un certificat côté serveur pour authentifier le serveur et peut également être utilisé pour authentifier le client à l'aide de différentes méthodes, telles que des mots de passe, des certificats ou des données biométriques. PEAP est largement utilisé dans les réseaux d'entreprise pour l'authentification sécurisée.|

Par exemple, LEAP et PEAP peuvent être utilisés pour authentifier les clients sans fil lorsqu'ils accèdent au réseau sans fil ou pour authentifier les employés distants qui se connectent au réseau via un VPN. Dans ces cas, l'utilisation de SSH ou de HTTPS serait difficile à mettre en œuvre, car ils sont conçus pour des objectifs différents. En termes de sécurité, **PEAP** est généralement plus sûr que LEAP car il utilise un certificat de clé publique côté serveur pour authentifier le serveur et crypter le hachage **MSCHAPv2**, tandis que **LEAP** repose sur un secret partagé qui est négocié entre le client et le serveur et ne crypte pas les hachages **MSCHAPv2**. PEAP prend également en charge l'utilisation d'algorithmes de cryptage plus puissants, tels que AES et 3DES, pour le cryptage des informations d'authentification, tandis que LEAP utilise l'algorithme RC4, plus faible.

Cependant, ces deux protocoles sont vulnérables à des attaques spécifiques et ont été largement remplacés par des protocoles plus sûrs tels que EAP-TLS.

Pour les connexions physiques, les protocoles avec **SSL** ou **TLS** sont utilisés par défaut, comme **SSH** ou **HTTPS**. Ces deux protocoles utilisent des algorithmes de cryptage robustes pour protéger les informations d'authentification transmises entre le client et le serveur. Cela permet d'empêcher l'interception ou la falsification des données d'authentification, ce qui est essentiel pour maintenir la sécurité du processus d'authentification. Ils permettent également d'utiliser des certificats numériques et des **PKI** pour authentifier le serveur auprès du client. Cela garantit que le client peut vérifier l'identité du serveur et aide à prévenir les attaques MITM. SSH et HTTPS sont des protocoles largement utilisés et bien supportés, avec des implémentations disponibles pour divers systèmes d'exploitation et dispositifs, ce qui les rend faciles à utiliser et à déployer dans une variété d'environnements.

# CONNEXIONS TCP/UDP

Le [Transmission Control Protocol](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) (**TCP**) et le [User Datagram Protocol](https://en.wikipedia.org/wiki/User_Datagram_Protocol) (**UDP**) sont deux protocoles utilisés pour la transmission d'informations et de données sur Internet. En règle générale, les connexions TCP transmettent des données importantes, telles que des pages web et des courriers électroniques. En revanche, les connexions UDP transmettent des données en temps réel, comme la vidéo en continu ou les jeux en ligne.

Le protocole TCP est un protocole orienté connexion qui garantit que toutes les données envoyées d'un ordinateur à un autre sont reçues. C'est comme une conversation téléphonique où les deux parties restent connectées jusqu'à ce que l'appel soit terminé. Si une erreur se produit lors de l'envoi de données, le destinataire renvoie un message afin que l'expéditeur puisse renvoyer les données manquantes. Le protocole **TCP** est donc fiable, mais il est plus lent que le protocole UDP, car il faut plus de temps pour la transmission et la récupération des erreurs.

L'UDP, quant à lui, est un protocole sans connexion. Il est utilisé lorsque la vitesse est plus importante que la fiabilité, par exemple pour le streaming vidéo ou les jeux en ligne. Avec **UDP**, il n'y a pas de vérification que les données reçues sont complètes et sans erreur. Si une erreur se produit lors de l'envoi de données, le destinataire ne recevra pas les données manquantes et aucun message ne sera envoyé pour les renvoyer. Certaines données peuvent être perdues avec **UDP**, mais la transmission globale est plus rapide.

## Paquet IP

Un paquet [Internet Protocol](https://en.wikipedia.org/wiki/Internet_Protocol) (**IP**) est la zone de données utilisée par la couche réseau du modèle [Open Systems Interconnection](https://en.wikipedia.org/wiki/OSI_model) (**OSI**) pour transmettre des données d'un ordinateur à un autre. Il se compose d'un en-tête et de la charge utile, c'est-à-dire des données de la charge utile proprement dite.

Nous pouvons également considérer le paquet IP comme une lettre envoyée dans une enveloppe. L'enveloppe contient l'en-tête, qui comprend des informations sur l'expéditeur et le destinataire, ainsi que des instructions pour l'acheminement de la lettre, c'est-à-dire les bureaux de poste par lesquels la lettre doit être envoyée. La lettre elle-même est la charge utile, c'est-à-dire les données de la charge utile proprement dite.

#### En-tête IP

L'en-tête d'un paquet IP contient plusieurs champs contenant des informations importantes.

|Field|Description|
|---|---|
|**Version**|Indique la version du protocole IP utilisée.|
|**Internet Header Length**|Indique la taille de l'en-tête en mots de 32 bits.|
|**Class service**|signifie l'importance de la transmission des données.|
|**Total length**|Indique la longueur totale du paquet en octets.|
|**Identification (ID)**|est utilisée pour identifier les fragments du paquet lorsqu'il est fragmenté en plus petites parties.|
|**Flags**|utilisé pour indiquer la fragmentation|
|**L'offset du fragment**|indique l'endroit où le fragment actuel est placé dans le paquet.|
|**Time to Live**|indique combien de temps le paquet peut rester sur le réseau.|
|**Protocol**|spécifie le protocole utilisé pour transmettre les données, tel que TCP ou UDP.|
|**Checksum**|est utilisée pour détecter les erreurs dans l'en-tête.|
|**Source/Destination**|Indique d'où le paquet a été envoyé et où il est envoyé.|
|**Options**|contient des informations facultatives pour le routage.|
|**Padding**|permet d'ajouter un mot à la longueur du paquet.|

Il peut arriver qu'un ordinateur ait plusieurs adresses IP dans différents réseaux. Dans ce cas, nous devons prêter attention au champ **IP ID**. Il est utilisé pour identifier les fragments d'un paquet IP lorsqu'il est fragmenté en parties plus petites. Il s'agit d'un champ de 16 bits avec un numéro unique compris entre 0 et 65535.

Si un ordinateur possède plusieurs adresses IP, le champ **IP ID** sera différent pour chaque paquet envoyé par l'ordinateur mais très similaire. Dans TCPdump, le trafic réseau peut ressembler à ceci :

#### Reniflage de réseau

```session shell
IP 10.129.1.100.5060 > 10.129.1.1.5060 : SIP, length : 1329, id 1337
IP 10.129.1.100.5060 > 10.129.1.1.5060 : SIP, longueur : 1329, id 1338
IP 10.129.1.100.5060 > 10.129.1.1.5060 : SIP, longueur : 1329, id 1339
IP 10.129.2.200.5060 > 10.129.1.1.5060 : SIP, longueur : 1329, id 1340
IP 10.129.2.200.5060 > 10.129.1.1.5060 : SIP, longueur : 1329, id 1341
IP 10.129.2.200.5060 > 10.129.1.1.5060 : SIP, length : 1329, id 1342
```

Nous pouvons voir dans la sortie que deux adresses IP différentes envoient des paquets à l'adresse IP 10.129.1.1. Cependant, à partir de l'**IP ID**, nous pouvons voir que les paquets sont continus. Cela indique clairement que les deux adresses IP appartiennent au même hôte sur le réseau.

#### Champ Record-Route de l'IP

Le champ **Record-Route** de l'en-tête IP enregistre également la route vers un appareil de destination. Lorsque l'appareil de destination renvoie le paquet **ICMP Echo Reply**, les adresses IP de tous les appareils qui passent par le paquet sont listées dans le champ **Record-Route** de l'en-tête IP. C'est ce qui se passe lorsque nous utilisons la commande suivante, par exemple :

```session shell
tchadoyeon@htb[/htb]$ ping -c 1 -R 10.129.143.158

PING 10.129.143.158 (10.129.143.158) 56(124) octets de données.
64 octets en provenance de 10.129.143.158 : icmp_seq=1 ttl=63 time=11.7 ms
RR : 10.10.14.38
        10.129.0.1
        10.129.143.158
        10.129.143.158
        10.10.14.1
        10.10.14.38


--- 10.129.143.158 statistiques ping ---
1 paquets transmis, 1 reçu, 0% de perte de paquets, temps 0ms
rtt min/avg/max/mdev = 11.688/11.688/11.688/0.000 ms
```

La sortie indique qu'une requête **ping** a été envoyée et qu'une réponse a été reçue du périphérique de destination et montre également le champ **Record-Route** dans l'en-tête IP du paquet **ICMP Echo Request**. Le champ Record-Route contient les adresses IP de tous les appareils qui ont traversé le paquet **ICMP Echo Request** sur le chemin de l'appareil de destination. Dans ce cas, le champ **Record-Route** contient les adresses IP :

| | | |
|---|---|---|
|10.10.14.38|10.129.0.1|10.129.143.158|
|10.129.143.158|10.10.14.1|10.10.14.38|


L'outil **traceroute** peut également être utilisé pour tracer la route vers une destination avec plus de précision. Il utilise la méthode du délai d'attente TCP pour déterminer quand la route a été complètement tracée.

1. Nous envoyons un paquet TCP SYN au dispositif de destination avec un TTL de 1 dans l'en-tête IP.
    Lorsque le paquet TCP SYN avec un TTL supérieur à 1 atteint un routeur, la valeur du TTL est diminuée de 1 et le paquet est transmis à l'appareil suivant. Si le paquet TCP SYN avec un TTL de 1 atteint un routeur, le paquet est abandonné et le routeur nous renvoie un paquet ICMP Time-Exceeded.

2. Nous recevons le paquet ICMP Time-Exceeded et notons l'adresse IP du routeur qui a envoyé le paquet.

3. Ensuite, nous envoyons un autre paquet TCP SYN à la destination, en augmentant le TTL de 1.

Le processus se répète jusqu'à ce que le paquet TCP SYN atteigne l'hôte de destination et reçoive une réponse **TCP SYN/ACK** ou **TCP RST** de la part de la cible. Une fois que nous avons reçu une réponse du périphérique de destination, nous savons que nous avons tracé la route jusqu'à la destination et nous terminons le processus de traceroute.

#### Charge utile IP

La charge utile (également appelée **données IP**) est la charge utile réelle du paquet. Elle contient les données des différents protocoles, tels que TCP ou UDP, qui sont transmises, tout comme le contenu de la lettre dans l'enveloppe.

## TCP

Les paquets TCP, également appelés **segments**, sont divisés en plusieurs sections appelées **headers** (en-têtes) et **payloads** (charges utiles). Les segments TCP sont enveloppés dans le paquet IP envoyé.

L'en-tête contient plusieurs champs contenant des informations importantes. Le port source indique l'ordinateur à partir duquel le paquet a été envoyé. Le port de destination indique l'ordinateur auquel le paquet est envoyé. Le numéro de séquence indique l'ordre dans lequel les données ont été envoyées. Le numéro de confirmation est utilisé pour confirmer que toutes les données ont été reçues avec succès. Les drapeaux de contrôle indiquent si le paquet marque la fin d'un message, s'il s'agit d'un accusé de réception de données ou s'il contient une demande de répétition de données. La taille de la fenêtre indique la quantité de données que le récepteur peut recevoir. La somme de contrôle est utilisée pour détecter les erreurs dans l'en-tête et la charge utile. Le pointeur urgent avertit le récepteur que des données importantes se trouvent dans la charge utile.

La charge utile est le contenu réel du paquet et contient les données transmises, tout comme le contenu d'une conversation entre deux personnes.

## UDP

UDP transfère des **datagrammes** (petits paquets de données) entre deux hôtes. Il s'agit d'un protocole sans connexion, ce qui signifie qu'il n'est pas nécessaire d'établir une connexion entre l'expéditeur et le destinataire avant d'envoyer des données. Au lieu de cela, les données sont envoyées directement à l'hôte cible sans aucune connexion préalable.

Lorsque **traceroute** est utilisé avec UDP, nous recevons un message **Destination inaccessible** et **Port inaccessible** lorsque le paquet de datagrammes UDP atteint le dispositif cible. En général, les paquets UDP sont envoyés en utilisant **traceroute** sur les hôtes Unix.

---

## Spoofing aveugle

Le **Blind spoofing** est une méthode d'attaque par manipulation de données dans laquelle un attaquant envoie de fausses informations sur un réseau sans voir les réponses réelles renvoyées par les appareils cibles. Il s'agit de manipuler le champ de l'en-tête IP pour indiquer de fausses adresses de source et de destination. Par exemple, nous envoyons un paquet TCP à l'hôte cible avec de faux numéros de port source et de destination et un faux **numéro de séquence initial** (**ISN**). L'**ISN** est un champ de l'en-tête TCP utilisé pour spécifier le numéro de séquence du premier paquet TCP dans une connexion. L'ISN est défini par l'expéditeur d'un paquet TCP et envoyé au destinataire dans l'en-tête TCP du premier paquet. Cela peut amener l'hôte cible à établir une connexion avec nous sans la recevoir.

Cette attaque est généralement utilisée pour perturber l'intégrité des connexions réseau ou pour rompre les connexions entre les périphériques du réseau. Elle peut également être utilisée pour surveiller le trafic réseau ou pour intercepter des informations envoyées par des périphériques réseau.


# CRYPTOGRAPHIE

Le cryptage est utilisé sur Internet pour transmettre des données, telles que des informations de paiement, des courriers électroniques ou des données personnelles, de manière confidentielle et à l'abri de toute manipulation. Les données sont cryptées à l'aide de différents algorithmes cryptographiques basés sur des opérations mathématiques. Grâce au cryptage, les données peuvent être transformées en une forme que les personnes non autorisées ne peuvent plus lire. Pour le cryptage, on utilise des clés numériques dans les processus de cryptage **symétrique** ou **asymétrique**. Selon les méthodes de cryptage utilisées, il est plus facile de déchiffrer les textes cryptés ou les clés. Si l'on utilise des méthodes cryptographiques de pointe avec des longueurs de clés importantes, elles fonctionnent de manière très sûre et sont pratiquement impossibles à compromettre à l'heure actuelle. En principe, on peut faire une distinction entre les techniques de cryptage **symétriques** et **asymétriques**. Les méthodes asymétriques ne sont connues que depuis quelques décennies. Néanmoins, ce sont les méthodes les plus fréquemment utilisées dans la communication numérique.

#### Chiffrement symétrique

Le cryptage symétrique, également connu sous le nom de cryptage à clé secrète, est une méthode qui utilise la même clé pour crypter et décrypter les données. Cela signifie que l'expéditeur et le destinataire doivent avoir la même clé pour décrypter correctement les données.

Si la clé secrète est partagée ou perdue, la sécurité des données n'est plus garantie. Les actions critiques pour les méthodes de cryptage symétrique concernent la distribution, le stockage et l'échange des clés. L'[Advanced Encryption Standard](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) (**AES**) et le [Data Encryption Standard](https://en.wikipedia.org/wiki/Data_Encryption_Standard) (**DES**) sont des exemples d'algorithmes de cryptage symétrique. Ce type de cryptage est souvent utilisé pour crypter de grandes quantités de données, telles que des fichiers sur un disque dur ou des données envoyées sur un réseau. AES est considéré comme l'algorithme de cryptage le plus sûr à l'heure actuelle.

#### Chiffrement asymétrique

Le chiffrement asymétrique, également connu sous le nom de **chiffrement à clé publique**, est une méthode de chiffrement qui utilise deux clés différentes :

- une **clé publique**
- une **clé privée**

La clé publique est utilisée pour crypter les données, tandis que la clé privée est utilisée pour décrypter les données. Cela signifie que n'importe qui peut utiliser une clé publique pour crypter des données pour quelqu'un, mais que seul le destinataire possédant la clé privée associée peut décrypter les données. Parmi les méthodes de chiffrement asymétrique, on peut citer [Rivest-Shamir-Adleman](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) (**RSA**), [Pretty Good Privacy](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) (**PGP**), et [Elliptic Curve Cryptography](https://en.wikipedia.org/wiki/Elliptic-curve_cryptography) (**ECC**). Le chiffrement asymétrique est utilisé dans une variété d'applications, dont voici quelques exemples :

| | | |
|---|---|---|
|Signatures électroniques|SSL/TLS|VPNs|
|SSH|PKI|Cloud|

## Chiffrement à clé publique

L'un des avantages du cryptage asymétrique est sa **sécurité**. Comme la sécurité est basée sur des problèmes mathématiques très difficiles à résoudre, des attaques simples ne peuvent pas la casser. En outre, le problème de l'échange de clés est contourné. Il s'agit d'un problème important pour les méthodes de cryptage symétrique. Cependant, comme la clé publique peut être accessible à tous, il n'est pas nécessaire d'échanger des clés secrètement. En outre, les méthodes asymétriques ouvrent la voie à l'authentification au moyen de signatures numériques.

## Data Encryption Standard

Le DES est un algorithme de chiffrement par blocs à clé symétrique, et son chiffrement fonctionne comme une combinaison des algorithmes de chiffrement par blocs à usage unique, de permutation et de substitution appliqués à des séquences de bits. Il utilise la même clé pour le chiffrement et le déchiffrement des données.

La clé se compose de **64 bits**, dont **8 bits** sont utilisés comme somme de contrôle. Par conséquent, la longueur réelle de la clé du DES n'est que de 56 bits. C'est pourquoi on parle toujours d'une longueur de clé de 56 bits lorsqu'on évoque le DES. Pour éviter le danger de l'analyse de fréquence, ce ne sont pas des lettres individuelles, mais chaque bloc de 64 bits de texte en clair qui est crypté en un bloc de 64 bits de texte chiffré.

Une extension du DES est ce que l'on appelle le [Triple DES / 3DES](https://en.wikipedia.org/wiki/Triple_DES), qui permet de crypter les données de manière plus sûre. La procédure consiste généralement en trois clés, la première étant utilisée pour crypter les données, la deuxième pour les décrypter et la troisième pour les crypter à nouveau.

Le **3DES** a été considéré comme plus sûr que le DES original parce qu'il offre une plus grande sécurité en utilisant trois tours de cryptage, bien que l'utilisation d'une clé de 56 bits le limite toujours. AES, le successeur de DES, offre une plus grande sécurité en utilisant des clés plus longues et est maintenant la technologie de cryptage symétrique la plus utilisée.

## Advanced Encryption Standard

Par rapport à DES, AES utilise des clés de 128 bits (**AES-128**), 192 bits (**AES-192**) ou 256 bits (**AES-256**) pour crypter et décrypter les données. En outre, l'AES est plus rapide que le DES car sa structure algorithmique est plus efficace. En effet, il peut être appliqué à plusieurs blocs de données à la fois, ce qui le rend plus rapide. Cela signifie que le cryptage et le décryptage AES peuvent être effectués plus rapidement que le DES, ce qui est particulièrement important lorsque de grandes quantités de données doivent être cryptées.

Par exemple, on trouve l'AES dans de nombreuses applications et protocoles différents, mais ils ne sont pas limités à :

| | | |
|---|---|---|
|WLAN IEEE 802.11i|IPsec|SSH|
|VoIP|PGP|OpenSSL|

## Modes de chiffrement

Un mode de chiffrement fait référence à la manière dont un algorithme de chiffrement par bloc chiffre un message en clair. Un algorithme de chiffrement par bloc chiffre des données, chacune utilisant des blocs de données de taille fixe (généralement 64 ou 128 bits). Un mode de chiffrement définit la manière dont ces blocs sont traités et combinés pour chiffrer un message de n'importe quelle longueur. Il existe plusieurs modes de chiffrement courants :

|**Mode de chiffrement**|**Description**|
|---|---|
|[Electronic Code Book](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation) (**ECB**) mode|n'est généralement pas recommandé en raison de sa vulnérabilité à certains types d'attaques. En outre, il ne masque pas efficacement les modèles de données. Par conséquent, l'analyse statistique peut révéler des éléments de messages en texte clair, par exemple, dans les applications web.|
|[Cipher Block Chaining](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#CBC) (**CBC**) mode|Le mode CBC est généralement utilisé pour crypter des messages tels que le cryptage de disques et les communications par courrier électronique. C'est le mode par défaut de l'AES et il est également utilisé dans des logiciels tels que TrueCrypt, VeraCrypt, TLS et SSL.|
|[Cipher Feedback](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Cipher_feedback_(CFB)) (**CFB**) mode|est bien adapté au cryptage en temps réel d'un flux de données, par exemple le cryptage de communications réseau ou le cryptage/décryptage de fichiers en transit comme les normes de cryptographie à clé publique (PKCS) et BitLocker de Microsoft.|
|[Output Feedback](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#OFB) (**OFB**) mode|est également utilisé pour crypter un flux de données, par exemple pour crypter des communications en temps réel. Toutefois, ce mode est considéré comme meilleur pour le flux de données en raison de la manière dont le flux de clés est généré. On trouve ce mode dans les PKCS mais aussi dans le protocole SSH.|
|[Counter](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#CTR) (**CTR**) mode|CTR crypte les flux de données en temps réel utilisés par AES, par exemple les communications réseau, le cryptage des disques et d'autres scénarios en temps réel dans lesquels les données sont traitées. Par exemple, IPsec ou BitLocker de Microsoft.|
|[Galois/Compteur](https://en.wikipedia.org/wiki/Galois/Counter_Mode) (**GCM**) mode|GCM est utilisé dans les cas où la confidentialité et l'intégrité doivent être protégées ensemble, comme les communications sans fil, les VPN et d'autres protocoles de communication sécurisés.|

Chaque mode a ses propres caractéristiques et convient mieux à certains cas d'utilisation. Le choix du mode de cryptage dépend des exigences de l'application et des objectifs de sécurité à atteindre.
