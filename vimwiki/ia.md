# OLLAMA
[site-officiel](https://ollama.com/)

chemin d'acces au model installé
```bash
/usr/share/ollama/.ollama/models/manifests/registry.ollama.ai/library/
```

PRÉCHARGER UN MODÈLE ET LE LAISSER EN MÉMOIRE
```bash
curl http://localhost:11434/api/generate -d '{"model": "llama3", "keep_alive": -1}'
```

DÉCHARGER LE MODÈLE ET LIBÉRER DE LA MÉMOIRE
```bash
curl http://localhost:11434/api/generate -d '{"model": "llama3", "keep_alive": 0}'
```
