## DOCUMENTAIRES
[L'amour est-il l'opium du peuple ? avec Eva Illouz, sociologue - FRANCE CULTURE](https://www.youtube.com/watch?v=dTtlrdyvqww)
[Eva Illouz : "La Fin de l’amour"](https://www.youtube.com/watch?v=DoKXqHwo5Lg)
[L'AMOUR : UNE ARME RÉVOLUTIONNAIRE](https://www.youtube.com/watch?v=F45K7ssH3fs)
[Philosophie de l’ennui](https://www.youtube.com/watch?v=u4Jv2hooUPc)
[Biosphère du désert](https://www.arte.tv/fr/videos/110232-001-A/biosphere-du-desert-la-prepa-d-une-mission-low-tech-1-5/)

## LIENS
[signe de l’autoritarisme](https://framablog.org/wp-content/uploads/2023/10/PremiersSignes.png)
[IA libre](https://www.lesnumeriques.com/intelligence-artificielle/hugginchat-un-chatgpt-performant-gratuit-libre-et-qui-ne-veut-pas-de-vos-donnees-personnelles-n218346.html)
[Pourquoi il est important que les médias restent libre](https://video.lqdn.fr/w/76ef8cfb-d17b-471c-abf3-c978f78546df)
[Internet Archive](https://archive.org/)

## MANGA ET BD
Legend of Lemnear
Legend of Crystania
El Hazard
Enfer et Paradis
Eagle - Kaiji Kawaguchi
Fushigi Yugi
Walt & Skeezix: Gasoline Alley (un homme et son fils)
Le mercenaire
Sillage
Vaughn Bode's Erotica (complete)
[Ryokunohara Labyrinth](https://archive.org/details/getvid2_20220827)

## LIVRES
L’art daimer - Eric Fromm
La république de Platon
La formule du professeur - Yoko Ogawa
Le pendule de foucault - Umberto Eco
Le nom de la rose - Umberto Eco
Les trois mousquetaires - Alexandre Dumas
La reine Margot - Alexandre Dumas
Un monde sans fin - Ken Follet
Au commencement était ..., une nouvelle histoire de l'humanité - David Graeber
Possibilités, Essais sur la hiérarchie, la rebellion et le désir - David Graeber
Pour une anthropologie anarchiste - David Graeber
Le mythe du déficit, la théorie moderne de la monnaie et la naissance de l'économie du peuple - Stéphanie Kelton
Le puits - Ivan Repila
La petite communiste qui ne souriait jamais - Lola Lafon
Fief - David Lopez
Il faut qu'on parle de Kevin - Lionel Shriver
La marié était en noir - William Irish

## FILMS
La rumeur - William Wyler
Le septième continent - Michael Haneke
Le mariage de Tuya - Wang Quan An
La frappe - Yoon Sung-Hyun
voir la version suédoise de Funny Games
Gataka
Entre chien et loup - Alexandre Arcady

## VIDÉOS
[QUAND LA COMPASSION DEVIENT UNE ARME POLITIQUE](https://www.youtube.com/watch?v=5QMnMsl9hAM) (8.43 ~ 9.20 | 40 ~ 40.40 | 52.27 ~ 53.50)
ROBUSTESSE 1:14:49 ~ 1:15:18 | 1:15:43 ~ 1:16:07 | 1:35:27 ~ 1:36:00 |

## AI GENERATOR
[perchance](https://perchance.org/welcome)
