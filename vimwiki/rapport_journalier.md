# MERCREDI 28 02 2024
visio inetum
mise à jour du generateur de fausse donnée sur visio concours. (en respectant le MCD, un coucours = une commission = une clé commission) Lionel à fait une revue de code mais il faut encore que j’attribue des vrai nom de ville aux centres et aux académies (pour plus de crédibilité). Faut que je vérifie si c’est les mêmes noms de villes pour chacun.
Sur les bugs que j’avais repérer dans le slider de visio concours. C’est le formatage de date dans la liste des candidats qui pose problème et non les dates dans le slider. À corriger.

# JEUDI 29 02 2024
ajout des noms de villes
modification cosmétique dans la liste des jury: concours et libellé bord à bord, manque de visibilité. ajout d’espace.
J’ai passé la journée à chercher où je pouvais modifier le format des dates enregistré en base. Et je n’ai pas trouvé. Mais ce que je vois c’est que les dates enregistrées dans la base correspondent à la date à minuit - 1 ce qui affiche nécessairement la date précédente.
Concernant les doublons: les dates sont identiques en base, donc le set côtés server ne doit pas bien marcher
j’ai donc converti les dates en chaîne de caractère pour faire un set dessus puis converti de nouveau en date à la sorti puisque c’est ce qui est attendu. Ça n’a pas marché. Je suis à cours d’idée.

# VENDREDI 01 03 2024
j'ai fini d'adapter les noms de villes aux centres et aux académies mais j'ai envoyé plusieurs commits d'un coup sur ma branche et ce n'était pas une bonne idée donc je vais remettre de l'ordre dans tout ça.

# LUNDI 04 03 2024
J’ai fait des test fonctionnelles avec Vincent, J’ai lu de la doc sur les Dates, durées et horloges en informatique.
```
Un souci habituel, c’est la manipulation d’un instant représenté par une date indépendante de l’utilisateur. Il arrive que le langage appelant emploie une date au fuseau horaire local, par exemple « le 19 juin 2021 à 01 h 00 UTC+02:00 », mais que la base de données stocke cet instant au format UTC, ce qui donnera « le 18 juin 2021 à 23 h 00 UTC ». Ce qui représente bien le même instant… mais cet instant sera associé au jour précédent si des calculs temporels sont faits directement par la base de données !
```
ce qui pourrait être bien c’est de modifier la façon dont sont stockées les dates en base.

# MARDI 05 03 2024
Création d'issu pour générer une image Docker de visio concours à chaque push
mise à jour du MCD visio concours. par contre on va attendre de travailler sur la base tant qu’aucun problème ne remonte de la part des utilisateurs. Donc je vais retourner travailler sur la boite.
revue de code sur la boite (problème d’import des fichiers.ics)
revue de code sur agenda (update fullcalendar to v6)
revue de code en cours "Préciser la destination du formulaire de contact"
aujourd'hui je vais voir si je peux travailler sur les timezone avec Lionel.

# MERCREDI 06 03 2024
matin: 3 revue de code sur le questionnaire.
après-midi: j'ai commencé à regarder avec Lionel le script de migration. Donc je continue ça aujourd'hui et je corrige avec Lionel Jeudi.

# JEUDI 07 03 2024
script de migration

# VENDREDI 08 03 2024
On a adapter le script de migration. On a pu appeller des méthodes côtés navigateur pour executer le script des migrations. On peut donc tester en direct ce qu'on fait. Mais on a encore un problème de trie sur les dates. On va donc corriger ça avec Lionel.

# LUNDI 11 03 2024
Vendredi dernier, on s'est cassé les dents avec Lionel sur les timezones. On a eu des moments où ça à marcher d'autres où ça n'a pas marché. Donc c'est pas au point. On va donc continuer la-dessus aujourd'hui.

# MARDI 12 03 2024
Sur les migrations, le UP fonctionne. Hier on a travailler sur le down où les timezones sont plus difficiles à gérer mais on va continuer aujourd’hui.
La date enregistrer en base est en UTC mais la dates afficher quand on récupère la date est en heure locale. Or ce qu’on veut c’est manipuler l’heure UTC.

# MERCREDI 13 03 2024
Avec Lionel on a résolu le problème de timezone. On va donc pouvoir tester les migrations. J’en ai déjà discuter avec Daniel ce matin pour savoir comment on fait, mais je vais quand même demander à Corentin comment il fait.
Et puis sur Sondage il faudrait intégrer l'affichage des timezones. Vincent je te montrerais comment ça marche. et ce qui est attendu.

# JEUDI 14 03 2024
J’ai poussé ce qu’on à fait avec lionel. En tout cas côté sondage, j’ai un message qui me dit que le site en cours de maintenance. `api.appsettings.migrationLockedText`
Donc j’ai au moins réussi à cassé des choses.
J’ai du configurer mon linter parceque rien ne passait. J’ai voulu tester la migrations mais je ne sais pas exactement où je dois regarder les changement en base.

# VENDREDI 15 03 2024
On a généré une première version de la migration. On a encore besoin de faire des tests pour s’assurer que les dates ne changent pas entre l’upgrade et le downgrade. Je travaille actuellement sur un script pour générer des fausses donnée dans sondage. Sinon Laurent et Barco peuvent commencer à regarder.

# LUNDI 18 03 2024
Si on ne s’est pas trompé, le DOWNGRADE fonctionne mais on a encore des problèmes de timezone sur le UPGRADE.

# MARDI 19 03 2024
On a régler le problème des timezones. le UPGRADE et le DOWNGRADE fonctionne. Donc il y a une review à faire. Il me reste à savoir si je fait des timezones sur d’autres services où si je part sur les settings de laboite.

# MECREDI 20 03 2024
Je fais de revue de code sur les timezones de sondage. Il y avait un problème à la prise de rendez-vous si on fait une migration et que la base n’est pas vide. Ça à l’aire de marcher maintenant. J’ai commencé à réfléchir sur comment ajouter les settings sur laboite. On en a parlé avec Gilles. Donc je continue de réfléchir la-dessus, et je vais essayé de récupérer les settings depuis un json et les afficher sur une page.

# JEUDI 21 03 2024
J’ai commencé à créer une page dans la partie administrateur de laboite dans la-quelle j’affiche des données que je récupère depuis les settings de Meteor, mais je me dit que je vais devoir recréer un objet quasiment identique mais dans le-quel on aura la description des paramètres en plus. Je vais donc m’occuper de ça.
Créer une issue.
Priorité aux collections.

# VENDREDI 22 03 2024
Poursuite de la création de la collection
J’ai fait un binding entre la base et le json des paramètres. Comme me disais Gilles ce matin, ça oblige à modifier la base pour chaque modification des paramètres. Donc je vais changer de méthodes.

# LUNDI 25 03 2024
J’ai revu le format de la collection des settings avec un schéma qui ne contient qu’une paire de clé valeur.
J’ai enregistrer tout les paramètres et leur description dans un json à part.
Et aujourd’hui je vais faire un script qui reconstruit des paramètres à afficher dans le tableau de bord des settings de Laboite à partir de la collection et des valeurs dans le json.

# MARDI 26 03 2024
J’ai reconstruit un objet js qui récupère les descriptions depuis un JSON. Maintenant il faudrait mettre les données en base et afficher les données depuis la base.
Je vais me basé sur les autres bases qui ont déjà été créé, et je vais lire la doc de Meteor.
Pour le moment je vais lire de la doc pour comprendre comment fonctionne la publication d’une collection pour pouvoir récupérer les données depuis la base.

# MERCREDI 27 03 2024
Hier j’ai pu mettre des données en base qui sont chargé dés le lancement de l'application,
par contre je me suis cassé les dents pour les faire remonter depuis la base vers le client.
Meteor ne reconnait pas la méthode que je lui ai déclaré.
Donc je vais continuer de chercher ce qui va pas et puis je vais faire un peu de ménage dans mon code.

# JEUDI 28 03 2024
Je récupère bien ma collection côté client mais j’ai un tableau vide alors que ma base est rempli.
Il me manque donc quelque-chose dans mon code pour que ça marche.
J’ai vu qu’il y a un fichier __register-api__ qui importe toutes les méthodes et publications côté server.
C’est peut-être ce qui me maque donc je vais tester ça.

# MARDI 02 04 2024
Je récupère bien les données depuis la base. Et aujourd’hui je vais travailler sur l’interface.
Faire en sorte que les champs soient éditable quand on clique dessus entre autre.
Et ajouter un bouton d’enregistrement pour faire un update de la base.

# MERCREDI 03 04 2024
J’ai fais du cosmétique.
J’ai passé pas mal de temps à essayé d’afficher une checkbox dans un bouton parce-que je préfère cliquer sur un bouton qu’une checkbox.
C’est plus gros. Mais je n’y suis pas arrivé j’ai donc simplement affiché activé ou désactivé.
J’ai des boutons valider et annuler. Et puis je vais créer une méthode pour mettre à jour la base.

# JEUDI 04 04 2024
Désormais le bouton enregistrer fonctionne. On peut donc mettre à jour la base.
J’ai rajouté 3 tests unitaires parce-que la batterie de test m’empêchait de pousser les changements.
J’ai fais un peu de factorisation.
Je peux enfin récupérer les données à la fois côté client et côté server.
Ce qui veut dire que on va pouvoir mettre le strict nécessaire en base et afficher le reste directement depuis le code.
Ce que je vais faire aujourd’hui.

# VENDREDI 05 04 2024
J’ai fait un peu de cosmétique
Affichage responsabilité admin system
J’ai revu un peu le format de ma base
Le tableau des paramètres est près donc maintenant je dois voir comment mettre à jour les paramètres sans relancer laboite. binder la base et les settings meteor

# LUNDI 08 04 2024
Je dois faire le lien entre les paramètres de Meteor et les données que j’ai en base.
Pour le moment j’ai affecter les valeurs en base aux paramètres de Meteor mais à part dans les logs qui m’affiche les changements dans le navigateur, c’est sans conséquence sur l’application. J’ai tenter de le faire côté server mais ça ne change rien. Donc je pense que c’est pas la bonne méthode.
Je vais voir s’il n’y a pas un setter propre à Meteor pour prendre en compte les nouveaux paramètres.

# MARDI 09 04 2024
J’ai peut-être trouvé un moyen de mettre à jour les paramètres de Meteor. Mais j’ai d’abord commencé à reconstruire un objet que je vais affecté dans son entièreté aux paramètres de Meteor.
Je compare les données en base et les paramètres de Meteor et si les données diffères, les données en base sont prioritaires. Mais pour le moment j’ai des soucis parceque les clés entre les deux objets ne correspondent pas toujours. Je vais donc corriger ça et je regarderais après si je peux mettre à jour les paramètres de Meteor.

# MERCREDI 10 04 2024

# JEUDI 11 04 2024
J’ai essayé de mettre à jour les paramètres de Meteor. Mais si je le modifie côté server je ne vois pas les résultats côté client et inversement.
Par contre quand je modifie les paramètres au lancement de Laboite, là il prend en compte. mais je veux pas modifier les paramètres au lancement.
Faudrait voir si je peux pas utiliser des fonction type disconnect, reconnect ou refresh, mais je comprends pas bien comment elles fonctionnent malgrés la doc. Je sens que je commence à peiner un peu.

# VENDREDI 12 04 2024
résolu un warning (each child in an array should have a unique key)
revue de code 731 problème de theme au survole de la page de détail de groupe
revue de code sur 642-inscription-un-gestionnaire-de-structure-devrait-pouvoir-inviter-une-ou-plusieurs-adresse (Bruno)
Après avoir discuté avec Gilles et Lionel je vais faire du ménage dans mon code.

# LUNDI 15 04 2024

J’ai fait en sorte que les paramètres en base soit chargé dans les paramètres de Meteor au lancement de l’application.
La question qui se pose à moi maintenant c’est de savoir si je peux modifier ses paramètres quand l’application est en cours d’execution.
Mais je n’ai rien vu à ce sujet dans la doc de Meteor. Ça aurait une certaine logique que ce ne soit pas modifier pendant l’execution de l’application.

# MARDI 16 04 2024

On peut changer les paramètres public côtés front mais j’ai peur que ce ne soit pas possible côtés server. Par contre on peut arrêter Meteor en espérant qu’il soit relancer par les "PODS".

process.reallyExit() --> exit with code 0
process.exit(1) --> exit with code 1
process.abort --> Exited from signal: SIGABRT

voir ce qui plait le plus à Kubernetes

je vais voir s’il n’y a pas de review à faire.

# JEUDI 17 04 2024

# VENDREDI 19 04 2024

# LUNDI 22 04 2024
Le matin Daniel m’a aidé un résoudre un problème sur Git. Et l’après-midi j’ai fait du cosmétique.

# MARDI 23 04 2024
J’ai fini de rêglé la partie cosmétique. Je peux aider si besoin. Sinon je vais faire un peu de revue de code.

# MERCREDI 24 04 2024
Hier revue de code, aujourd’hui revue de code et rédaction de code pour la journée.

# JEUDI 25 04 2024
J’ai rédigé la doc. À voir sur quoi je pars aujourd’hui.

# VENDREDI 26 04 2024
J’ai commencé à regarder la migration pour les sondages. Mais actuellement j’ai des problèmes d’affichage de l’appli sondage, dû à une dépendance node. Faut que je résolve ça et après j’avancerais sur la migration avec Lionel aujourd’hui.

# MARDI 30 04 2024
Avec Lionel on a essayé de faire marché la migration des sondages. On a encore un problème sur le upgrade donc on doit régler ça, et après ou pourra s’occuper du downgrade.

# JEUDI 02 05 2024
Sur la migration des sondages, l’upgrade fonctionne. Je continue d’écrire le script du downgrade que je vais tester ce matin. Et puis faudra que je discute avec Gilles pour voir si la pages des paramètres de Laboite est utilisable dans son état actuelle ou s’il y a encore des modifications à faire.

# VENDREDI 03 05 2024
Je continue de travailler sur le script de migration des sondage. C’est un peu sophistiqué, donc pas utile que je rentre dans les détail, mais ça avance.

# LUNDI 06 05 2024
Vendredi j’ai continuer de bosser sur le downgrade des sondages, et aujourd’hui il faut que je vois avec Gilles comment on avance sur les paramètres de Laboite.

# MARDI 07 05 2024
j’ai corrigé un bug qui était juste le message d’erreur qui posait problème.
par contre Gilles tu voudrais que je reconstruise les paramètres de Meteor avant que l’appli ne rende la main or je le fait déjà au redémarrage de l’appli.

# LUNDI 13 05 2024
Mardi dernier j’ai fait du cosmetique et du refactoring sur les settings de Laboite. Et j’aimerais voir avec toi Gilles comment on avance là-dessus.

# MARDI 14 05 2024
Le matin j’ai fait de la revue de code sur les timezones avec Lionel. L’après-midi après avoir fait une démo à Gilles des paramètres de Laboite, j’ai corrigé quelques bugs graphique.
On à vu avec Gilles comment on pourrait informer les Pods du status de l’application. C’est une chose qu’on implementera avec Bruno, quand il aura fini ce qu’il a faire.
Aujourd’hui je vais rajouté des champs de type textArea pour les paramêtres qui prennent des listes d’élémnents en paramètre ( comme des url ), et faire en sorte que la liste fournie en paramètre soit convertie en tableau dans la base.

# MERCREDI 15 2024
télétravail, réparation internet

# JEUDI 16 05 2024
Hier j’ai fait de la refactorisation parce-que chaque fois qu’on tapait une lettre dans un champs pour modifier la valeur il réécrivait l’entièreté de l’objet qui contient tous les paramètres, donc c’était pas malin. j’ai modifier les champs de type textArea, qui sont sensé prendre en paramètre des tableau d’url ou d’extension. C’est à dire que l’admin ne tape que les valeur les unes à la suite des autres, et tout est retransformé en tableau au moment de l’enregistrement.
Aujourd’hui il faudrait que je créé un champs pour les descriptions qui me permette d’intégrer du markdown par exemple pour avoir du texte enrichie.

# VENDREDI 17 05 2024
Recherche de bibliothèque markdown sur laboite en lecture seule.
résolution de warning:

`div cannot appear has descendent of <p>`
`fieldset cannot appear has descendent of <p>`

Je continue de voir comment enregistrer les paramètres qui prennent des tableaux d’url et d’extensions en base. puisque les données en base sont formater avant l’affichage puis reformater au moment de l’enregistrement en base sauf que le formattage des données à l’affichage, il ne le fait qu’une fois au chargement de la page or je veux qu’il le fasse chaque fois que je fais un enregistrement.

# MARDI 21 05 2024
Toujours sur les settings. J’ai tout cassé vendredi dernier. Le fait de formater les chaines de character en tableau juste pour l’affichage et de reconvertir en chaine pour l’enregistrement, c’est un peu casse gueule mais je vais corriger ça. d’autant que j’utilise différentes source de donnée pour l’affichage. j’ai une partie des données qui viennent de la base et une autre partie qui vient d’un fichier javascript.

# MERCREDI 22 05 2024
Je continue de bosser sur mes conversions de format de chaine de caracter et de tableau. Côté affichage ça commence à marcher. Par contre il faut que je vérifie ce qu’il m’enregistre en base parce-que pour le moment j’ai encore des erreurs.

# JEUDI 23 05 2024
Hier j’avais encore des problèmes d’affichage à résoudre. La valeur afficher sur les boutons sur les-quels on clique n’était pas bonne. Les boutons ne s’affichaient pas quand les champs de type textArea était vide. Donc tout ça c’est rêglé.
À l’enregistrement en base ça à l’air correct. Par contre il va faut que je regarde ce que les paramètres de Meteor prennent au lancement de l’application parceque je sais à l’avance que je vais devoir modifier le STARTUP.

# VENDREDI 24 05 2024
J’ai résolu des conflit pour intégrer les changement de Vincent. Ça marche. Par contre j’ai pas le même nombre de paramètre en base où dans le JSON de Meteor. Il faut que je change la façon dont je met à jour les paramètre au lancement de l’application.

# LUNDI 27 05 2024
Dans les settings de Meteor j’ai rajouté la partie privé. Ce qui aurait du être le cas depuis le début. Et puis j’ai fait planté l’application parce-que j’ai des donnée vide dans les valeurs par default donc je vais les remplir. et je verrais après ce que je fais.

# MARDI 28 05 2024
Je comparait les trois sources de donnée dont je dispose (settings meteor, les valeurs en base et les valeur par default). Donc je vais continuer d’homogénéisé tout ça parceque il y a des clés qui existe dans une source et pas dans une autre par exemple. Je vais continuer de faire le ménage jusqu’à ce que je n’ai plus d’erreur.

# MERCREDI 29 05 2024
Mise à jour de la doc
fini de faire le ménage dans les sources de données. l’appli a arrêté de planté.
J’ai fait de la revue de code sur la branche de Vincent, l’ajout du header et du footer de laboite.
Je vais embêté Bruno pour voir ce que je peux faire avec Node Discovery.

# JEUDI 30 05 2024
Je me suis repenché sur Node Discovery, j’ai regardé la doc et le ce qu’à fait Bruno mais pour le moment j’ai pas encore compris comment on va relancer l’appli depuis le code. puisque pour le moment ça fait un process.exit() comme avant. Donc il faut que j’en reparle avec Bruno.

# LUNDI 03 06 2024
j’ai fait des test sur eole3.dev

Les services --> filtre par cétégories --> au-delà de 1 le nombre de service indiqué ne correspond pas au nombre de service affiché
Les services --> Text blue foncée sur fond rouge foncée
Les groupes --> Afficher uniquement les groupes rejoints --> affiche également les groupes dont la demande a été faites mais n’a pas encore été accepté
Mes publications (mode nuit) --> éditeur de contenu (Markdown) --> texte noir sur fond noir.
Mes publications (mode nuit) --> éditeur de contenu (Markdown) --> icône en-tête --> texte blanc sur fond blanc.
Mon espace --> hover avec transition / Les groupes --> hover sans transition
Les groupes --> toto (groupes) --> Annuaire --> Text blue foncée sur fond rouge foncée

# MERCREDI 05 06 2024
J’ai fini de faire les tests sur eole3.dev.
J’ai regardé un fix que Lionel a fait sur l’application de l’espace personnel par defaut mais j’ai pas l’impression que ça marche donc je reverrais ça avec lui.
Et aujourd’hui je vais essayer de corriger des bugs sur la page des structures.

# JEUDI 06 06 2024
Hier j’ai corrigé le problème de pagination sur la page des structures. Chaque fois qu’on réinitializait la barre de recherche. la pagination en dessous disparaissait.
J’ai commencé à me pencher sur un autre problème qui est que quand on clique sur une structure parent on s’attend à voir les informations de cette structure. Actuellement ça n’affiche que la structure dans la liste mais pas son détail. Donc je vais changer ça également.

# VENDREDI 07 06 2024
J’ai de nouveaux tester la réinitialisation de l’espace personnel. Visiblement on est pas d’accord avec le rôle du bouton de réinitialisation de l’espace personnel.
J’ai aider Vincent 2 sur le stockage de média. c’était juste un problème de nommage de fichier. Il aime pas trop les caractères spéciaux.
Et puis j’ai voulu reproduire un problème que Bruno avait rencontré sur l’affichage des structures. j’ai donc voulu ajouté un service de structure pour le voir apparaître dans les infos de la structure mais il faut être admin, et j’arrive pas à être admin de ma structure. Donc je vais demander de l’aide ce matin.
Et puis il faut que je vois Gilles pour voir ce qu’il reste à faire avec les Paramètres.

# LUNDI 10 06 2024

# MARDI 11 06 2024
J’ai résolu mon problème d’affichage dans les paramètres de Laboite. Ça essayait d’itérer sur une clé qui n’existait pas.
J’ai fait du python avec le stagiaire.
Et puis j’ai commencé à regarder comment intégrer du texte enrichit avec du markdown dans les descriptions des paramètres.
Donc je vais continuer la dessus aujourd’hui.

# LUNDI 17 06 2024
J’ai refactoré le code Pour prévenir les erreurs quand les clé entre les différentes sources ne match pas. J’ai encore des choses à poffiner avant que ça remarche. Donc aujourd’hui je faire du debug.

# MARDI 18 06 2024
J’ai fini de faire du debug sur les settings.
C’est casiment fini me semble-t-il donc je vais pousser et je vais voir avec Gilles si il y a encore des choses à changer ou si je pars sur autre chose.

# MERCREDI 19 06 2024
Le matin j’ai du résoudre des problèmes avec Git parce que il y avait un commit qui passait pas au pipeline. Lionel a pu tester ma branche mais il à eu un problème, c’est peut-être un paramètre manquant dans sa config. Donc il faut que je prévienne ce type de problème.
j’ai rédigé de la doc pour expliqué comment rédiger les descriptions des paramètres. (la doc est dans le ticket et dans la doc de Laboite) j’ai voulu rajouté une condition dans les méthodes pour s’assurer que l’utilisateur est admin quand il modifie des paramètres. Pour le moment ça marche pas.

( Pour ça j’utilise une fonction qui prend l’id l’utilisateur en argument, et l’__id__ me retourne __undefined__ donc l’__admin__ est toujours à __false__. )

Donc je continue de regarder ça.

# JEUDI 20 06 2024
J’ai fait des corrections sur les labels. Quand la clé éxiste pas, il affiche un message comme quoi il n’y a pas de description pour cette clé, au lieu d’affiché le nom de la clé.
J’ai mis des conditions pour vérifier que l’utilisateur ast admin. Par contre j’ai mis ces conditions côté client. Je sais pas si c’est ce qu’il y a de mieux. J’en rediscuterais avec Bruno.
J’ai un autre souci. C’est que quand j’affecte la partie public des paramètre à Meteor.settings.public ça marche. Il prend en compte.
Mais quand je lui affecte les settings dans leur ensemble (publique et privé), Il ne prend plus en compte les changements.

(Rediscuter de la necessité de vérification admin sur une page qui en soit devrait être accessible uniquement si on est admin)

# VENDREDI 21 06 2024
J’ai pu vérifier si l’utilisateur est admin côté server.
J’ai pu débuguer mon problème de récupération des settings privée.

# LUNDI 22 06 2024
J’ai aider Vincent sur un problème avec une méthode dont il n’arrivait pas à récupérer la valeur.
On a fait un point avec Gilles, revue des tickets pour évaluer les temps dont on a besoin pour finir la v13.
J’ai pas encore trouvé comment lancer l’application sans utilisé sans le fichier de config JSON.

# MARDI 23 04 2024
À quoi sert __app/import/startup/server/db-initialize/ContextSettings.js__ ?
Avec Lionel on a essayé de lancer l’appli sans son fichier de config. Mais c’est pas simple parce-que l’appli à besoin de variable système et on ne peut pas mettre les variables systèmes en dure dans les valeurs par defaut.
Donc actuellement l’appli a toujours besoin d’un fichier de config au démarrage.
Je vais continuer de regarder ça.

# MECREDI 26 06 2024
##### VISIO INETUM 12H

Hier j’ai fini de débugger les settings. Donc l’appli se lance avec un fichier de config minimal qui contient uniquement les paramètres systèmes. Le reste des paramètres dit "admin" sont chargés au lancement si la base est vide comme d’habitude.
Maintenant il faut que d’autre dev test et merger la branche.

Aujourd’hui je vais rédiger un peu de doc pour expliquer comment créer un nouveau paramètre.
Et après je verrais.

# JEUDI 27 06 2024
J’ai rédiger la doc pour créer des paramètres. J’ai donc créer des paramètres. Et j’ai eu des erreurs que j’ai corrigé au passage.
Donc faudra tester ça aussi.
Ce serait bien de savoir si tous les paramètres qui existent actuellement servent à quelque chose ou s'il y en a qu'on peut supprimer.

Quatre clé à traiter

countly.consent
countly.appKey
createUserTokenApiKeys
createUserApiKeys

# VENDREDI 28 06 2024
J’ai testé les modif de Bruno qui sont de pouvoir écraser les valeurs défaut si elles sont explicitement indiqués dans le JSON au démarrage de l'appli et ce uniquement si la base est vide. J’ai également vérifié que ça n'écrasait rien si la base n'était pas vide. Donc ça marche.
Et puis j'ai corrigé des petites choses à chaque fois qu'on me faisait remonter des problèmes.
J’ai adapté la doc.

# LUNDI 01 07 2024
J’ai intégrer une barre de recherche dans les settings. J’ai encore un petit bug, puisque il réinitialise la recherche une fois qu’on a modifier la valeur alors que on pourrait vouloir rester sur la recherche actuel.

# VENDREDI 05 07 2024
J’ai corrigé quelques bugs que Lionel m’a fait remonter.
J’ai encore un bug à corriger que Vincent m’a fait remonter également.
J’ai commencé à regarder sur matérial ui ce qu’ils ont pour les champs de type mot de passe.

# LUNDI 08 07 2024
J’ai masquer les mots de passe et je me suis posé une question: Il y a des champs de type textArea qui ont prennent des clé API en valeurs. Pas sûre de pouvoir transformer un champs type textArea en champs pour liste de mot de passe.
Voir s’il faut cacher les mots de passe uniquement sur les boutons dans la liste des paramètres ou également dans la modal.

```
vu avec Gilles
garder les getSettings.
créé les getSettings si rien dans default Value.
créer des nouveaux paramètres uniquement avec getSettings
```

# MARDI 09 07 2024
finellement je vais laisser les mots de passe pour les champs de type textArea de côté pour le moment.
J’avais commencé à me renseigner sur la façon de reconstruire un objet pour les paramètres à partir des valeurs par defaut. Pour avoir un seul fichier à modifier le jour où on veut ajouter un paramètre, mais j’ai vu Gilles ce matin, donc je vais laisser les settings de côté et je vais voir ce que je trouve comme issue aujourd’hui.

# MERCREDI 10 07 2024
nettoyage de code
revue de code de Corentin sur date et notif 723

J’ai vu l’issue que Bruno à poster
827 - Page de profil: erreur dans la console du navigateur
```
Exception in delivering result of invoking 'structures.getStructures': TypeError: setStructures is not a function
```

Voila ce que j’ai dans le code. __ui/pages/system/ProfilePage.jsx : 189__ 
Dans les deux premières lignes il y a un setter mais pas de state.
Les useState prennent toujours deux arguments que je sache.
Après je sais pas où il faut récupérer les states dans le code pour que la méthode soit fonctionnelle.
À voir avec Bruno quand il sera de retour.
En attendant je vais soit faire de la revue ou partir sur une autre issue.

# JEUDI 11 07 2024
Contre la volonté du chef, je suis retourné sur les settings pour apporter une petite modif. On avait tout une arborescence des paramètres Meteor coder en dur. Donc j’ai changé ça.
l’arborescence est recréé dynamiquement à partir des valeurs par defaut.
L’avantage c’est que on a plus que deux sources 

commit de Lionel
Tout les paramètres sont dans les valeurs par defaut et plus dans le JSON.
Dans le JSON on a laissé que les paramètres system. On peut aussi mettre ses propres paramètres qui écraseront les valeurs defauts.

# VENDREDI 12 07 2024
J’ai retesté l’appli de mon côté également. J’ai remis à jour la documentation. Aujourd’hui j’aimerais terminer une issue que Bruno avait poster et après Gilles m’a dit que il y avait plusieurs chose à faire sur DSOF, donc on reverra ça ensemble tout à l’heure.

# LUNDI 15 07 2024
j’ai fini l’issue de Bruno. il y a avait une erreur dans la console à cause d’une méthod qui ne servait plus.
J’ai traité une issue sur les descriptions dans la modal. j’ai modifié le style car il laissait croire que la description était cliquable et modifiable.
J’ai fait de la revue sur une merge de Corentin. J’aimerais quand même que Joël vérifie parce-que ça marche mais j’ai pas l’impression que ça change grand chose.
L’après-midi on a défini avec Gilles quelles étaient les informations à déclarer lors de la première connexion d’un utilisateur local, pour définir son profil.
Sur la merge [823 - Masquer les mots de passe dans les paramètres](https://gitlab.mim-libre.fr/alphabet/laboite/-/merge_requests/1074), je laisse de côté parceque c’est pas prioritaire et parce-qu’il n’y a pas encore de choix clairement défini sur la façon de traiter les mots de passe d’un côté et les secret de l’autre.

# MARDI 16 07 2024
J’ai mis un filtre sur les paramètres pour les lister dans l’ordre alphabétique
J’ai fait un peu de revue de code (806-affichage par defaut de la page des groupes - Corentin)
On a revue un peu avec Gilles ce qu’il fallait faire sur la page de profil
J’ai remis à plat les notes
J’ai réfléchit par quel bout j’allais prendre les choses et je vais me lancer dedans aujourd’hui

# MERCREDI 17 07 2024

# JEUDI 18 07 2024
Moi et Vincent on continue de travailler sur la page de profil. J'ai ajouté les les attributs Lieux téléphone et location dans les models. J'ai fait remonter des valeur defaut dans le bloque métier.
Il faut que rajoute un paramètre pour rendre le bloque métier actif ou non. et puis il faut que je test les changements que j'ai fait dans model.

# VENDREDI 19 07 2024
Je me suis occupé de terminer la partie métier qu'on peut rendre actif ou non dans la page profil. J’ai galérer un peu sur git mais c’est régler. J'ai revu avec Gilles les amélioration à faire

# LUNDI 22 07 2024
J’ai des méthodes à mettre en place pour mettre en base les données métiers.
J'ai une demande de Bruno à traiter. Il y a des chemins vers certains paramètres qui ne sont pas correcte

# MARDI 23 07 2024
Je m'occupais de la partie métier sur la page profil et j'avais un problème avec le champs pour téléphone parce-que on utilisait une balise qui avait un comportement différent des autres. Donc j'ai adapter le code. J'ai encore un message d'erreur côté front qui faudrait régler et puis j'ai des demandes de Bruno qui attendent. Je vais m'occuper de ça ce matin.

# MERCREDI 24 07 2024
Draft: Resolve "ligne vide dans les paramètres si le paramètre en base n’existe pas dans les valeurs par default" (issue 1096)
Draft: Resolve "settings: problème d'affichage dans la page d'administration" (issue 1095)
Draft: Resolve "settings: variables mal préfixées dans defaultValue.contextSettings.js" (issue 1093)

# VENDREDI 26 07 2024
J’ai juste rajouté le type des valeurs en base et ça simplifie le code dans la page des settings.

# LUNDI 26 08 2024
J'ai répondu à une demande de Bruno qui me proposait de refactorer mon code sur les settings.
J'ai apporté une petite modification sur sur la barre de recherche des settings. Le label n'était pas correcte.
J'ai commencé à regarder frontals-nextcloud. Il y a également des modifications de description à apporter dans i18n. Mais j'ai un problème de connexion. Je dois sans doute lancer une instance de NextCloud, mais je ne sais pas encore comment faire.

# MERCREDI 28 08 2024
je travaille sur le Blog. J'ai modifier des labels sur la page __à propos__ mais j'arrive pas à voir les changements. quand je clique sur le lien __à propos__, ça ouvre la page courante dans un nouvel onglet mais je n'ai toujours pas accès à la page __à propos__

Dans agenda
i18n : la clé __searchUserHelp__ n'est pas utilisé

# VENDREDI 30 08 2024
J'ai consommer de la doc sur Svelte pour comprendre comment est fait le Blog
J'ai modifier les labels sur le blog. Il y a toujours ce problème de page __à propos__ qui ne s'affiche pas. Et j'ai aussi constaté que les boutons retour ne marche pas. J'ai créé une issue pour ça
Sur Agenda il y a un doute sur le fait que je dois changer LaBoîte par appName ou Agenda par appName

# MERCREDI 04 08 2024
le matin on a fait un point sur les demandes prioritaires avec Benoit Piedalu.
L'après-midi j'ai testé un problème que joël m'a fait remonter sur le Blog. la création de lien dans les publication ne marche pas avec un format d'url qui commence par www. Par contre ça marche avec avec un format d'url qui commence par https. Donc est-ce qu’il faut préciser à l'utilisateur le format à utiliser ou est-ce qu'il faut faire en sorte que les format qui commence par www soit pris en compte ? je ne sais pas.

# JEUDI 05 08 2024
J'ai regardé si on pouvait corrigé le problème des liens au format www dans l'édition des articles. Visiblement C'est un problème connu dans la bibliothèque ToastUi. Et je ne crois pas qu'il ai été résolu.
J'ai un peu fouillé la lib pour voir comment c'était fait et j'ai trouvé une option __extendedAutolink__ qu’il faudrait que je teste. Mais je sais pas encore comment la tester.
Après comme je devais pas passer plus d'une journée la dessus je peux aussi passer sur autre chose.

# VENDREDI 06 08 2024
Toujours sur le Blog concernant les liens dans les publications, l'option extendedAutolinks ne donne rien, donc tans pis. Et sinon j'ai fait de la revue de code.

# MARDI 10 08 2024
Sur la page About de Questionnaire, j'ai fait quelque retouche cosmétique. Et puis on a eu un problème sur la bascule entre les langues anglais français. Corentin à fini par trouver le problème. Donc c'est résolu. Aujourd'hui je vais m'occuper d'une demande sur les settings. les settings qui ont été créé avant l'ajout des types en base peuvent poser problème à l'affichage. Il faudrait déjà que j'arrive à reproduire l'erreur, et puis je verrais après comment régler ça.

# MERCREDI 11 08 2024
J'ai corrigé le problème avec les settings. Donc si le type n'existe pas ou qu'il n'est pas correct dans la base. On peut le changer dans les valeurs par défauts et ce sera mis à jour en relançant l'application. Faudrais que tu regardes ça Joël pour être sûre que c'est ce que tu voulais.
Et puis j'ai fait des corrections mineurs sur Questionnaire. correction orthographique et ajout de label.
Je vpas voir ce que je fais aujourd'hui.

# JEUDI 12 09 2024
Je me suis occupé de rajouter les paramètres de la boite au SuperCrud. Je me demande quand même à quoi sert le fait que l'on puisse changer les types depuis LaBoite si c'est pour le refaire depuis le SuperCrud. J'ai vu qu’il y avait une suggestion de Jason pour utiliser le cache pour optimiser SuperCrud.

# VENDREDI 13 09 2024
Hier j'étais très dispersé. J'ai travaillé sur la v15. J'ai fait en sorte que les données qui sont en base et qui n'existe pas dans les valeurs par defaut soi supprimé. Je n'ai rien poussé parceque je préfère voir Joël sur quelle branche je balance ça.
J'ai voulu me pencher sur l'issue 677 où on remplace les espace par des tirets bien qu'on m'ai appris que d'autre avant moi se sont cassé les dents dessus.
Je n'ai pas cherché longtemps puisque Bruno m'a interpellé pour me dire qu'il y a un problème sur la nouvelle collection que j'ai créé sur le SuperCrud.
Quand on modifie un booléen il retourne une chaine de charactère en base. Donc True ou False entre guillement.
Je supute que l'on ai le même problème avec les nombres.
Peut-être que dans la partie API où on retrouve les GET, DELETE, UPDATE Il faudrait modifier le type de la valeur retourner sous certaine condition.
Je n'ai pas encore la solution mais je vais continuer d'y réfléchir aujourd'hui.

# LUNDI 16 09 2024
Avec Bruno on à fait de la revue de code et on a corrigé ce problème de booléen qui s'enregistrait en chaîne de caractère.

# MARDI 17 09 2024
Je test les requêtes API sur LaBoite depuis hier. Il y a un script qui existe déjà pour ça mais LaBoite à évoluer depuis donc il faut l'adapter. Je continue ça aujourd'hui.

# MERCREDI 18 09 2024
J'ai tester l'API sur les groupes J'obtenais des status 200 mais sans changement dans la base. Il y avait des problèmes de condition et de forEach qui n'opérait pas sur le bon attribut. Corentin a corrigé ça et ça marche beaucoup mieux.
Il faudra rajouter des fonctions de suppression :
- les structures
- les bookmarks
- les services
- les extensions

Je vais terminer de tester les fonctions existantes sur ces collections aujourd'hui.
Il y a encore quelque correction mineur à faire sur des commentaires. Oui ce ne sont que des commentaires mais ils donnent des exemple de commande curl à éxécuter pour tester les requêtes et il y a des erreurs de syntaxe dedans. Donc quand on copie colle ça ne marche pas.

# JEUDI 19 09 2024
Il y a des requêtes que je n'arrive pas à tester.

Dans les requêtes qui servent à rucupérer un objet en particulier (group, extension, service, bookmark...) il y a une fonction dedans qui reconstruit un objet avant de le retourner et c'est dans ces fonctions que 
Problème avec generateDefaultPersonalSpace sur Asams ( il n'arrive pas à trouver le userId )
Problème avec generateDataForBookmark sur Bookmark ( il vérifie s'il y a une correspondance entre l'id des groups et le groupId des bookmarks )

# VENDREDI 20 2024
J'ai presque fini de tester les api. C'est à dire que j'ai tester individuellement les commandes curl. Il y a une commande qui ne passe pas. C'est le get one extenision sur Asams. On a regardé ça avec Lionel. Il confirme. Donc on reverra ça entre nous.
Ce que je voudrais finir c'est de vérifier que le script de test marche du premier coup. Parce-que potentiellement une commande de type create peut ne pas marcher parce-qu'elle essaye de créer un objet qui existe déjà. Donc je vais vider la base et vérifier que les commandes ne se marchent pas dessus si je puis dire.

# LUNDI 23 09 2024
J'ai continuer de bosser sur le script des api. J'ai pas d'erreur. Les fonctions de suppression ont été rajoutés donc j'ai rajouter des delete pour néttoyer la base après avoir tester les requêtes. Je voudrais pofiner ça aujourd'hui et terminer de vérifier le résultat en base.

# MARDI 24 09 2024
Avec Lionel on a continuer de tester le script des API. Ça a été l'occasion de remonter quelque problème et de créer une issue. Et Je vais terminer ça avec Lionel avant de faire une démo. Au suivant.

# MERCREDI 25 09 2024
Avec Lionel on a continuer d'améliorer le script de test des api. J'ai fait de la revue de code sur des corrections que Corentin à apporté. Il y avait encore un petit problème sur la modification des admins, membres et animateurs dans les Groups. Je sais pas si Lionel a réussi à le corriger. J'ai pas réussi à l'avoir hier en fin de journée.

# JEUDI 26 09 2024
J'ai fait remonter un problème à Corentin. C'est que quand on supprime le role membre d'un utilisateur il supprime aussi les role admin et animateur. Donc quand le problème sera résolu je dirais le script de test des api fonctionne. Faudra que Lionel test de son côté. J'ai fait de la revu de code sur une issue de Vincent. C'est un problèm d'affichage des rôles qui n'est pas le même entre l'espace personnel et la page des groupes. L'inconvéniant c'est que les erreurs que je vois chez moi n'apparaissent pas toujours chez lui.
Faudrait qu'on trouve la cause.

# VENDREDI 27 09 2024
J'ai fait une correction sur le script de Lionel. Je l'ai testé. ça marche. J'ai fait de la revue de code et puis j'ai regardé le problème de sauvegarde de status sur questionnaire mais je n'arrive pas à reproduire l'erreur.

# LUNDI 30 09 2024
J'ai commencé à travailler sur Lookup-server. J'ai fait un script pour tester quelques commandes curl pour vérifier que ça répond comme attendu.
J'ai voulu tester des requêtes avec des nom d'utilisateur avec accents. quand le regarde la réponse, ça ressemble à de l'encodage javascript.
Je vais regarder le code pour voir ce que je peux faire.

# MARDI 01 10 2024
J'ai fait de la revue de code sur sondage.
On a fait une réunion suivi d'une visio avec Benoit Piedalu et Nicolas Schon pour savoir comment on gère les homonymes dans Lookup-server.
Et pour Lookup server, l'idée c'est faire quelque log pour voir ce que l'API prend en entrée de la part de NextCloud et faire une image pour la tester.

# MERCREDI 02 10 2024
Avec Gilles on a apporté des modifications sur Lookup-server notemment pour faire une recherche uniquement sur la première partie de l'adresse mail. On a déployé ça sur la préprod. On a vu que les accents passaient correctement.
Ce que je vais faire dans l'immédiat, c'est de développer une recherche un peu plus sophistiqué dans le cas où l'utilisateur tape plusieurs mots séparé par un espace.

# JEUDI 03 10 2024
Lookup-server: J'ai développé la recherche dans le cas où plusieurs mots sont séparés par un espace. Il faudrait de nouveau le déployer en préprod pour tester.
Actuellement ce qui ce passe, c'est que si je tape éric et édouard il va me rechercher les deux utilisateurs. Ce qu'il faudrait c'est qu'il ne recherche qu'un seul utilisateur dans lequel on retrouve tous les mots tapé parmi les champs (mail, username, firstname, lastname).
Je suis allé regarder ce qu'à fait Lionel sur la recherche qui ne se déclenche qu'à partir de 5 caractères. La question que je me pose c'est : Si il y a plusieurs mots séparé par un espace, est-ce que chaque mot doit faire plus de 5 caractères ? Parceque si il y a un mot qui fait plus de 5 caractère et un autre qui fait un caractère, il peut déclencher une recherche sur tous les utilisateurs qui possède cette lettre.

# VENDREDI 04 10 2024
J'ai continuer de bosser sur Lookup-server. J'ai une piste pour faire une recherche avec plusieurs mots sans qu'il recherche 36 utilisateurs par mots. Je termine ça et je regarderais les demandes qui restent sur le Lookup. Au suivant.

# LUNDI 07 10 2024
J'ai terminer de mettre au point la recherche dans le lookup-server, faut que je rajoute quelque log pour vérifier que ça marche bien et puis à tester sur la préprod.

# MARDI 08 10 2024
logs et gestion des erreurs + atelier pour préparer rencontre avec Benoit Piedalu

# MERCREDI 09 10 2024
J'ai fait un point avec moi même.
Je dois afficher des données qui respectent la RGPD mais actuellement il n'y a que (email, username, firstname, lastname, federationid)
(email, lastname et federationId) sont exclus. donc il ne reste plus que le username ou le firstname. Est-ce bien utile ?

- Gérer la recherche Nextcloud avec un ou plusieurs espaces dans le searchstring (gestion des espaces entre les mots FAIT)
- L’API ne correspond pas à ce qu’attend Nextcloud (je ne sais pas comment traiter)
- La recherche (en Prod Apps) semble bien sensible à l’ordre des mots, à la casse, à l’accentuation (On s'en occupe pas)
- Logging: it's hard to tell if the application is working (J'en ai parlé)
- Traceback in prod (try except)
- Les résultats de la recherche sont différents entre la recherche de contacts et le partage de fichiers (je pense que ça ne peut pas être vérifié en local)
- Ne commencer la recherche qu'à partir de 5 caractères (recherche à partir de 2 caractères FAIT)

# JEUDI 10 10 2024
Je travaille sur les regex dans le Lookup. J'ai pris exemple sur Laboite. J'ai adaptaté le code en python. Ça ne marche pas encore mais c'est une question de temps.

# MARDI 15 10 2024
J'ai rajouté des logs sur le Lookup, les accents sont pris en compte et J'ai une régression sur la recherche d'adresse mail, donc je vais regarder ça.
On a également fait un point avec Benoît pour voir ce qu’on pourrait ajouter ou améliorer sur Laboîte.

# MERCREDI 16 10 2024
j'ai fait de la revue de code sur Questionnaire
Sur le Lookup, il y avait un autre problème sur la requête avec les email que j'ai corrigé. On peut passer plusieurs clé dans un tableau comme argument dans la commande curl mais il faut échaper les crochets.
Il y avait un problème sur l'image Docker parce-qu’il manquait une dépendance python dans les imports. Ça a été corrigé.
Et comme l'a dit Joël on a tester le lookup avec une image Docker.
Il y a encore un problème encodage UTF8.
Je vais continuer de bosser dessus.

# JEUDI 17 10 2024
Joël a merger ce qui a été fait sur les branches sur lesquelles je ne travaille plus. J'ai rebaser tous ça, résolu des conflits et j'ai eu une régression. Donc j'ai corrigé.
Je sais pas non plus pourquoi il y a un conflit sur le merge 23.
J'ai retravaillé le script de test, tous fonctionne.
Il faudrait de nouveau tester ça sur la préprod.

# VENDREDI 18 10 2024
J'ai résolu le conflit sur le Lookup
J'ai vu qu'il y avait une demande qui m'était adressé sur les settings concernant l'affichage des mots de passe.
Donc j'ai apporté quelque correction.
J'ai un rebase à faire sur cette branche également mais j'ai une erreur de la part du linter. Apparement il manque un module dans le fichier discovery.js
Je vais regarder.
```
/home/edouard/Documents/laboite/app/imports/api/discovery.js
5:21  error  Unable to resolve path to module 'neoip'  import/no-unresolved
```

# MARDI 22 10 2024
Avec Lionel on a généré plusieurs images pour tester le lookup sur la préprod et il faut qu’on continue

# JEUDI 24 10 2024
revue de code
[a-la-connexion-clignotement-entre-les-pages-informations-et-mon-espace-sans-raison](https://gitlab.mim-libre.fr/alphabet/laboite/-/merge_requests/1163)

# VENDREDI 25 10 2024
revue de code
lookup tester via nextcloud sur joel
correction sur l'affichage du federationId
J'ai encore une correction à apporter sur un message d'erreur qu'il faudrait supprimer ou faire en sorte que ça retourne autre chose.

# LUNDI 28 10 2024
avec Lionel on a du faire des corrections mineur sur le lookup
Lionel à laisser un message pour dire qu'il faudrait déployer le lookup en préprod pour tester et si ça marche le déployer en prod
et moi aujourd'hui je ne sais pas encore ce que je vais faire donc je vais regarder les tickets.

# MERCREDI 30 10 2024
je continue de faire du cosmétique sur questionnaire. Là je travaille sur la couleur des sections qui ne contraste pas toujours avec le texte. Il me semble qu'on aurait pu faire plus simple pour le code. Donc je termine ça et je verrais après ce que je fais.

# LUNDI 04 11 2024
J'ai travaillé sur Questionnaire J'ai rajouté des éléments disponible comme la notation avec les étoiles ou des choix de question par oui ou non.
J'ai encore quelques problèmes à résoudre pour que ce soit vraiment fonctionnel.

# MARDI 05 11 2024
J'avais un problème de curseur sur le texte que j'ai réglé.
J'ai un problème sur les réponses oui ou non. Il ne prend pas en compte ma réponse. Donc je dois continuer de chercher d'où ça vient.

# JEUDI 07 11 2024
Questionnaire: j'ai réglé le problème de boucle infini sur le rendu du composant et je ne sais pas comment j'ai fait. Après j'ai fait une mise à jour de firefox ce qui m'a causé pas mal de soucis.
Mais j'ai à peu près retrouver ma configuration initial et je n'ai plus de problème sur mes composants.
J'ai résolu des warnings dans la console parce que c'était bien rouge.
Et puis j'avais un soucis sur le fait qui ne gardais pas en mémoire le dernier choix qu'on avait fait quand on voulait modifier sa réponse, et c'est également résolu.
Aujourd'hui j'aimerais bien rajouter un champs "autre" à la suite des cases à cocher.

# VENDREDI 08 11 2024
Le matin je travaillais sur l'élément toggle et après la visio de 11h, il a été décidé que ce serait un switch. Donc ça a été transformer en Switch. J'ai encore quelque petite chose à paufiner avant que ce soit complètement fonctionnel et je verrais après ce que je fais.

# MARDI 12 11 2024
Sur le questionnaire, l'élément Toggle marche. J'ai eu des soucis cosmétiques mais c'est réglé. Je sais que ça ne fait pas parti des demandes marqué MVP mais j'aimerais que les libéllés à gauche et à droite du toggle soit éditable comme on en avait parlé lors de la dernière visio, comme ça au lieu de oui ou non l'utilisateur peut choisir ce qu'il veut. Donc je vais regarder ça.

# MERCREDI 13 11 2024
sur questionnaire, je continue de travailler sur l'édition des labels du toggle. C'est pas évident parceque le code actuel est assez générique. C'est à dire qu'il est quasiment le même pour chaque composant. Et le fait de spécifier l'édition des éléments m'oblige à rajouter de la complexité. Je vais voir si je trouve une solution.

# JEUDI 14 11 2024
Sur le Questionnaire: je suis bloqué sur le Toggle. Faut tenir compte du state global de React et j'ai peut-être pas bien compris comment ça marche.
Sinon sur le problème que tu m'as fait remonter sur Laboite Joël, l'issue 823 : masquer les mots de passes, Ça peut être des données en base qui ne serait pas présentes dans les valeurs par défaut. En tout cas en local c'était la cause de ce type d'erreur.
Si quelqu'un peut m'aider sur le questionnaire c'est cool.
Ce matin je dois partir un peu avant midi parceque j'ai un rendez-vous chez le médecin et je reviens cet après-midi.

# VENDREDI 15 11 2024
Je suis toujours bloqué sur le toggle. J'ai déclaré un labelLeft et un labelRight dans le store de react. C'est pas très propre mais pour le moment j'aimerais que ça marche.
Dans la page de visualisation il recréé un composant dans le quel ont peut retrouver des propriété du store, mais je ne retrouve pas les propriété que j'ai ajouté. Donc j'aimerais bien savoir comment se composant est créé. Le code est difficile à déchiffrer.

# LUNDI 18 11 2024
On a bossé avec Lionel sur le toggle du questionnaire. On a avancé mais il y a encore des problèmes à résoudre, comme la prise en compte de la valeur par default qui n'est pas bonne actuellement ou le fait qu'il ne rajoute pas de champs quand on en a déjà ajouté deux, un pour chaque label.

# MARDI 19 11 2024
j'ai laissé de côté le toggle du questionnaire. Et je suis reparti sur le lookup.

# MERCREDI 20 11 2024
Sur le questionnaire quand on est pas connecté et qu'on soumet la réponse, on a une erreur `Uncaught (in promise) TypeError: user is null`.
Avec Lionel on a vu que ça venait probablement d'une fonction dans le fichier utils. `export const hasAlreadyRespond = (user, form) => {`
C'est une fonction qui prend l'utilisateur en paramètre mais dans les fichiers où la fonction est appellé, l'utilisateur n'est pas toujours passé en paramètre, donc il est null et ça crash.

# JEUDI 21 11 2024
J'ai retester le questionnaire avec un bouton radio.
La première fois que je répond au questionnaire en ouvrant le lien dans un onglet privé, la soummission fonctionne.
La deuxième fois en copiant le lien dans un nouvel onglet privé, la soummission échoue.
Dans les deux cas l'utilisateur est à __null__. je n'ai pas vu de différence non plus entre les formulaire à part l'id du formulaire.
C'est donc mystérieux pour moi.

J'ai fait de la revue de code.

Et puis j'ai bosser sur le lookup.
J'ai travaillé sur la gestion des __@__. J'ai fait plusieurs test. Ça à l'air de marcher mais je ne suis pas toujours sûre de savoir comment ça marche en arrière plan.
Si je passe un __@__ tout seul, je m'attendais à ce qu'il me retourne tout les résultats possible, mais finalement il ne trouve rien. C'est à dire qu'il fait bien sa recherche sur le champs mail, mais il ne considère pas l'__@__ comme un carcatère partie intégrante de l'adresse. C'est peut-être pas plus mal.

# VENDREDI 22 11 2024
J'ai bossé sur le lookup. J'ai continuer de travaillé sur les __@__. À tester.
J'ai regardé si on pouvait mettre l'option __read only__ sur la base Mongo. Je galère à trouver la liste des options possible dans la doc de __pymongo__.
Il faut que je continue de chercher

# LUNDI 25 11 2024
J'ai fait du refactoring sur le Lookup.
Si les requêtes sont __find()__ c'est forcément __read only__. Donc pas sûre que l'on est besoin de configurer pymongo pour ça.
Et pour le __multithread__, il y a déjà une option dans la config qui indique __multithread__ à __True__.

# MARDI 26 11 2024
J'ai tester le multithread sur le lookup. Quand je lance une requête sur la racine du lookup via plusieurs onglet, J'ai un pid pour le Lookup et plusieurs id pour chaque requête lancé. Donc ça m'a l'air d'être opérationnel.
J'ai fait de la revue de code sur Laboite concernant le stockage de média. Je pense que c'est pas au point parce-que quand on enregistre une image, on est obligé de la rogner et ce n'est pas simplement pour la vignette qu'on la rogne. Il n'enregistre que la partie rognée, donc j'ai ouvert un thread à ce sujet.
Je continue de travailler sur le Lookup. J'aimerais alléger les requêtes en limitant le nombre caractère avec accent pris en compte par la vérification.
Pour le moment ça ne marche pas comme je veux donc je continue de chercher.

# MERCREDI 27 11 2024
J'avais un problème sur le Lookup. J'avais peur que mon script de test ne fonctionne plus. Mais ça remarche donc pas de panique. Donc il faudrait merger la demande sur les __@__.

Je me suis pencher sur l'issue [942-Etudier la recherche d'utilisateurs pour la rendre plus rapide](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/942). Je ne sais pas où le nouveau champs doit être mis à jour. Donc s'il y a expert sur la base Mongo et sur les utilisateurs en particulier, je veux bien qu'il m'aide.

### J'ai éplucher les demandes sur Laboite: (je verrais ça avec Joël)
issue [947-les "valeurs actuelles" ne sont pas actualisées lorsque l'on clique sur ENREGISTRER](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/947). Je me suis permis d'ajouter un commentaire.... Soit on considère que le comportement par défaut est ainsi soit on modifie le comportement.

[922-DSOF/RPC Retirer via setting des items du "menu utilisateur"](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/922)
__Sébastien Rico__ veut qu’on supprime des éléments du menu déroulant de l'utilisateur, mais est-ce bien pertinent ?

### revue de code sur Laboite
[917-"Ajout des informations cycle de vie dans le profil utilisateur"](https://gitlab.mim-libre.fr/alphabet/laboite/-/merge_requests/1166)
J'ai une page blanche sur la page de profil et une erreur dans la console: `TypeError: user.lifeCycleDates is undefined`

# JEUDI 28 11 2024
J'ai revue avec Joël la recherche sur l'__@__. C'est pas au point, mais j'ai compris ce qui ne va pas. Donc je corrige ça.

# VENDREDI 29 11 2024
J'ai fait de la revue de code sur questionnaire [Expiration du questionnaire : le bouton calendrier est trop loin de la date affichée](https://gitlab.mim-libre.fr/alphabet/questionnaire/-/merge_requests/279)
J'ai apporter des corrections sur le problème du __fédérationId__.
Il faut que je fasse une image avec Joël pour tester.

# LUNDI 02 12 2024
J'ai apporté des corrections mineurs et fait un peu de ménage dans le __Lookup__.
J'ai livrer le script de test sur le __Lookup__.
J'ai essayé de faire une image Docker, mais j'ai plein d'erreur.
Donc aujourd'hui, je vais essayer de corriger ça.
Et il faut que j'incrémente la version du __Lookup__.

# MARDI 03 12 2024
J'ai essayé de rajouter un champs pour dans les modèles de __Laboite__ dont la valeur est construite automatiquement à partir d'autre champs, J'ai essayé d'utiliser la fonction `autoValue()` mais actuellement je n'arrive pas à récupérer les autres champs.
je vais regarder si je peux utiliser des __hooks__.
Dans le __Lookup__ j'ai chercher un moyen d'avoir un environnement de __dev__ et un environnement de __prod__, mais c'est pas très propre ce que j'ai fait donc je vais essayer de trouver un moyen qui n'affecte pas les fichiers qui partiront en __prod__.

# MERCREDI 04 12 2024
J'ai terminer mon script pour pouvoir travailler plus proprement en local sur le __Lookup__.
Maintenant j'aimerais pouvoir tester le __Lookup__ avec une image Docker, mais pour le moment j'ai des erreurs, donc je vais regarder ce qui va pas.

# JEUDI 05 12 2024

# VENDREDI 06 12 2024
J'ai régler les erreurs que j'avais à la construction de mon image docker sur le __Lookup__ mais quand je le test j'ai des résultats vide. Je soupçonne que la correspondance entre la base locale et la base du conteneur ne se fait pas correctement.
Je sais pas où se trouve la base __Mongo__ sur ma machine donc si quelqu'un peut m'aider c'est pas de refus.

# LUNDI 09 12 2024
J'essaye toujours de faire marcher le __Lookup__ avec docker. Comme je l'avais déjà dis la dernière fois, j'ai une base vide. Donc j'ai voulu transférer les données de la base locale vers la base du conteneur. Et c'est pas évident parce-que les outils de __dumb__ et d'__export__ marchent pas sur ma machine donc je vais essayer de copier la base à la main.

# MARDI 10 12 2024
Avec Lionel on a fait un backup de la base __mongo__ et je peux enfin utiliser __mongodump__ maintenant. On a également fait du __provisionning__. J'ai recréé des utilisateurs et il faudrait que j'arrive à la mettre dans le conteneur. Et puis j'ai eu mon entretien avec __Gilles__ et __Matthieu__.

# MERCREDI 11 12 2024
J'ai pu exporter la base __mongo__ vers le conteneur. Je peux enfin tester le __Lookup__ dans le conteneur. Suite à ça j'ai fait des tests sur les requêtes. Je vais avoir des petites correction à apporter. Pour rappel j'avais un code qui marchait en locale et pas en prod et inversement. Maintenant le code de prod fonctionne dans le conteneur. Possiblement il va falloir que j'adapte le script de __provisionning__ pour tester convenablement les requêtes.
### J'ai des améliorations à faire sur le Lookup avant le 31/12/2024. C'est quoi ?

# JEUDI 12 12 2024
J'ai réécris le script de test. Par contre j'ai fais quelques autres modifications mineur et je ne sais pas si je dois tout commiter sur la même branche. Je verrais avec Joël si besoin.
Et puis je verrais après ce que je fais.

# VENDREDI 13 12 2024
J'ai commiter les dernier changements sur le __Lookup__, l'occasion de rêgler quelques conflit. Pour les demandes restantes, voir si je suis compétent pour les résoudre.

- [Les nextcloud peuvent demander la suppression des informations de comptes](https://gitlab.mim-libre.fr/alphabet/lookup-server/-/issues/20)
Étant donné que la base utilisateur est gérée par laboite,  il devrait être possible de renvoyer un code 200 afin que nextcloud considère que la requête est faite et ne retente pas plusieurs fois la même requête.

- [Slow query du au regex ...](https://gitlab.mim-libre.fr/alphabet/lookup-server/-/issues/17)
Je ne sais pas si on a trancher sur la façon de résoudre ce problème.

- [Suite 1.2.0 : crashlookup en prod](https://gitlab.mim-libre.fr/alphabet/lookup-server/-/issues/16)
- [Recherche utilisateurs sans accents, mais avec accents sur NC](https://gitlab.mim-libre.fr/alphabet/lookup-server/-/issues/19)
la recherche de Benoit Piedallu ne conduit pas au résultat escompté : Benoît Piédallu, tandis que la recherche Frédéric Wrobel mène bien au résultat escompté : Frederic Wrobel.

- [La CI devrait remonter une erreur si il manque une dépendance python dans les requirements](https://gitlab.mim-libre.fr/alphabet/lookup-server/-/issues/13)
- [Ne commencer la recherche qu'à partir de 5 caractères](https://gitlab.mim-libre.fr/alphabet/lookup-server/-/issues/5)

Dans les __curl__ on peut spécifier une clé __email__. J'ai supprimer une partie du code qui permet de traiter ça puisqu'on a décider qu'on ferais une recherche dans le champs __mail__ à partir du moment où on trouve un __@__ dans l'argument de la requête.
L'autre chose, on a vu avec Joël qu'il y avait des adresses avec __nuage__ à l'endroit du __nclocator__. il se trouve que quand le __Lookup__ ne trouvais pas de __nclocator__ dans la base il en créait un par défaut pour éviter que le __Lookup__ crash. J'ai mis un __try except__ pour gérer le __nclocator__ donc maintenant le __Lookup__ peut retourner une chaîne vide pour le __nclocator__.
Est-ce que c'est un problème ou pas je ne sais pas.
Et puis je vais retester le provisionning avec des données avec Accent. Parceque les fausses données actuel sont en anglais.

# LUNDI 16 12 2024
On à mis à jour le provisionning avec Lionel. au moins on peux tester les accents.
Formation __Svelte 5__ avec Vincent.
Ce qui serait bien, c'est d'avoir un script de test dynamique pour que chacun puisse tester avec ces propres données.

# MARDI 17 12 2024
Le matin j'avais une visio avec Inetum pour un compte rendu sur le travail qu'on avait mené sur l'IA pendant la fermeture du Rectorat. L'après-midi j'ai commencé à améliorer le script de test du __Lookup__. L'idée c'est d'automatiser la création de donnée avant de les tester puis de les supprimer juste après.
Je continue la-dessus aujourd'hui.

# MERCREDI 18 12 2024
J'ai terminé d'automatiser le script de test
Donc on peut créer des utilisateurs spécialement pour les tests
Copier la base Mongo dans le conteneur Mongo
Lancer les test sur les nouveaux utilisateurs
supprimer les utilisateurs de la base locale quand on a fini les tests
Et tout a été documenter
C'est poussé et en __to review__
Je vais tester quelques commandes curl en plus et je verrais ce que je fais après

# JEUDI 19 12 2024
J'avais encore quelques commande curl à tester sur le __Lookup__. Ce sont les commandes qui prennent en compte la __clé email__. On en avait parlé __Gilles__ et on avait dit qu'il fallait garder le code. Mais actuellement c'est une partie du code qui ne fonctionne plus. À l'origine elle ne fonctionne que si on fourni le mail complet, or si on fourni le mail complet, on ne rentre pas dans la condition puisque dés que le __Lookup__ trouve un __@__ dans la recherche, il considère déjà que c'est un mail et il le traite autrement.
Donc il faudrait revoir l'ordre des instructions.
Je reverrais ça avec toi Gilles.
je vais voir ce que je fais aujourd'hui en attendant.

# VENDREDI 20 12 2024
de la revue de code
Et sur le displayName quand je veux créer un nouvel utilisateur il me dit qu'il a envoyé un message à mon adresse mail pour que je confirme la création du nouvel utilisateur mais je ne peux pas lui passer une adresse mail qui existe déjà donc soi je peux pas confirmer le nouvel utilisateur soi je ne peux pas le créer.
À voir avec les dev.
À voir avec __Gilles__ tester __Lookup__ sur __lab16__ ou __préprod__.
## INFOS
	Il y a deux types de recherche sur __Nextcloud__. Peut-être la __clé email__ est utilisé dans la deuxième recherche.
	Coder en dur le fait de ne pas faire remonter les utilisateur nommé __adm.api__ et __adm.grp__.

# VENDREDI 03 01 2025
ne pas afficher les comptes admin sur le __Lookup__

# LUNDI 06 01 2025
Moi vendredi pour accentuer l’adaptabilité opérationnelle, j'ai mis à profit mon expertise pour concevoir une solution qui obscurcit les comptes administratifs sur le serveur Lookup. En intégrant le nouveau paramètre de Laboîte, corrélativement à une forte suppléance stratégique, j'ai pu extraire une liste complète des informations d'identification de l'administrateur qui devaient être nettoyées. L'ensemble du processus a été élégamment conteneurisé dans un environnement localisé, garantissant ainsi une efficacité maximale et minimisant toute latence potentielle ». Et tout cela en seulement 05 jour.

`J'ai passé l'après-midi à passer des tests pour garantir la qualité et la maintenabilité du code.`

# MARDI 07 01 2025
J'ai refait des tests sur le masquage des comptes.
On a fini de merger ce qu'il y avait à merger le __Lookup__.
Et je vais voir avec les copains ce que je fais aujourd'hui.

# MERCREDI 08 01 2025
J'ai voulu voir si on pouvait optimiser les regex dans le __Lookup__ parcequ'il y a beaucoup d'accent qu'il n'est pas utiles de tester à mon avis mais je n'ai pas l'impression que ça change beaucoup de chose en terme de performance donc je vais pas y toucher.
Et puis il faut que je réécrive la doc pour le __Lookup__. Et potentiellement que j'adapte le script de test pour tester les comptes masqués.

# JEUDI 09 01 2025
J'ai rédiger de la doc et refait des test pour corriger pour améliorer le script de test.
J'aimerais bien automatiser le script parceque la, il marche avec ma base mais si quelqu'un d'autre l'utilise il va falloir qu'il modifie certaine variable dans le script, donc je voudrais voir si je peux régler ça.

# VENDREDI 10 01 2025
J'ai refait le script de test sur le __Lookup__ donc j'ai automatiser la création et le masquage des comptes admin. J'aimerais bien automatiser la création d'utilisateurs. Parceque là je suis obliger de les créer à la main. Comme ça il y aura juste à lancer un script pour les tests.

# LUNDI 13 01 2025
J'ai terminé mon script de test. Il créé un utilisateur et un admin à tester, il lance la batterie de test et à la fin il supprime l'utilisateur et l'admin de test.
J'ai modifié la doc ce matin. Je pousse et je verrais avec les dev ce que je fais après.

# MARDI 14 01 2025
J'ai vu avec Joël qu'il y avait un problème sur les accents, c'est que si dans la recherche il y a un accent qui n'est pas bon, le __Lookup__ ne le corrige pas. Donc j'ai corrigé ça.
J'ai rajouté le test en conséquence.
Et puis avec mon script je vérifiait à la main si les données qu'il retournait était correcte. Alors que l'idée c'est qu'il dise uniquement si les test passe ou pas. Donc je vais améliorer ça.

# MERCREDI 15 01 2025
J'ai améliorer mon script de test. Donc au lieu de vérifier à la main si les résultats sont correctes. Ça dit juste si c'est OK. Je vais juste rajouter un compteur pour qu'il nous retourne combien de test passe sur la totalité. Et qu'il retourne quelle sont les erreurs. Je retesterais avec Joël.

# JEUDI 16 01 2025
J'ai fini mon script de test. Faudrait que quelqu'un regarde ça. Et je vais voir les revues qui restent.

# VENDREDI 17 01 2025
J'ai résolu quelques conflit sur git et concernant mon script de test, j'ai vu avec Lionel que ce serait mieux si mon script aller récupérer des variable d'environnement dynamiquement pour que ça marche sur la machine de tout le monde. Je vais en rediscuter avec lui parceque il y a des choses, je ne sais pas trop comment faire.

# MARDI 21 01 2025
J'ai revue mon script de test avec Lionel. Désormais il est agnostique à son environnement.
Ça marche.
Ce qui serait bien c'est que les tests soit lancé par le CI. Lionel et moi on va regarder ensemble.

# MERCREDI 22 01 2025
J'ai rapportés des modifs dans les tests du __Lookup__ parceque moi je l'utilise dans un environnement Docker et Lionel teste sur en locale. Donc j'ai fait en sorte que le script soit agnostique comme on dit.
Mais Lionel à quand même rencontrer des problème suite aux modifications. Donc il faudrait que je regarde ça avec lui.
la variable d'environnement dont parlais Joël à été supprimer.
sinon je vais faire de la revue de code.

# JEUDI 23 01 2025
J'ai voulu faire de la review sur l'issue 953:
[Les notifications provenant du backend sont en anglais](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/953)

Mais je ne sais même pas où est traité la traduction des notifications.
Quand je fais un rgrep sur i18n.('api.notifications...
je ne trouve que rien. En tout cas rien qui ne m'intéresse. Je soupçonne une fonction de traiter les libellé dynamiquement, d'où le fait que je ne puisse pas les retrouver litérallement.
Peut-être que je trouverais quelqu'un pour m'aider.
Sinon puisque Lionel est là je vais voir avec lui les problème qu'il a rencontrer sur les test du __Lookup__.

# VENDREDI 24 01 2025
Avec Lionel on a fait du ménage dans les branches pour pouvoir mettre à jour le __Lookup__.

# LUNDI 27 01 2025
Avec Lionel on à mis à jour les branche sur le __Lookup__ avant de merger. Donc là c'est propre. Joël à tester, il y a avait encore un petit soucis sur les accents mais c'est réglé.
Je vais voir ce que je fais aujourd'hui.

# MARDI 28 01 2025
REVUE DE CODE :
[Page blanche sur la page d'administration d'un groupe](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/979)
[Pb création de compte avec 'sub' modifié dans l'acessToken (pb de pivot)](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/927)

DÉMO:
création d'un watcher avec PyMongo

# MERCREDI 29 01 2025
Je continue de travailler sur le fameux Watch sur Lookup. Je rappel le but du Watch c'est d'être informer sur le __Lookup__ des changements dans la collections des Users dans la base Mongo.
Hier Avec Gilles on a fait des tests sur un nouveau chemin du __Lookup__. C'est pas encore au point mais je vais continuer de chercher.

# JEUDI 30 01 2025
Je confirme que le __Watch__ block les requête __curl__ d'où l'intérêt d'avoir de l'asynchrone. mais je ne pense pas qu'il y ai de l'asynchrone sur __Flask_pymongo__. Par contre je pense qu'on peut faire une combinaison de __Flask__ et de __Motor__ qui est une bibliothèque python pour faire de l'asynchrone.
Sinon j'ai passé un peu de temps à fouéter le stagiaire.

# VENDREDI 31 01 2025
J'ai avancé sur le __Lookup__ et le __Watch__. J'ai galéré à faire fonctionner l'asynchrone donc j'ai utiliser du multiprocessing. et ça à marcher. Je m'explique:
Le Watch est à l'écoute des changement dans la base Mongo et il ne bloque plus les requête curl sur le __Lookup__. Donc on peut faire marcher les deux conjointement.
Maintenant l'idée c'est de mettre en cache la base Mongo sur le __Lookup__.
Sinon j'ai passé un peu de temps à fouéter le stagiaire.

# LUNDI 03 02 2025
J'ai continuer à bosser sur le __Lookup__. J'ai cherché un moyen de mettre la base Mongo en cache, et je cherche toujours.

# MARDI 04 02 2025
J'ai continuer d'avancer sur le __Lookup__. J'ai créé un cache. J'ai tout mis toute les valeurs en majuscule. J'ai fais une recherche dessus et ça répond correctement.
Ce que je vais devoir faire c'est de récupérer tout le code recherche qui à été coder avant et l'adapter pour que ça fonctionne avec le cache et non plus la base Mongo.

# MERCREDI 05 02 2025
je continue sur __Lookup__.

# JEUDI 06 02 2025
J'ai tester le Lookup avec avec le cache. J'ai des erreurs.
Je vais refaire des tests. C'est chiant à tester parce-que le mode debug du lookup n'est pas compatible avec le Watch. Je suis obligé de relancer le Lookup après avoir fait des changement sur la base.
les logs se font pas forcément dans l'ordre que je veux donc c'est pas facile de comprendre ce qui ce passe.
Je vous redirais ce qu'il en est plus tard.

# LUNDI 10 02 2025
J'ai testé le cache vendredi. ça à l'air de marché. 

# MARDI 11 02 2025
Sur toujours sur le __Lookup__. J'ai une reqête à pofiner. Elle a l'air de fonctionner sur les utilisateurs de la base. mais elle n'a pas fonctionner sur mes utilisateurs test.
Donc je vais regarder pourquoi. J'ai encore de la mise en forme à faire.
Les temps de réponses ont l'air bon (quelques millisecondes) sauf la mise en cache (entre 8 et 9 secondes)
La deuxième chose qu'il faudrait que j'optimise, c'est de faire en sorte que le watch n'observe que les champs concernés.

# MERCREDI 12 02 2025
J'ai cherché comment optimiser le __Watch__ pour qu'il n'observe que les champs concerné mais j'ai pas trouvé. Ça manque d'exemple dans la doc. Je vais quand même continuer de chercher.
J'ai résolu des __curl__ qui passait pas les tests.
Faut que je réimplémente le masquage des comptes admin.

# JEUDI 13 02 2025
J'ai essayé de supprimer les comptes admin à la mise en cache des users pour ne pas les afficher. Et j'ai un problème que je ne comprends pas. Si quelqu'un peux m'aider, c'est cool.

# LUNDI 17 02 2025
J'ai continuer de regarder si je pouvais améliorer le __Lookup__. Mais bon depuis le temps faut le tester.
Sinon le 13 Mars si les planètes sont alignés selon les prédictions, je devrais disparaître de ce bureau.

# MARDI 18 02 2025
Joël a mis le Lookup en PP. Ça n'a pas marché du premier coup donc il a fallu corriger des choses. Hier soir il n'y avais plus d'erreur par contre on a pas encore tester les perfs.
Sinon je suis assigné à bosser sur un cron conteneurisé sur __Laboite__ qui consiste à nettoyé les notifs et envoyer des mails. Mais j'ai regardé sur la boite mais ça existe déjà. Donc s'il y a des choses à améliorer dessus, qu'on me le dise.

# MERCREDI 19 02 2025
Dans le cron de __Laboite__ il y a effectivement une fonction de suppression des notifs à rajouter. Il faut juste me dire au dela de combien de temps une notif doit-être supprimé et ce sera fait, parce-que dans la demande il y a écrit "les notifs __trop ancienne__".
Et il y a une fonction "Envoyer les emails du cycle de vie"

# JEUDI 20 02 2025
J'ai encore corriger des trucs sur le __Lookup__. De ce que m'a dis Joël, ça va deux fois plus vite. Il y a encore des choses à corriger. Notemment le formatage des donnée au retour.
Il y a pas toujours les réponses attendu. Donc il faut que je regarde ça.
J'ai continuer de chercher une solution pour faire un watch sur des champs spécifique mais je n'ai toujours rien trouvé.

# VENDREDI 21 02 2025
J'ai refais des tests sur le __Lookup__. J'ai des réponses qui ne remonte pas. J'essaye de comprendre pourquoi. Et puis j'ai fouété un Gabin comme d'hab.

# MARDI 25 02 2025
Je fais du ménage dans les tests du __Lookup__ Pour que ça marche, histoire de pas laisser trop bordel avant de partir.
Je vais faire un peu de mise à jour sur la doc.

# MERCREDI 25 02 2025
J'ai toujours pas de réponse au fait que je vois pas les log du Watch dans le conteneur.
J'ai retravaillé sur le CRON de la boite et les notifs. C'est poussé.
Faut que je mette à jour mes test, parce que les variables d'environnement sont pas importer automatiquement avec Docker.
L'idée c'est quand même d'avoir un script de test qui fonctionne indépendement de son context.

# JEUDI 26 02 2025
J'avais des variables d'environnement qui n'existait pas dans le conteneur Docker. Maintenant j'ai un script de test qui est plus indépendant de son environnement. (À voir où créer une merge)
Et je vais regarder si de la review.

# VENDREDI 27 02 2025
du ménage sur les branche git. Le cron de laboite est en to review.
voir si il y a de la revue de code.
