# DIJON TENNIS DE TABLE

entré dans le club: lundi 17 06 2024
No de license: 

## CLASSEMENT MENSUEL
rang national au 28/08/2024

| Prénom            | Nom             | rang  | points | handicap     | Division       |
|-------------------|-----------------|-------|--------|--------------|----------------|
| Alexis            | CARO            | 20896 |        |              | D1             |
| Alexis            | VELDEMAN        | 6628  |        |              |                |
| Amina             | KAGUERMANOV     |       |        | sport adapté |                |
| Andris            |                 |       |        |              |                |
| Anissa            | KACHIN          |       |        |              | D5             |
| Antoine           | HENRY           | 67097 |        |              | D4             |
| Aurélien          | POUHIN          | 75370 |        | handisport   | D5             |
| Batiste           |                 |       |        | sport adapté |                |
| Carine            | BOSVY           | 1108  |        |              | D1             |
| Claire            |                 |       |        |              |                |
| Clara             | BOURMANI        | 5208  |        |              |                |
| Cristophe (Cocot) |                 |       |        |              |                |
| Cécilia           | DEHAN           | 220   |        |              |                |
| David             |                 |       |        | sport adapté |                |
| Diane             | PONTILLON       | 20031 |        |              |                |
| Edouard           | LE BOURGEOIS    |       |        |              | D5             |
| François          | DOS REIS        |       |        |              | D1             |
| François          | MORLON          | 20421 |        |              |                |
| Gabin             | FAIVRE          | 24038 | 983    |              | ~ R3           |
| Gatien            | CARBILLET       | 71076 |        |              | D4             |
| Gilbert           | BARBIERI        | 45258 |        |              |                |
| Grégoire          | BAILLOU         |       |        | sport adapté |                |
| Hervé             | GUENEAU         | 15851 |        |              |                |
| Hicham            | CHAAR           | 30462 |        |              | D2             |
| Hugo              | DELLA MARTIRE   |       |        |              |                |
| Jean-Marc         | ALBERT          | 36785 |        |              | D2             |
| Jean-Pierre       | MASSART         | 56367 |        |              |                |
| Jules             | MAGNIN          | 35862 |        |              | D3             |
| Julien            | HAMOUSIN        | 75370 |        | handisport   | D5             |
| Liam              | ALIBERT-VALAIRE | 38468 |        |              | D3             |
| Lilou             | SIVILAY         | 5094  |        |              |                |
| Lionel            | RIVIÈRE         | 27500 |        |              |                |
| Louise            | FLEURAT-LESSARD | 956   |        |              |                |
| Lucas             | DOS REIS        | 33824 |        |              | D2             |
| Léa               |                 |       |        |              |                |
| Léa               | FERNEY          | 346   |        | handi adapté |                |
| Marine            | JULIEN          | 492   |        |              |                |
| Matéo             |                 |       |        |              |                |
| Maxime            | CHABANNE        | 40158 |        |              | D2             |
| Mickaël           | RIALLAND        | 19208 |        |              | D1             |
| Nada Alaâ         | MOKHTARI        | 4518  |        |              | D4             |
| Naré              | KIRAKOSIAN      | 711   |        |              |                |
| Nina              | SIVILAY         | 5094  |        |              |                |
| Philippe          | HARATYK         | 35477 |        |              |                |
| Philippe          | MATA            | 56253 |        |              | D4             |
| Serge             | CLEMENT         | 53944 |        |              |                |
| Stéphane          | BAILLOU         |       |        |              |                |
| Thomas            | JUAN            | 75370 |        |              | D4             |
| Thomas            | PERNOT          | 75370 |        |              |                |
| Titouan           | BONHOMME        |       |        |              | D1             |
| Tom               |                 |       |        |              |                |
| Yann              | YVRAY           | 6238  |        |              |                |
| Ylan              | BOURMANI        | 36822 |        |              | D3             |

## SET GAGNÉ

| Prénom   | nom           | date         | comment                                                            |
|----------|---------------|--------------|--------------------------------------------------------------------|
| Aurélien | POUHIN        | 26 08 2024   | handicap - 2 ans de compétition                                    |
| Clara    | BOURMANI      | 22 08 2024   | elle a perdu plusieurs service et a envoyé la balle dans les roses |
| Serge    | CLEMENT       | 06 08 2024   |                                                                    |
| Hugo     | DELLA MARTIRE | 13 09 2024   |                                                                    |
| Grégoire | BAILLOU       | 16 09 2024   | handicap                                                           |
| Antoine  | HENRY         | 10 10 2024   |                                                                    |
| Delphine | FOURCAULT     | 12 10 2024   | compétition entre 17 et 20 ans. Reprise vers 45~50 ans - 2 set     |
| Nina     | SIVILAY       | 15 10 2024   |                                                                    |
| Côme     | REVAUX        | 15 10 2024   |                                                                    |
| Aurélien | POUHIN        | 07 11 2024   | handicap                                                           |
| Aurélien | POUHIN        | 05 12 2024   | 11-3 - handicap                                                    |
| Nada     | MOKHTARI      | 12 12 2024   | 11-4                                                               |
| Matéo    |               | 13 12 2025   |                                                                    |
| Tom      |               | 14 01 2025   | 12-10                                                              |


## MATCH GAGNÉ

| Prénom           | date       | comment                 |
|------------------|------------|-------------------------|
| Grégoire Baillou | 17 10 2024 | sport adapté            |
| Thomas Juan      | 26 10 2024 |                         |
| Jonathan         | 07 11 2024 | ancien joueur de tennis |

## RÉSULTATS COMPÉTITIONS

| date       | lieux            | type      | solo | double |
|------------|------------------|-----------|------|--------|
| 05 10 2024 | Genlis           | équipe    | 2/4  | 0/1    |
| 12 10 2024 | Fontaine d'Ouche | critérium | 0/5  |        |
| 19 10 2024 | Belleneuve       | équipe    | 0/4  | 0/1    |
| 26 10 2024 | Fontaine d'Ouche | crêpe     | 0/3  | 1/3    |
| 02 11 2024 | Fontaine d'Ouche | équipe    | 0/4  | 0/1    |
| 12 11 2024 | Fontaine d'Ouche | vétéran   | 0/4  | 0/1    |
| 16 11 2024 | Marsannay        | équipe    | 0/4  | 0/1    |
| 24 11 2024 | Fontaine d'Ouche | critérium | 2/4  |        |
| 30 11 2024 | St Apollinaire   | équipe    | 0/4  | 0/1    |
| 03 12 2024 | Beaune           | vétéran   | 0/4  | 1/2    |

[pingpocket](https://www.pingpocket.fr/#accueil)
Mon profil --> Nom Prénom --> journée

| joueurs par ville - type     | points | simples gagnés | commentaires                                        |
|------------------------------|--------|----------------|-----------------------------------------------------|
| GENLIS - équipe              |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| BIANCHI Gilles               | 500    | X              | sport adapté                                        |
| CHEVIGNY Christelle          | 500    |                | ancienne D2                                         |
| MARTIN Charly                | 604    |                | 3 ans de compétition                                |
| MARTIN Cyril                 | 500    | X              | 1 an libre                                          |
|------------------------------|--------|----------------|-----------------------------------------------------|
| FONTAINE D'OUCHE - critérium |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| DEBOYER DE CHAMPRIEU Nico    | 534    |                |                                                     |
| MARTINEZ Isabelle            | 500    |                |                                                     |
| HENRY Antoine                | 531    |                | 2 ans d'expérience                                  |
| DUPUY Dylan                  | 514    |                |                                                     |
| FOURCAULT Delphine           | 500    |                | (40~45 ans) 3~4 ans d'expérience entre 16 et 20 ans |
|------------------------------|--------|----------------|-----------------------------------------------------|
| BELLENEUVE - équipe          |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| AZNAR FERNANDEZ Théo         | 526    |                | 7 ans d'expérience                                  |
| GUILBAUD Johan               | 774    |                | 20 ans d'expérience                                 |
| JOBARD OLLIVIER Lily         | 609    |                |                                                     |
| LIODENOT Frédéric            | 1062   |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| FONTAINE D'OUCHE - crêpe     |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| MONOD Noa                    | 641    |                |                                                     |
| GOUPILLON Allan              | 601    |                |                                                     |
| GUILLEMIER Antoine           | 1138   |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| FONTAINE D'OUCHE - équipe    |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| MAL Patrick                  | 500    |                |                                                     |
| TAVERNARI Noel               | 500    |                | retraité - 2 ans licences libre                     |
| JAPIOT Christophe            | 500    |                | retraité - 2 ans licences libre                     |
| JOOS Guillaume               | 500    |                | retraité - 2 ans licences libre                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| FONTAINE D'OUCHE - vétéran   |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| ALBERT Jean-Marc             | 800    |                |                                                     |
| GUENEAU Hervé                | 1144   |                |                                                     |
| HUMBERT Pascale              | 576    |                |                                                     |
| BLAISE Fabrice               | 516    |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| MARSANNAY - équipe           |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| GERBAULT Amael               | 712    |                |                                                     |
| PLATHEY Denis                | 614    |                |                                                     |
| CHAUMONNOT Laura             | 571    |                |                                                     |
| BERECHE Gaëtan               | 554    |                | 2 ans licence libre - 1 an compétion                |
|------------------------------|--------|----------------|-----------------------------------------------------|
| FONTAINE D'OUCHE - critérium |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| MARTINEZ Isabelle            | 500    | X              |                                                     |
| HAMOUSIN Julien              | 500    | X              |                                                     |
| Moine Cyril                  | 500    |                |                                                     |
| JUGE Nicolas                 | 500    |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| ST APPOLINAIRE - équipe      |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| DANO Robin                   | 500    |                | 1 / 3 - 1 an compétion                              |
| GAUTHIER Jean-Michel         | 515    |                |                                                     |
| REBOUILLAT Benjamen          | 567    |                |                                                     |
| JUGE Nicolas                 | 500    |                | 7 ans d'exp il y a 20 ans                           |
|------------------------------|--------|----------------|-----------------------------------------------------|
| BEAUNE - vétéran             |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| MESNIL Thierry               | 870    |                |                                                     |
| LAMIDEY Emmanuel             | 1136   |                |                                                     |
| GAUDRY Pierre                | 789    |                |                                                     |
| CANTE Charles                | 500    |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| FONTAINE D'OUCHE - équipe    |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| MARTIN Cyril                 | 500    | X              | 3 / 1 - 1 an de libre                               |
| MARTIN Charly                | 604    |                | 1 / 3 - 3 ans de compétition                        |
| FRAU Nicolas                 | 500    |                | 1 / 3 - 1 an de libre                               |
| DEMOUGEOT Vivien             | 500    |                | 0 / 3 - 1 an de libre                               |
|------------------------------|--------|----------------|-----------------------------------------------------|
| BELLENEUVE - équipe          |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| MATULA René                  | 506    | X              | 3 / 1                                               |
| MATULA Joelle                | 500    | X              | 3 / 0                                               |
| HUMMEL Jean-Marie            | 500    |                | 1 / 3                                               |
| BORNOT Jean-Louis            | 508    |                | 0 / 3                                               |
|------------------------------|--------|----------------|-----------------------------------------------------|
| FONTAINE D'OUCHE - vétéran   |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| ZAFOUR Yassin                | 970    |                |                                                     |
| FEVRE Yoann                  | 778    |                |                                                     |
| VIRELY Patrick               | 799    |                | 1 / 3                                               |
| ALANORE Stephane             | 663    |                | 1 et demi d'exp + 35 ans de tennis                  |
|------------------------------|--------|----------------|-----------------------------------------------------|
| FONTAINE D'OUCHE - critérium |        |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|
| POTIEZ Manon                 | 500    |                |                                                     |
| GAUDARD Thierry              | 500    |                | sport adapté                                        |
| HAMOUSSIN Julien             | 500    |                | andi-sport                                          |
| GOUJARD Franck               | 536    |                | andi-sport                                          |
| DA SILVA Paulo               | 500    |                | andi-sport                                          |
| BOISAUBERT Dimitri           | 500    |                | 1 an libre                                          |
| POUHIN Aurélien              | 500    |                | andi-sport                                          |
| TRUCHOT Mathieu              | 500    |                |                                                     |
|------------------------------|--------|----------------|-----------------------------------------------------|


## PÉRIODE D'ABSENCE

17 09 2024
19 09 2024
20 09 2024
21 09 2024 compétition
23 09 2024
24 09 2024
27 09 2024
30 09 2024
01 10 2024
04 10 2024
11 10 2024
14 10 2024
29 10 2024

## COMPÉTITION VÉTÉRAN
[tableau excel](https://docs.google.com/spreadsheets/d/1h7mpIpSaQNg74IcfQvxkXuVAqz8tm-bL/edit?pli=1&gid=834940164#gid=834940164)

| Date       | Horaires | Rencontre 1        | Rencontre 2        | Lieu       |
|------------|----------|--------------------|--------------------|------------|
| 12 11 2024 | 18h30    | DTT1 / DTT2        |                    | Dijon      |
| 12 11 2024 | 19h30    | DTT1 / Auxonne2    | DTT2 / Auxonne1    | Dijon      |
|------------|----------|--------------------|--------------------|------------|
| 03 12 2024 | 20h      | Beaune 1 / DTT 1   | Beaune 2 / DTT2    | Beaune     |
| 03 12 2024 | 21h      | Beaune 1 / DTT 2   | Beaune 2 / DTT1    | Beaune     |
|------------|----------|--------------------|--------------------|------------|
| 21 01 2024 | 18h30    | DTT 1 / SMTT1      | DTT2 / Marsannay 1 | Dijon      |
| 21 01 2024 | 19h30    | DTT 1 / SMTT1      | DTT2 / Marsannay 2 | Dijon      |
|------------|----------|--------------------|--------------------|------------|
| 04 02 2024 | 20h      | Chevigny 1 / DTT1  | Genlis 1 / DTT2    | Chevigny   |
| 04 02 2024 | 21h      | Chevigny 1 / DTT2  | Genlis 1 / DTT1    | Chevigny   |
|------------|----------|--------------------|--------------------|------------|
| 18 03 2024 | 18h30    | DTT1 / Belleneuve1 | DTT2 / Belleneuve2 | Dijon      |
| 18 03 2024 | 19h30    | DTT1 / Belleneuve2 | DTT2 / Belleneuve1 | Dijon      |
|------------|----------|--------------------|--------------------|------------|
| 01 04 2024 | 18h30    | DTT1 / Belleneuve3 | DTT2 / Belleneuve4 | Dijon      |
| 01 04 2024 | 19h30    | DTT1 / Belleneuve4 | DTT2 / Belleneuve3 | Dijon      |
|------------|----------|--------------------|--------------------|------------|
| 15 04 2024 | 20h      | DTT1 / ST APO1     | DTT2 / ST APO2     | ST APO     |
| 15 04 2024 | 21h      | DTT1 / ST APO2     | DTT2 / ST APO1     | ST APO     |
|------------|----------|--------------------|--------------------|------------|
| 06 05 2024 | 19h45    | DTT1 / Auxonne1    | DTT2 / Auxonne2    | Belleneuve |

## LACUNES
rester les épaules en avant
défense faible, bloquer les balles aux rebonds
bouger sur la gauche pour les balles en revert
cesser les reverts sur les coups droit
ne pas toujours prendre la balle trop tôt
frotter la balle

## PONGISTE
| nom          | nationalité | rang      | commentaire |
|--------------|-------------|-----------|-------------|
| ITO Mima     | Japon       | 11        |             |
| SATO Hitomi  | Japon       | 78        | Défense     |
| HIRANO Miu   | Japon       | 13        |             |
| NIWA Koki    | Japon       | 13        | Nonchalance |
| YUAN Jia Nan | France      | 18        | 40 ans      |
| BOLL Timo    | Allemagne   | 1 en 2003 | 43 ans      |
| SUH Hyo-Won  | Corée       | 21        | Défense     |

