# LINUX STRUCTURE
## Philosophy


Linux suit cinq principes fondamentaux :

|Principe|Description|
|---|---|
|**Everything is a file**|Tous les fichiers de configuration pour les différents services fonctionnant sur le système d'exploitation Linux sont stockés dans un ou plusieurs fichiers texte.|
|**Small, single-purpose programs**|Le système Linux offre de nombreux outils différents avec lesquels nous allons travailler, et qui peuvent être combinés pour fonctionner ensemble.|
|**Ability to chain programs together to perform complex tasks**|L'intégration et la combinaison de différents outils nous permettent d'effectuer de nombreuses tâches importantes et complexes, telles que le traitement ou le filtrage de résultats de données spécifiques.|
|**Avoid captive user interfaces**|L'intégration et la combinaison de différents outils nous permettent d'effectuer de nombreuses tâches importantes et complexes, telles que le traitement ou le filtrage de résultats de données spécifiques.|
|**Configuration data stored in a text file**|Les données de configuration stockées dans un fichier texte sont par exemple le fichier **/etc/passwd**, qui stocke tous les utilisateurs enregistrés sur le système.|

## Composants


|Composant|Description|
|---|---|
|**Bootloader**|est un morceau de code qui s'exécute pour guider le processus d'amorçage afin de démarrer le système d'exploitation. Parrot Linux utilise le Bootloader GRUB.|
|**OS Kernel**|Le noyau du système d'exploitation est le composant principal d'un système d'exploitation. Il gère les ressources des périphériques d'entrée/sortie du système au niveau matériel.|
|**Daemons**|Les services d'arrière-plan sont appelés "daemons" dans Linux. Leur objectif est de s'assurer que les fonctions clés telles que la planification, l'impression et le multimédia fonctionnent correctement. Ces petits programmes se chargent après le démarrage ou la connexion à l'ordinateur.|
|**OS Shell**|Le shell du système d'exploitation ou l'interpréteur du langage de commande (également appelé ligne de commande) est l'interface entre le système d'exploitation et l'utilisateur. Cette interface permet à l'utilisateur de dire au système d'exploitation ce qu'il doit faire. Les shells les plus couramment utilisés sont Bash, Tcsh/Csh, Ksh, Zsh et Fish.|
|**Graphics server**|Il fournit un sous-système graphique (serveur) appelé "X" ou "serveur X" qui permet aux programmes graphiques de s'exécuter localement ou à distance sur le système de fenêtrage X.
|**Window manager**|Le "gestionnaire de fenêtres" est également connu sous le nom d'interface utilisateur graphique (GUI). Il existe de nombreuses options, notamment GNOME, KDE, MATE, Unity et Cinnamon. Un environnement de bureau comporte généralement plusieurs applications, notamment des navigateurs de fichiers et des navigateurs web. Celles-ci permettent à l'utilisateur d'accéder et de gérer les fonctions et services essentiels et fréquemment utilisés d'un système d'exploitation.|
|**Utilities**|Les applications ou utilitaires sont des programmes qui exécutent des fonctions particulières pour l'utilisateur ou un autre programme.|

## Architecture de Linux

Le système d'exploitation Linux peut être décomposé en couches :

|Layer|Description|
|---|---|
|**Hardware**|les périphériques tels que la mémoire vive, le disque dur, le processeur et autres.|
|**Kernel**|Le noyau du système d'exploitation Linux a pour fonction de virtualiser et de contrôler les ressources matérielles communes de l'ordinateur, telles que le processeur, la mémoire allouée, les données accédées, etc. Le noyau donne à chaque processus ses propres ressources virtuelles et prévient/régule les conflits entre les différents processus. Le noyau donne à chaque processus ses propres ressources virtuelles et prévient/régule les conflits entre les différents processus.|``Shell`` Une interface de ligne de commande (**CLI**), également connue sous le nom de shell, dans laquelle un utilisateur peut entrer des commandes pour exécuter les fonctions du noyau.|``Shell``.
|**Shell**|L'interface de ligne de commande (**CLI**), également connue sous le nom de shell, dans laquelle l'utilisateur peut entrer des commandes pour exécuter les fonctions du noyau, est une interface de ligne de commande (**CLI**).|
|**System Utility**|Donne à l'utilisateur l'accès à toutes les fonctionnalités du système d'exploitation.|
## Hiérarchie du système de fichiers

Le système d'exploitation Linux est structuré selon une hiérarchie arborescente et est documenté dans la norme [Filesystem Hierarchy](http://www.pathname.com/fhs/) (**FHS**). Linux est structuré avec les répertoires de premier niveau standard suivants :


![iamge](https://academy.hackthebox.com/storage/modules/18/NEW_filesystem.png)


|Path|Description|
|---|---|
|**/**|Le répertoire de premier niveau est le système de fichiers racine et contient tous les fichiers nécessaires au démarrage du système d'exploitation avant que les autres systèmes de fichiers ne soient montés, ainsi que les fichiers nécessaires au démarrage des autres systèmes de fichiers. Après le démarrage, tous les autres systèmes de fichiers sont montés à des points de montage standard en tant que sous-répertoires de la racine.|
|**/bin**|Le répertoire racine contient les fichiers binaires des commandes essentielles.|
|**/boot**|Le système d'exploitation Linux est composé du chargeur d'amorçage statique, de l'exécutable du noyau et des fichiers nécessaires au démarrage du système d'exploitation Linux.|
|**/dev**|Contient des fichiers de périphérique pour faciliter l'accès à chaque périphérique matériel attaché au système.|
|**/etc**|Fichiers de configuration du système local. Les fichiers de configuration des applications installées peuvent également être sauvegardés ici.|
|**/home**|Chaque utilisateur du système dispose d'un sous-répertoire pour son stockage.|
|**/lib**|Les fichiers de bibliothèque partagés qui sont nécessaires pour le démarrage du système.|
|**/media**|Les périphériques amovibles externes tels que les lecteurs USB sont montés ici.|
|**/mnt**|Point de montage temporaire pour les systèmes de fichiers normaux.|
|**/opt**|Les fichiers optionnels tels que les outils tiers peuvent être sauvegardés ici.|
|**/root**|Le répertoire personnel de l'utilisateur root.|
|**/sbin**|Ce répertoire contient les exécutables utilisés pour l'administration du système (fichiers système binaires).|
|**/tmp**|Le système d'exploitation et de nombreux programmes utilisent ce répertoire pour stocker des fichiers temporaires. Ce répertoire est généralement effacé au démarrage du système et peut être supprimé à d'autres moments sans avertissement. Le système d'exploitation et de nombreux programmes utilisent ce répertoire pour stocker les fichiers temporaires.
|**/usr**|Contient les exécutables, les librairies et les fichiers de données.|
|**/var**|Ce répertoire contient des fichiers de données variables tels que des fichiers journaux, des boîtes de réception d'e-mails, des fichiers liés à des applications web, des fichiers cron, etc.|
# LINUX DISTRIBUTIONS

Nous pouvons utiliser ces distributions partout, y compris sur les serveurs (web), les appareils mobiles, les systèmes embarqués, l'informatique en nuage et l'informatique de bureau. Pour les spécialistes de la cybersécurité, les distributions Linux les plus populaires sont, entre autres, les suivantes :

| | | |
|---|---|---|
|[ParrotOS](https://www.parrotsec.org/)|[Ubuntu](https://ubuntu.com/)|[Debian](https://www.debian.org/)|
|[Raspberry Pi OS](https://www.raspberrypi.com/software/)|[CentOS](https://www.centos.org/)|[BackBox](https://www.backbox.org/)|
|[BlackArch](https://www.blackarch.org/)|[Pentoo](https://www.pentoo.ch/)||

Kali Linux est la distribution la plus populaire pour les spécialistes de la cybersécurité, car elle comprend un large éventail d'outils et de paquets axés sur la sécurité. Ubuntu est très répandu pour les utilisateurs d'ordinateurs de bureau, tandis que Debian est populaire pour les serveurs et les systèmes embarqués. Enfin, red Hat Enterprise Linux et CentOS sont populaires pour l'informatique d'entreprise.

# PROMPT OPTIONS

|Special Character|Description|
|---|---|
|**\d**|Date (Mon Feb 6)|
|**\D{%Y-%m-%d}**|Date (YYYY-MM-DD)|
|**\H**|Full hostname|
|**\j**|Number of jobs managed by the shell|
|**\n**|Newline|
|**\r**|Carriage return|
|**\s**|Name of the shell|
|**\t**|Current time 24-hour (HH:MM:SS)|
|**\T**|Current time 12-hour (HH:MM:SS)|
|**\@**|Current time|
|**\u**|Current username|
|**\w**|Full path of the current working directory|


