# PORTE
(c) 5780x - tourner à droite

# GITLAB
https://sso.mim-libre.fr

# VISIO
https://groupes.mim-libre.fr
https://partage-ecran.mim-libre.fr/?room=EOLE&create=true

# PORTAIL
[portail de test dev](https://portail.eole3.dev/personal)

# MIMLIBRE
mail: edouard.le-bourgeois@inetum.com
mdp: mimlibrefr

# SCREEGO
[screego](https://partage-ecran.mim-libre.fr/?room=EOLE)

# PROXY
http: proxy.eole.lan
port: 3128

# MAIL INETUM
adresse: gfi1.sharepoint.com
mail: edouard.le-bourgeois@inetum.com
mdp: monmotdepasseinetum

3 questions securite:
- createurdereve
- choucroute
- artiste

# GESTIONNAIRE DE PAIE
Aurelie CARINDO
[paie](https://www.mypeopledoc.com)

# INETUM WORDWIDE
mdp: monmotdepasseinetumwordwide
https://gfi.360learning.com/home/content/all

# MÉDECINE DU TRAVAIL
[médecine-travail](https://aist21.padoa.fr/employee/562263/dashboard)

# METEOR CONTEST VISIO
feed token: PSC-G6nGzkcatwP_vtiJ
new token: glpat-AFyCb8i3mkC4HYdHBiny

# PIA
https://pia.ac-dijon.fr
Pour edouard.le-bourgeois@ac-dijon.fr, aller sur https://webmail.ac-dijon.fr
Pour Edouard.Lebourgeois@region-academique-bourgogne-franche-comte.fr, aller sur https://webmail.region-academique-bourgogne-franche-comte.fr/
https://id.ac-dijon.fr
id: elebourgeois
mdp: mailaCademique

# DOCUMENTATION EOLE
[eole](https://eole.education/fr/home)
[pipeline et contribution](https://eole.education/fr/DuTicketALImageDocker)

# DOCUMENTATION VISION CONCOURS 2023
[visio](https://codimd.mim-libre.fr/JVEbh4ldTwW4gowHGHZ20Q?view)

# SWILE
## code d'identification
OED6JW
## code carte
7728

# COMPTE OPEN WEBUI
- nom : calderano
- mail : hugo.calderano@gmail.com
- mdp : hugo.calderano@gmail.com

# VERSPIEREN
Direction des Assurances de personnes
- mail : adp@verspieren.com
- Tél : 03 20 45 33 33

Identifiants nécessaires à votre connexion :
- Login : votre numéro de Sécurité sociale sur 13 caractères.
- Mot de passe : votre date de naissance au format JJMMAAA.

Vous pouvez utiliser les identifiants suivants :
- Login :KZ2M4396
- Mot de passe :3817

# MATRIX
passphrase: matrixmimlibrefr
