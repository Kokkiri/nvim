Extract pages from PDF file and make a separate PDF file for each page:
```
pdfseparate path/to/source_filename.pdf path/to/destination_filename-%d.pdf
```

Specify the first/start page for extraction:
```
pdfseparate -f 3 path/to/source_filename.pdf path/to/destination_filename-%d.pdf
```

Specify the last page for extraction:
```
pdfseparate -l 10 path/to/source_filename.pdf path/to/destination_filename-%d.pdf
```

exemple:
```
pdfseparate -f 1 -l 4 note_frais_jury_besancon_21-11-2022~22-11-2022.pdf page%d.pdf
```

combine pdf files into one:
```
pdfunite path_folder/*.pdf new_file.pdf
```
