REMPLACE TOUT LES MOTS-CLÉ DANS UN FICHIER
`sed -i 's/old-text/new-text/g' toto.md`

AJOUTE DES GUILLEMETS AUTOUR DES PHRASES
`sed 's/^/`/;s/$/`/' toto.md`

AJOUTE DES BACK-TICK SUR TOUTES LES PHRASES À L'EXCEPTION DES PHRASES QUI COMMENCENT PAR "# " PAR "- " ET LES LIGNES VIDENT ( ! = EXCLUSION | = SEPARATEUR D'EXPRESSION ^ = DÉBUT DE PHRASE $ = FIN DE PHRASE () = GROUPER LES EXPRESSIONS )
`sed -i '/\(# \|- \|^$\)/! s/^/`/; /\(# \|- \|^$\)/! s/$/`/' toto.md`

SUPPRIME TOUS LES `0` DEVANT LES FICHIERS
```bash
for file in `ls`;  do mv $file `echo $file | sed -e 's:^0*::'`;  done
```
REMPLACEMENT À LA LIGNE 27
`sed 27s/foo/bar/ toto.md`

