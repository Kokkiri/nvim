|-------------------------------|------------------------------------------------------------|
| tree                          | comment                                                    |
|-------------------------------|------------------------------------------------------------|
| imports/                      |                                                            |
| startup/                      |                                                            |
| client/                       |                                                            |
| index.js                      | # import client startup through a single index entry point |
| routes.js                     | # set up all routes in the app                             |
| useraccounts-configuration.js | # configure login templates                                |
| server/                       |                                                            |
| fixtures.js                   | # fill the DB with example data on startup                 |
| index.js                      | # import server startup through a single index entry point |
|                               |                                                            |
| api/                          |                                                            |
| lists/                        | # a unit of domain logic                                   |
| server/                       |                                                            |
| publications.js               | # all list-related publications                            |
| publications.tests.js         | # tests for the list publications                          |
| lists.js                      | # definition of the Lists collection                       |
| lists.tests.js                | # tests for the behavior of that collection                |
| methods.js                    | # methods related to lists                                 |
| methods.tests.js              | # tests for those methods                                  |
|                               |                                                            |
| ui/                           |                                                            |
| components/                   | # all reusable components in the application               |
|                               | # can be split by domain if there are many                 |
| layouts/                      | # wrapper components for behaviour and visuals             |
| pages/                        | # entry points for rendering used by the router            |
|                               |                                                            |
| client/                       |                                                            |
| main.js                       | # client entry point, imports all client code              |
|                               |                                                            |
| server/                       |                                                            |
| main.js                       | # server entry point, imports all server code              |
|-------------------------------|------------------------------------------------------------|

# METEOR MONGO
## lancer mongo
meteor mongo

## commandes disponibles
help

## afficher les bases
show dbs

## afficher les collections
show collections

# MONGOSH
## insertion
db.collection.isert({})

## suppression
db.collection.remove({})

# METEOR
## Pour connaitre la version de meteor
cat .meteor/release

## Installation des packets npm
meteor npm install

## monter de version
meteor update --release METEOR@2.14

## vérifier la version node de meteor
meteor node -v

## vérifier les dépendances dépréciées 
npm outdated

## mise à jour des packets npm dans la limite du package-lock.json
npm update

## supprimer la base de donnée locale
meteor reset

## lancer le linter
meteor npm run lint

# PROBLÈME POSSIBLE
## Unexpected mongo exit code 62. Restarting.
suite à une montée de version de meteor il peut être nécessaire de vider la base de donnée locale.

# NPX
npm install les dependances node
npx les executes
#### example:
npm install mongosh
npx mongosh mongodb://localhost:3003/meteor

# MODIFICATION DES PARAMÈTRES PENDANT L’EXÉCUTION DE L’APPLICATION
Il est important de mentionner que Meteor garde une référence au hashmap public à partir duquel il publie les valeurs au client. Pour cette raison, remplacer l'objet complet comme ci-dessous ne fonctionnera pas.

```javascript
Meteor.settings.public = {SETTING_ONE : '1'}
```
Ce qu'il faut faire à la place, c'est utiliser Object.assign pour modifier l'instance de l'objet sur place.

```javascript
Object.assign( Meteor.settings.public, {SETTING_ONE : '1'} )
```

