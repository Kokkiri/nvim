| Permission | Valeur numérique | Type de fichier      | Signification                                                                                                  |
|            |                  |                      |                                                                                                                |
| ---------  | 000              | N'importe quel type. | Aucun accès à qui que ce soit                                                                                  |
| rw-------  | 600              | Fichiers             | Fichier parano : lecture et écriture réservées au propriétaire.                                                |
| rw-r--r--  | 644              | Fichiers             | Standard : tout le monde peut lire le fichier mais vous êtes le seul à pouvoir le modifier.                    |
| rw-rw-rw   | 666              | Fichiers             | Fichier public : tout le monde peut lire et écrire.                                                            |
| rwx------  | 700              | Répertoires          | Répertoire parano : lecture, accès et écriture réservés au propriétaire.                                       |
| rwxr-xr-x  | 755              | Fichiers             | Fichier lisible et exécutable par tous, vous seul pouvez le modifier.                                          |
|            |                  | Répertoires          | Standard : tout le monde peut accéder au répertoire et lire son contenu, vous seul pouvez modifier le contenu. |
| rwxrwxrwx  | 777              | Répertoires          | Répertoire public : tout le monde peut y accéder, lire le contenu et modifier celui-ci.                        |
|            |                  | Fichiers             | Exécutable public : tout le monde peut le lire, l'exécuter et le modifier.                                     |

| Valeur | Droit | Binaire |
| 0      | -     | 000     |
| 1      | x     | 001     |
| 2      | w     | 010     |
| 3      | wx    | 011     |
| 4      | r     | 100     |
| 5      | xr    | 101     |
| 6      | rw    | 110     |
| 7      | rwx   | 111     |

| groupe propriétaire | groupe utilisateur | groupe autre |
| rwx                 | rwx                | rwx          |
