Alexander Pushkin - L'inspiration est nécessaire en géométrie, tout autant qu'en poésie
Alexander Suvorov- Les mathématiques sont une gymnastique de la pensée
Blaise Pascal - La contradiction n'est pas un signe de fausseté, ni l'absence de contradiction un signe de vérité
Emile Lemoine - A mathematical truth is neither simple nor complicated in itself, it is
Galileo Galilei - Les mathématiques sont le langage avec lequel Dieu a écrit l'univers
Godfrey Hardy - Les modèles du mathématicien, comme ceux du peintre ou du poète, doivent être beaux ; les idées, comme les couleurs ou les mots, doivent s'emboîter de manière harmonieuse.
Henry Ford - La réflexion est le travail le plus difficile qui soit, et c'est probablement la raison pour laquelle si peu de gens s'y adonnent
Hugo Steinhaus - Il est facile de passer de la maison de la réalité à la forêt des mathématiques, mais rares sont ceux qui savent comment revenir en arrière
Jhon Locke - Logic is the anatomy of thought
Luca Pacioli - Sans mathématique il n'y a pas d'art
Oscar Zariski - La géométrie, c'est la vraie vie.
Pierre Gassendi - Si nous savons quelque chose, nous le savons par les mathématiques
Raoul Bott - Il y a deux façons de faire de grandes mathématiques. La première consiste à être plus intelligent que tout le monde. La seconde consiste à être plus stupide que les autres, mais à persévérer
