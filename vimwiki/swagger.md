# DESCRIPTION
Swagger génère du code vide et présente une api avec une interface.
Swagger se base sur des api existante pour générer des models d’api.
Possibilité de créer des modèles à partir de zéro.

# LAUNCH SWAGGER
`docker run -p 80:8000 swaggerapi/swagger-editor`

see on https://editor.swagger.io/


