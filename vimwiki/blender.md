| command | description |
|---|---|
|ctrl + :|move dot pivot of object|
|ctrl + space|large window|
|shift + space|main tool panel ( move, size, transform... )|
|shift + s|cursor options|
|shift + tab|aimentation|
|t|open \ close left panel|
|n|open \ close right panel|
|s ( x, y, z)|resize|
|alt + s|resize point of a path|
|shift + d|copy path object (as the hair without the circle shape)|
|r ( x, y, z)|rotate|
|g ( x, y, z)|grab|
|m|collection|
|shift + a|ajouter|
|tab|switch edit mode / last used mode|
|ctrl tab|switch pose mode / last used mode|
|fn .|centre sur l'objet selctionné ( valable dans le graph editor )|
|origin|centre sur la scene|
|ctrl + ( 1, 2, 3, 4, ... )|subdevide object to make smooth and round curve|
|maj + d|copy keyframe in dopsheet ( then move the cursor and left click )|
|o|proportional editing (scroll to change size)|

**script**
doc blender :	<https://docs.blender.org/api/current/index.html>
ctrl + alt gr + /	# comment
alt + p			# execute le script
bpy.data.object["arbre"]		# pointe l'arbre
bpy.context.selected_objects	# pointe l'objet selectionné	

**graph editor**
alt + o			# smooth keys
maj + alt + o		# sample key frame ( comble les vides par des clés )

**mode annotation**
ctrl + click gauche	# erase

**mode edition**
s + z → shift + 0		# aplati les points sur l'axe z

**flip**
add modificateur / mirror

**merge object**
add modificateur / boolean / union

**rigging**
ctrl + p			# avec poids automatique

**avoid other shape deformation by rigging**
bone properties > envelope multiply

**create hair**
create path and circle-path / select path / object data properties / geometry / bevel → and select circle-path as object

**parenting :**
select multiple object, right click and select parent → object.
The last object selcted become the parent

**camera libre**
vue → navigation → déplacement par marche	( can right click on an option to asign a shortcut )
shint + f			# camera free
e / q			# haut / bas
a / d			# gauche / droite
w / s			# avant / arrière
g				# gravity

**camera taille et focale**
object data properties > focale
object data properties > camera

**camera**
put camera to the current view (fn)
`ctrl + alt + 0`

move view to camera (fn)
`0`

**changer l'axe de rotation d'un objet**
positionner le curseur 3D à l'endroit voulu, clique droit --> set origin --> Origin to 3d Cursor
