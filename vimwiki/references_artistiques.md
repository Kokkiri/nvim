Albert Joseph Pénot - peintre
Ana Clerici - peintre
Anne-Sophie Tschiegg - peintre
Antoni Taulé - peintre
Antony Gormley - sculpteur
Arkhip Ivanovich Kuindzhi - peintre
Aryz - graphiste
Barnaby Dixon - marionettiste
Barnard Frize - peintre
Bram Van Velde - peintre
Camille Trouvé - marionettiste
Charles-François Daubigny - peintre
Claire Tabouret - peintre
Dump Type - musique installation
Enric Torres Prat - illustrateur
Eugen Von Blass - peintre
Faye Hsu - illustratrice
Franck Frazzeta - peintre illustrateur
Franck Stella - peintre
François Morellet - sculpteur
George Rouault - peintre
Gilles Aillaud - peintre
Giorgio de Chirico - peintre
Hélène Morlon - mathématicienne écologue - spécialiste de la modélisation de la biodiversité
James Allen St. John - peintre illustrateur
Jean Dupuy - anagramme
Jean Giraud - illustrateur
Jean-François Millet - peintre
Jean-Pierre Pincemin - peintre sculpteur
Jim Pinkoski - illustrateur
Julien-Joseph Ducorron - peintre
Larry Elmore - illustrateur
Luc Tuymans - peintre
Marc Desgrandschamps - peintre
Marjori Rice - mathématicienne
Martin-Johnson Heade - peintre
Maryam Mirzakhani - mathématicienne
Mike Hoffman - illustrateur
Nicolas Bournay - illustrateur
Paul Cox - peintre graphiste
Paul Mccarthy - installation
Peter Stämpfli - peintre
Pierrick Saurin - vidéaste
Ray Turner - peintre
Reuben Margolin - kinetic
Richard Tuttle - sculpteur
Robert Crumb - illustrateur
Robert Maguire - illustrateur
Samuel John Peploe - peintre
Sarah Sze - installation
Shepard Fairey - sérigraphie
Sol Lewitt - peintre
Spain Rodriguez - illustrateur
Steven Shearer - peintre
Tadao Ando - architecte
Tadashi Tokedia - mathématicien origami
Tom Wesselman - peintre
Tsai Ming Liang - cinéaste
Victoria Hart - mathématicienne musicienne
Wilhelm Sasnal - peintre
William-Adolphe Bouguereau - peintre
Yuko Shimizu - illustratrice

[Galerie Catherine Putman](https://www.catherineputman.com)
[Galerie Lelong](https://www.galerie-lelong.com)
[Galerie Gogosian](https://gagosian.com)
[Wiki Art](https://www.wikiart.org/fr)
[Ana Clerici](https://www.anaclerici.com)
https://archive.org/details/adultcomics
