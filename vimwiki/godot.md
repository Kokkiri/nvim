#### À savoir
"post-processing effects" pour les filtres d'écran

#### raccourcies clavier
éditeur → paramètre de l'éditeur → raccourcie

#### script shortcuts
| command | description |
|---|---|
| ctrl + d | dupliquer une ligne |
| alt + ( up_arrow / down_arrow ) | déplacer une ligne |
| maj + scroll | défilement horizontale du script |

#### script
set color in script ( Color take value between 0 & 1 )
`<MeshInstance>.get_surface_material(0).albedo_color = Color(1, 1, 1)`

#### view 3d
centrer sur la selection
`f`
centrer sur l'origine
`o`

fullscreen (while game)
`alt + F11`

#### set transparency
mateials :
flags → transparency : active
albedo → color : A = 0

#### create shortcuts
projet → paramètre du projet → control

#### create class, MeshInstance and change albedo_color
```python
class Face:
	func _init(face, name, rot, trans, color, obj):
		face.name = name
		face.mesh = PlaneMesh.new()
		face.rotation = rot
		face.translation = trans
		var mat = SpatialMaterial.new()
		mat.albedo_color = color
		face.set_surface_material(0, mat)
		obj.add_child(face)
```

#### property : sky_contribution
Node : WorldEnvironment
limite impact of median color sky on object

#### culling
quand l'objet n'est visible que d'un côté