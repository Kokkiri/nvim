# SYNTAX
```javascript
  useEffect(() => {
    if (appsettings.textMaintenance !== msgMaintenance) setMsgMaintenance(appsettings.textMaintenance);
    if (appsettings.userStructureValidationMandatory !== userStructureValidationMandatory)
      setUserStructureValidationMandatory(appsettings.userStructureValidationMandatory);
  }, [appsettings]);
```
Le dernier paramètre indique que l’application doit se recharger à chaque modification de ce dernier.
