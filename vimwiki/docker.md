# INSTALLATION SOUS DEBIAN

#### ADD DOCKER'S OFFICIAL GPG KEY:
`
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
`

#### ADD THE REPOSITORY TO APT SOURCES:
`
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
`

__NOTE : Si vous utilisez une distribution dérivé, tel que Linux Mint, vous devez utilisez `UBUNTU_CODENAME` au lieu de `VERSION_CODENAME`__

If you use an Ubuntu derivative distro, such as Linux Mint, you may need to use UBUNTU_CODENAME instead of VERSION_CODENAME.

#### INSTALLE LA DERNIÈRE VERSION DE DOCKER
`
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
`

#### POUR VÉRIFIER QUE ÇA MARCHE
`
sudo docker run hello-world
`

# INSTALLATION SOUS MANJARO

MISE À JOUR
`sudo pacman -Syu`

INSTALLATION
`sudo pacman -S docker`

CHECK VERSION
`docker version`

GET INFO
`docker info`

---

La différence entre `docker run` et `docker exec` est que `docker exec` exécute une commande sur un conteneur en cours d'exécution. En revanche, `docker run` crée un conteneur temporaire, exécute la commande dans celui-ci et arrête le conteneur lorsqu'il a terminé.

---

CONSTRUIRE UNE IMAGE À PARTIR DU DOCKERFILE
`docker build -t <image tag> <chemin>`
exemple
`docker build -t visio .`

# INSTALLATION DE DOCKER-COMPOSE
`
curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o docker-compose
sudo mv docker-compose /usr/local/bin/
sudo chmod +x /usr/local/bin/docker-compose
`
# COMMANDE DOCKER

LANCER LE DOCKER DEAMON
`systemctl start docker.service`

LANCE AUTOMATIQUEMENT LE DOCKER DEAMON À CHAQUE DÉMARRAGE DE LA MACHINE.
`systemctl enable docker.service`

AFFICHE TOUS LES PROCESSUS DOCKER ( les services ) EN COURS ( -a liste tous les process lancer ou arrêté. -q liste tous les process par leur id )
`docker ps -a`

LISTE LES IMAGES DOCKER
`docker image ls`
`docker images`

STOPER LES PROCESS
`docker stop $(docker ps -aq)`

SUPPRIMER LES CONTAINERS
`docker rm $(docker ps -aq)`
`docker container prune`

SUPPRIME LE RÉSEAUX DOCKER
`docker network prune`

SUPPRIME LES VOLUMES QUI NE SONT PAS TAGUÉ
`docker volume prune`

SUPPRIME LES VOLUMES TAGUÉ ET NON TAGUÉ
`docker volume prune -a`

SUPPRIME UN VOLUME
`docker volume rm <name>`

SUPPRIME TOUS LES VOLUMES AVEC DOCKER-COMPOSE
`docker-compose down -v`

SUPPRIME UNE IMAGES
`docker image rm <images_name>`

SUPPRIME LES IMAGES `dangling` ( images qui ne sont pas versionner par un tag ) ( -a supprimer toutes les images locales )
`docker image prune -a`

POUR SUPPRIMER :
- tous les conteneurs qui ne sont pas en cours d'exécution
- tous les réseaux qui ne sont pas utilisés par au moins un conteneur en cours d'exécution
- toutes les images dangling qui ne sont pas taguées et qui ne sont pas utilisées par au moins un conteneur en cours d'exécution
- tous les caches utilisés pour la création d'images Docker.
( -a pour supprimer les images taguées )
`docker system prune -a`

RELANCE UN CONTAINER
`sudo docker exec -it 6e1f9f9dbdfe bash`

LISTE LES CONTAINERS ( -a POUR AFFICHER TOUS LES CONTAINERS ACTIF ET INACTIF )
`sudo docker container ls -a`

METTRE UN CONTAINER EN PAUSE
`docker pause <id or name>`

LIBÉRER LA PAUSE D'UN CONTAINER
`docker unpause <id or name>`

ÉXÉCUTER UNE COMMANDE DANS UN CONTAINER DEPUIS SA MACHINE
`docker exec <id or name> <cmd> <arg>`

CRÉER ET LANCER UN CONTENEUR EN PRÉSISANT L’IP ET LE PORT
`docker run -p 127.0.0.1:8000:8000 <image tag>`

---

# ISSUE
Si docker ne se lance pas en mode sudo:
[docker got permission denied](https://www.digitalocean.com/community/questions/how-to-fix-docker-got-permission-denied-while-trying-to-connect-to-the-docker-daemon-socket)
