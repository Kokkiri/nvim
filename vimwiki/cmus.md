
###### shortcut
|Commande|Description|
|---|---|
|c|         = pause / lecture|
|q|         = quit|
|x|			= restart|
|v|			= stop|
|b|			= play next|
|z|			= play prev|
|i|			= focus sur la musique en cours|
|r|			= repeat ( don't forget to play continue )|
|s|			= shuffle ( random )|
|f|			= the focus follow the currently playing music|
|Shift + c|	= play continue or not|
|Shift + d|	= supprimer une musique|
|Enter|		= start|
|Tab|			= focus side bar|
|left / right|	= 5s next / prev|
|-|			    = vol +10|
|+|		    	= vol -10|
|:add (folder / file)|	= ajouter un fichier ou le contenu d'un dossier|
|:colorscheme + option|	= changer de theme|

---
###### liste des themes:

- cyan
- default
- green-mono-88
- green
- gruvbox
- jellybeans
- night
- solarized-dark
- xterm-white
- zenburn