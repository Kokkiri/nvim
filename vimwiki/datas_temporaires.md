:expressionless: :point_up: va chier !

# IDENTIFIANT REMBOURSEMENT POLE EMPLOI
6830965M / IC50 / 20230517I

# SETTINGS LABOITE
https://gitlab.mim-libre.fr/alphabet/laboite/-/tree/dev/config
```javascript
"laboiteApiKey": "849b7648-14b8-4154-9ef2-8d1dc4c2b7e9"
```

# CREATION DES METHODES
https://www.youtube.com/watch?v=CQtcXv2z9E4

# TEST NOTIFICATIONS

curl -X  POST -H "X-API-KEY: 849b7648-14b8-4154-9ef2-8d1dc4c2b7e9" \
	   -H "Content-Type: application/json" \
	   -d '{"userId":"M5xThNTNmGLhMpxGr", "title":"test", "content":"test notif par API", "type":"info"}' \
	   http://localhost:3000/api/notifications
     
### obtenir userid dans la console
Meteor.userId()

# CREATION DES SONDAGES
Une fois le sondage créé:
- Aller dans participation copier le lien
- aller dans navigation privé
- copier l’url
- choisir les dates et enregistrer
Pour pouvoir enregistrer le sondage doit être actif, pour ça aller dans __gestion__ et cliquer sur l’__oeil__.
La collection __polls_answers__ apparaitra dans __mongosh__.

# AUTHENTIFICATION EOLE3.DEV TEST

user: benny
mdp: Anticonstitutionelement1!
mail: ben@ac-toulouse.fr

user: eric
mdp: Enrike!de5uède
mail: eric@eric.fr

user: christelle
mdp: Chr!ste11e
mail: bibi@ac-dijon.fr

user: jojo21
mdp: '6AJ}pG%HH9#TsY
mail: joel.cuissinou@ac-dijon.fr

user: melanie
mdp: Melanie21!
mail: melanie@ac-nantes.fr

# LABEL
menuAdminNextcloudUrl

# Gilles
Avec ce que Lionel à fait, je vois pas comment la migration peut marcher.
Si vous supprimer tous les Meteor.settings dans les getSettings(), ça peut pas marcher.
Même si t’as l’impression que ça marche chez toi, ça peut pas marcher.
Si ça marche chez toi, c’est qu’il y a un truc que t’as pas compris.
Je vois comment t’es et il faut que tu arrêtes de te laisser influencer par Lionel.
Ton code marchait. Lionel à créé un bug et après il te demande de corrigé le code ce qui t’oblige à détruire ce que t’as fait. (__page blanche à cause de dataMap[keyLabel] undefined__)
Tous ce qui est dans __defaultValue__ il faut le virer.

# AUTENTIFICATION LABOITE
first name: toto
last name: delamuerta
email: toto.delamuerta@moribon.com
username: edouardo
mdp: mortaupalfrelin

# FEUILLE DE ROUTE AVEC BENOÎT
## La Digitale
[La Digitale](https://ladigitale.dev/)

## Création de carte
[Umap](https://umap.openstreetmap.fr/fr/?p=2)

## Manipulation collaborative de donnée
[Grist](https://donnees.incubateur.anct.gouv.fr/toolbox/grist)

## Intégrer Shlink dans le portail (raccourcisseur d'url)
[Shlink](https://shlink.io/)

## cycle de vie | mise en place avant fin 2024
démarrer ça avant la fin de l'année :
- enregistrement de l'activité de l'utilisateur (type dernière connexion)
- [SCIM](https://www.okta.com/fr/blog/2017/01/what-is-scim/)
- modifier le Keycloak depuis Laboite ? supprimer des utilisateurs par exemple

## Accessibilité
[RGAA](https://accessibilite.numerique.gouv.fr/)

## outil d'aide à la migration (Shadow IT, Google Drive) :
crééer une page qui liste les services et qui importe les donnée dans Laboite
Synchronisation de donnée entre une machine et des serveurs
[Rclone](https://rclone.org/)

## Pastille
[Visio de test webinaire](https://visio-test-1.education.fr/home)

## Calendrier
[Zimbra](https://zimbra.free.fr/)

## Thème DSFR
refonte à 90% de l'interface Laboîte. plusieurs mois de travail

## Avoir un canal de communication entre les groupes

## Fonctionnalité suplémentaires
#### Déterminer le status de la personne
- Occupée
- Présente
- Absente

#### Notifications
- rappel d'évênement
- lien de visio

#### Résumé de visio pour les gens qui arrivent en retard
[Aristote](https://www.centralesupelec.fr/fr/aristote-une-ia-au-service-de-lenseignement-superieur)

# LOOKUP
## MULTITHREAD
[flask gunicorn multthread](https://stackoverflow.com/questions/35837786/how-to-run-flask-with-gunicorn-in-multithreaded-mode)

## CONNEXION AU LOOKUP DEPUIS LAB15
```bash
ssh -A edouard@lab15.labs.eole.education
sudo -sE
su -s /bin/bash - lookup-sever
```
### Les logs sur le lab16
`docker-compose logs -f --tail 20`

### Infos nextcloud
`cat nextcloud-labs.private`

## REDIRIGER LE NEXTCLOUP VERS L'URL DU LOOKUP
### Vérifier l'instance du nextcloud utilisé: exemple lab13
### puis se connecter
```bash
ssh -A edouard@lab13.labs.eole.education
sudo -s
```
### Rediriger le nextcloud vers l'url du lookup déployé avec la commande suivante
```bash
docker exec -u www-data nextcloud-aio-nextcloud php ./occ config:system:set lookup_server --value='https://lookup-server.eole3.dev'
```

# AJOUTER UN SERVICE A UNE STRUCTURE
administration --> gestion des utilisateurs
- donner les droits d'administration de structure
service de la structure --> ajouter un service
réglage globeaux
- public.ui.isBusinessRegroupingMode = True
