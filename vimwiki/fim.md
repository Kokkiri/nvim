
ouvre le dossier complet d'image
`fim <dossier>`

mode ascii
`fim -t <fichier>`

inverse color
`i`

Prev / Next image
`PageUp` / `PageDown` or `b` / `n`

Zoom in / out
`+` / `-`

Autoscale
`a`

Fit to width
`w`

flip / mirror
`f` / `m`

Rotate (Clock wise / ant-clock wise)
`r` / `R`

Quit
`ESC` / `q`

up
`x`

down
`maj x`

