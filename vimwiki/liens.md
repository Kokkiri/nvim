CINQ LIVRES QU'IL FAUT LIRE
https://blog.scottlogic.com/2023/01/03/five-books-every-developer-should-read.html
create your own shortcut
https://itnext.io/make-yourself-comfortable-keyboard-shortcuts-d3348b6d0a19
méthode d'exploit
https://www.youtube.com/watch?v=qpyRz5lkRjE "https://www.youtube.com/watch?v=qpyRz5lkRjE"
Comment connecter un développeur vers son environnement de test (gateway)
https://dev-eole.ac-dijon.fr/doc/eolecitests/installation_gateway_dans_one.html
Les standard du développement
https://dev-eole.ac-dijon.fr/doc/devnotebook/index.html
Doc Eole
https://pcll.ac-dijon.fr/eole/
pôle de compétence logiciel libre
https://www.libre-communaute.fr/
meteor_contest_visio
https://gitlab.mim-libre.fr/edouard.le-bourgeois/meteor_contest_visio
modèle de réseaux
https://dev-eole.ac-dijon.fr/projects/eole-ci-tests/wiki/ModeleReseaux
python POO
http://sametmax.com/le-guide-ultime-et-definitif-sur-la-programmation-orientee-objet-en-python-a-lusage-des-debutants-qui-sont-rassures-par-les-textes-detailles-qui-prennent-le-temps-de-tout-expliquer-partie-1/
CSS en 4 mn
https://jgthms.com/web-design-in-4-minutes/#share
JavaScript en 14 mn
https://jgthms.com/javascript-in-14-minutes/
Pour partager son code
https://paste.cadol.es/zerobin/
Git Bonne Pratique
https://dev-eole.ac-dijon.fr/projects/eole/wiki/GitBonnesPratiques
Configuration éditeur et prompt
https://dev-eole.ac-dijon.fr/doc/devnotebook/getting-started.html
Pourquoi tester
http://www.arolla.fr/blog/2015/09/pourquoi-tester/
JavaScript promises basic
https://scotch.io/tutorials/javascript-promises-for-dummies
JavaScript promises, les trucs à pas faire
https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html
tuto react nodejs mongodb express
https://www.youtube.com/watch?v=ghdRD3pt8rg
info dev devops how to ...
https://devconnected.com/
PROJET REACT AUTHENTIFICATION
https://www.digitalocean.com/community/tutorials/how-to-add-login-authentication-to-react-applications
MISE EN PROD D'UNE APP REACT
 https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs
HOW DO I RUN A NODE.JS APP AS A BACKGROUND SERVICE
https://stackoverflow.com/questions/4018154/how-do-i-run-a-node-js-app-as-a-background-service/29042953#29042953
tuto react nodejs mongodb express
https://www.youtube.com/watch?v=ghdRD3pt8rg
tuto react nodejs mongodb express
https://www.youtube.com/watch?v=7CqJlxBYj-M
Node React Express
https://www.youtube.com/watch?v=klEs4oi3Igc
Restfull api
https://www.youtube.com/watch?v=vjf774RKrLc
connection local
https://www.youtube.com/watch?v=9q-KmjcKKh4
mise en prod d'une app react
https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs
tuto react nodejs mongodb express
https://www.youtube.com/watch?v=ghdRD3pt8rg
tuto react nodejs mongodb express
https://www.youtube.com/watch?v=7CqJlxBYj-M
Node React Express
https://www.youtube.com/watch?v=klEs4oi3Igc
Restfull api
https://www.youtube.com/watch?v=vjf774RKrLc
mongodb connection local
https://www.youtube.com/watch?v=9q-KmjcKKh4
MONGODB 48 COMMANDES À CONNAÎTRE
https://geekflare.com/fr/mongodb-queries-examples/

---
###### AUTRES

concours :					https://www.itrf.education.gouv.fr/itrf/concoursOuvertsNCA.do
material-ui/icons :			https://material-ui.com/components/material-icons/
toast :						https://fkhadra.github.io/react-toastify/introduction
recherche de job :			
recherche d'alternance :

---
###### HARDWARE

split keyboard
***ergodox***

clavier typematrix
***typematrix.com***

ordinateur portable sur mesure
***system76.com***

laptop modulaire
frame.work/fr/fr

clavier ergonomique
***shop.keyboard.io***

---
###### CRÉATION

plateforme de donation pour créateur
***ko-fi.com***
