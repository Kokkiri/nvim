## INSTALL OLLAMA
`curl -fsSL https://ollama.com/install.sh | sh`

## CHOISIR UN MODEL
https://ollama.com/library
`ollama run <model>`

## OLLAMA EN MODE API
https://github.com/ollama/ollama/blob/main/docs/api.md

## WEB SOCKET
https://www.youtube.com/watch?v=o8Fn9BgqoQM
`npm i socket.io`

## PYTHON OLLAMA
https://pypi.org/project/ollama/
`pip install ollama`

### TODO
Arrêter le message en cours s'il reçoi un nouveau message avant qu'il n'est fini de répondre.
Afficher l'heure d'envoie du message
Afficher la date dans l'entête

### ISSUE
Ollama ne mémorise pas ses propres réponses, seulement les messages utilisateurs.

## RAGFLOW (local)
http://192.168.230.223/flow

# RAPPORT:
Pour ce projet, j'ai utilisé les technologies React, NodeJS, Socket.io et Ollama

J'ai utilisé Ollama comme une API avec une interface web pour une interaction avec l'utilisateur.
J'ai utilisé des websockets pour obtenir une réponse au compte goute, ce qui évite d'attendre qu'Ollama est fini d'écrire sa réponse pour la retourner côté interface.

J'ai vérifier avec le prompt "system" qu'il était possible de brider le modèle de langage. On peut donc lui demander de répondre uniquement à des questions qui concernent les données qui lui ont été fourni.

ci dessous le lien vers le dépo git de mon prototype:
https://framagit.org/Kokkiri/ollama-et-socket.io
le prototype se trouve dans le dossier ollama_api/
