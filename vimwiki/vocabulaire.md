| Abréviation | défignition                                           |
|-------------|-------------------------------------------------------|
| DSOF        | Décrochage Scolaire Obligation de Formation           |
| TMC         | Test de monté en charge                               |
| RNI         | Référentiel National d'Identité                       |
| RGAA        | Référentiel Général d'amélioration de l'accessibilité |
|             |                                                       |
