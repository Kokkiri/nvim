
[Settings : impossible de créer un setting dans public depuis l'interface d'admin](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/824) (Lionel)
reste la création de settings depuis l'interface

---

[ajouter des informations métier lors de l'inscription d'un utilisateur local](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/697) (issue)
[ajouter des informations métier lors de l'inscription d'un utilisateur local](https://gitlab.mim-libre.fr/alphabet/laboite/-/merge_requests/1090) (merge)
première connexion:
message de bienvenue
carte d’identité:
	- nom
	- prenom
	- username
	- mail
	- choix de la langue
	- created Date
	- updated Date
- nextcloud:
	- récupération du token
	- id ...
- structure:
	- charger
	- attente
	- message motivation (pour rejoindre la structure)
- options:
	- porter de deconnexion
	- espace perso avancé
	- version beta
	- choix de deconnexion
	- télécharger les données de publications
- Publications:
	- télécharger mes données de publication
	- importer mes données de publication
	- activer mon blog
- Informations métiers:
	- location
	- phone
	- jobTitle
- CGU (Conditions Générales d’Utilisation):
	- text CGU
	- accepter
	- Refuser:
		- suppression compte utilisateur + désactivation du compte keycloak + deconnexion
	dans une fenêtre à part:
	- Version
	- date validation
	- date refus

---

1. Dans les shémas User, ajouter les colonnes: location, phone, jobTitle (pour Moi)

2. Settings:
- information métier (default False)
- activer information métier
- URL CGU

3. UI: gérer le workflow: login --> CGU --> Profil

4. Workflow changement de structure + message motivation obligatoire ou non à vérifier à la mise à jour

## lOOKUP SERVER
[Laboratoire Nextcloup PCLL](https://codimd.mim-libre.fr/E95mJwIuQIqqmUZ1OTkruQ#)
[recherche par mail et federationId](https://gitlab.mim-libre.fr/alphabet/lookup-server/-/merge_requests/26)
[pymongo_asynchone doc](https://pymongo.readthedocs.io/en/stable/async-tutorial.html)
[exemple pour recherche sur champs unique](https://gitlab.mim-libre.fr/alphabet/laboite/-/merge_requests/1181/diffs#fb7c4cf517eb576f6db5454c743015ef62ba99d9)

Créer une issue pour supprimer import de wraps dans __init.py__
supprimer la variable HIDDEN_ADMIN_USER
régler le problème de nclocator

TODO
dermathologue
réparer robinet

## QUESTIONNAIRE
[material icons](https://v4.mui.com/components/material-icons/)
[material components](https://mui.com/material-ui/all-components/)

SwitchInput:
- component builder - gestion des erreurs et des options selon le composant
- manage switch label - gestion des options du composants
- question slice - le store de react
- switch input - le composant
- generate componenent - retourne le composant selon son type
- visualizer - affiche l'ensemble des composants

