| shortcut | description |
|---|---|
|`Alt F4`|Eteindre, veille, déconnexion|
|`Ctrl Alt d`|met toutes les applications en arrière plan|
|`Ctrl echap`|Menu démarrer|
|`Alt espace`|menu de la fenêtre|
|`Alt F4`|fermer l'application|
|`Alt Suppr`|supprimer un bureau|
|`Alt Inser`|créer un bureau|
|`sudo do-release-upgrade`|met à jour la version de xubuntu|
|`ctrl alt left_arrow right_arrow`|change desktop|
|`alt tab`|change windows on desktop|
|`ctrl tab`|change rubric in web-browser or Atom|
|`ctrl t`|open new windows in web-browser|
|`ctrl w`|close rubric|
|`shift alt num`|target rubric number|
|`ctrl page-up page-down`|change between open windows|
|`ctrl F1 F2 ...`|switch on a specify desktop|
|`alt Flèche`|Passe d'une vidéo à une autre sur youtube|
|`super w`|ouvrir le navigateur|
|`super f`|ouvrir file manager|
|`super r`|ouvrir appFinder|
