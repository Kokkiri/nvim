# SAUVEGARDE DE LA BASE AU FROMA BSON
`mongodump --uri="mongodb://127.0.0.1:3001/meteor" --out=laboite-mig29/`

# RESTITUTION DE LA BASE À PARTIR DU DUMP
`mongorestore --drop --uri="mongodb://127.0.0.1:3001/meteor" laboite-mig29/meteor`
