[Les notifications provenant du backend sont en anglais](https://gitlab.mim-libre.fr/alphabet/laboite/-/issues/953)

```bash
rgrep i18n.__\(\`api.notification
```
pages/groups/SingleGroupPage.jsx:                      {`(${i18n.__(`api.notifications.labels.roles.${getUserRoleInGroup(userId, group._id)}`)})`}
components/groups/GroupDetailsPersSpace.jsx:                  {i18n.__(`api.notifications.labels.roles.${getUserRoleInGroup(userId, group._id)}`)}
components/groups/GroupDetails.jsx:              {i18n.__(`api.notifications.labels.roles.${getUserRoleInGroup(userId, group._id)}`)}

# MDP VISIO
eole21;
$2b$10$p7FMuKfdXbd9PsBo9CFfxueqGyMDXPaHcirH//EAWEAxFK/xvfHTu

# Mail Barth
Bonjour Florent,

Ma mission actuel ce termine le 13 Mars.
Je suis disponible si tu as des besoins parmi tes équipes.
À savoir que si vous avez besoin de travailler avec le PCLL, je maintiens de bonnes relations avec mes collègues.
Si tu penses pouvoir bénéficier de mes compétences et de mes relations, je serais ravi d'en discuter avec toi.

Cordialement,

# CODE
def deleteOldNotifications():
    d = datetime.now()
    d = datetime(d.year - 1, d.month, d.day)
    db["notifications"].delete_many({"createdAt": {"$gte": d}})


db = get_database()
checkUsersForFirstMail()
checkUserForInactivity()
checkUserForDeparture()
checkUserForPreventDeletion()
checkUserForDeletion()
deleteOldNotifications()
