load sound
`sound = vlc.MediaPlayer('my_file')`

play sound
`sound.play()`

stop sound
`sound.stop()`

get duration
`sound.get_length()`

get current time
`sound.get_time()`

is sound playing ?
`sound.is_playing()`