# TMUX SHEET-CHEAT
[sheet](https://tmuxcheatsheet.com/)

## create panel
`ctrl b + c`

## find panel
`ctrl b + f`

## close panel
`ctrl b + x`

## rename panel
`ctlr b + ,`

## split horizontaly
`ctrl b + "`

## split verticaly
`ctlr b + %`
