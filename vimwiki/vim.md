###### SHORTCUT

| shortcut                      | description                                                                       |
|-------------------------------|-----------------------------------------------------------------------------------|
| i                             | insertion                                                                         |
| v                             | visual                                                                            |
| C-v                           | visual block                                                                      |
| M-i                           | insertion sur selection                                                           |
| escape                        | quitter mode insertion                                                            |
| :x                            | sauvergarder quitter                                                              |
| :q                            | quitter sans sauvegarder                                                          |
| :q!                           | forcer à quitter                                                                  |
| dd                            | supprimer une ligne                                                               |
| u                             | annuler                                                                           |
| x                             | supprimer une lettre                                                              |
| 5x                            | supprime les 5 lettres suivant                                                    |
| d5w                           | supprime les 5 mots suivant                                                       |
| y                             | copier                                                                            |
| yy                            | copie la ligne                                                                    |
| p                             | paste                                                                             |
| d0                            | supprime toute la ligne depuis le debut jusqu'au curseur                          |
| d$                            | supprime toute la ligne depuis le curseur jusqu'à la fin                          |
| d4 <Left-Arrow> <Right-Arrow> | supprime 4 character à gauche ou à droite                                         |
| r <lettre>                    | remplace un caractère                                                             |
| C-w                           | switch between windows                                                            |
| C-a                           | increment number                                                                  |
| C-x                           | decrement number                                                                  |
| <num>gg or <num>G             | aller à la ligne n                                                                |
| alt gr + k                    | mettre en majuscule ou minuscule                                                  |
| # or gd                       | rechercher les occurences du mot survoler par le curseur                          |
| :noh or C-l                   | annuler une recherche                                                             |
| (visual) u                    | met en minuscule                                                                  |
| (visual) M-u                  | met en majuscule                                                                  |
| :! <command shell>            | permet d’executer une commande shell dans Vim                                     |

---
##### insert on multiline
1. `C v` 
2. `M i`
3. insert your text
4. `escape escape`

https://doc.ubuntu-fr.org/vim

---
##### install plugins with vim-plug

paste command in `plugin.vim` files
`Plug 'neoclide/coc.nvim', {'branch': 'release'}`

then in your vim prompt type
`:PlugInstall`

---

##### COC ( Conquer Of Completion )

installation
`:CocInstall coc-json coc-tsserver coc-snippets coc-rust-analyzer`

list snippets
`:CocList snippets`

create `snippets` folder in your `nvim` folder
then create `python.snippets` file for exemple ( it will automaticaly recognise the snippets as python snippets ) 

content exemple of `python.snippets`
```json
snippet mlimport
import nympy as np
import panda
import sys
endsnippet
```

`mlimport` is the name of my snippet

autocomplete when Enter key is pressed ( in `init.vim` file )
```js
inoremap <silent><expr> <cr> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"
```

https://github.com/neoclide/coc.nvim

---

###### SET FOLDER AS PARENT DIRECTORY
in tree: go to selected directory
`<M-c>`

###### REPLACEMENT

Change chaque 'foo' par 'bar' dans la ligne courante.
`:s/foo/bar/g`

Change chaque 'foo' par 'bar' dans toutes les lignes.
`:%s/foo/bar/g`

Change chaque 'foo' par 'bar' dans toutes les lignes et demande confirmation.
`:%s/foo/bar/gc`

Supprime la ligne à l'endroit du mot + demande confirmation.
`%s/.*text.*\n//gc`

Change chaque 'foo' par 'bar' dans toutes les lignes de 5 à 12 incluse.
`:5,12s/foo/bar/g`

Change chaque 'foo' par 'bar' for all lines from mark a to mark b inclusive.
`:'a,'bs/foo/bar/g`

En mode visuel, change chaque 'foo' par 'bar' pour toutes les lignes dans la selection visuel. Vim automatically appends the visual selection range ('<,'>) for any ex command when you select an area and enter :. Also, see Note below.
`:'<,'>s/foo/bar/g`

Change chaque 'foo' par 'bar' pour toutes les lignes de la ligne courante (.) à la dernière ligne ($) incluse.
`:.,$s/foo/bar/g`

Change chaque 'foo' par 'bar' pour la ligne courante (.) et les deux lignes suivantes (+2).
`:.,+2s/foo/bar/g`

Change chaque 'foo' par 'bar' dans each line starting with 'baz'. 
`:g/^baz/s/foo/bar/g`

supprimer tous les caractères depuis le début de la ligne jusqu’au premier espace
`:%s/^.* /`

ajouter une chaine de caractère en partant de la fin de la ligne
`:%s/$/toto/`

Supprime toutes les lignes à l'endroit du mot
`:g/mot/d`

---

###### COMMAND LINE
Connaître la description d'un raccourci clavier
`verbose map <shortcut>`

###### CORRECT INDENT (this is not autoindent)
(`=`, the indent command can take motions. So, `gg` to get the start of the file, `=` to indent, `G` to the end of the file)
`gg=G`.

###### RELOCATE TAB
`:tabm <num>` or `:tabm -1` or `:tabm +1`

###### COPY LINE FROM VIM TO ANOTHER PROGRAM ( v mod ) 
`"+y`

###### REPLACE TAB BY SPACE
Si nécessaire, définir la tabulation et les éspaces
`set tabstop=2 shiftwidth=2 expandtab`
remplacement
`:retab`

