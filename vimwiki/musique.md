## MUSIC
[Serial Experiment Lain - Ambiant](https://www.youtube.com/watch?v=otUN-OdlpS8)
[Brian Eno](https://www.youtube.com/watch?v=WK5pUw2OOKM)
[ôlafur Arnalds](https://www.youtube.com/watch?v=XmHs_sMDueA&list=PLWUfn0hwzTR0vRrWJNBdl6PsgGJyaUm3K&index=11)
[Joep Beving](https://www.youtube.com/watch?v=AD-8urlz7Gg&list=PLTQdMkca3BJjdPiVOk03RsYGz2P4i6cLx&index=7)
[William Basinski - Cascade](https://www.youtube.com/watch?v=V5s-KLGVcTI)
[William Basinski - Momentary](https://www.youtube.com/watch?v=9gbialQ1o-o&list=OLAK5uy_nUpu-VvrGBxwsDSgFcmGrMf2qYMkOpkZA&index=9)
[Donato Wharton](https://www.youtube.com/watch?v=cI5xPOItQDc&list=PLVx6HpgGCDbGspTo7ECzjq6KykGXBmbVn&index=15)
[Ryoji Ikeda]()
[Alva Noto - Uni Acronym](https://www.youtube.com/watch?v=bRYWpquWqFY)
[Jonathan Fitoussi](https://www.youtube.com/watch?v=_v6uvk4LzBw&list=PLplZDCxq6n4g33aUUGpXs0irgdUmAt-HA&index=10)
[Pauline Oliveros](https://www.youtube.com/watch?v=U__lpPDTUS4)
[Stuart Dempter](https://www.youtube.com/watch?v=tGxqDGo9uDk&list=OLAK5uy_mpZ_QTekVpbzitAEeuLzZO6nJ4cr3LR-s&index=4)
[FoF music - album: 1979](https://www.youtube.com/watch?v=n7boq1delXo)
[Deru - I Would Like](https://www.youtube.com/watch?v=ewOnX3x9vtg&list=OLAK5uy_le0crvH5ZClovIbb4jW0zaaQk1xIYtrpY)
[BYETONE - Plastic star](https://www.youtube.com/watch?v=M1ommoLVLgw&list=PLVx6HpgGCDbGspTo7ECzjq6KykGXBmbVn&index=45)
[Still Corners](https://www.youtube.com/watch?v=jSQKxyUumfs)
[Dump Type - Love/Sex](https://www.youtube.com/watch?v=GSvj2IvsRnw)
[Bonobo - Flutter](https://www.youtube.com/watch?v=JsQhTxckK8E)
Toshiya Sukegawa
[Hiroshi Yoshimura - Four Post Cards](https://www.youtube.com/watch?v=JiHHR9I3XAc)
[Erik Satie](https://www.youtube.com/watch?v=5pyhBJzuixM)
Bill Fontana
[Nobukazu Takemura - Curious child](https://www.youtube.com/watch?v=T_DvZQxiQ4M&list=OLAK5uy_mLq-wy0Px6Jlsj0n8KYJnXrO5yKYpwan0)
[Murcof - Rostro](https://www.youtube.com/watch?v=YHfdOYJohN0&list=PLx8zUw4PoWHh0ir1ULV_38yZ0dj4rb26h&index=125)
[Sugai Ken - Denden](https://www.youtube.com/watch?v=Vsy9ORZuWyg&list=PLVx6HpgGCDbGspTo7ECzjq6KykGXBmbVn&index=53)
[Sugai Ken - Wochikaeri to Uzume](https://www.youtube.com/watch?v=0xVhBhdxVjU&list=PLVx6HpgGCDbGspTo7ECzjq6KykGXBmbVn&index=52)

[Duke Nukem - Aliens, Say Your Prayers!](https://www.youtube.com/watch?v=u3SBTQQ0GJ0&list=PLRo-v7L_Lr1MkblND7LAkRv8W-nBRAamW&index=16)
[Duke Nukem - In Hiding](https://www.youtube.com/watch?v=4wW9CcYmqhE&list=PLRo-v7L_Lr1MkblND7LAkRv8W-nBRAamW&index=21)
[Duke Nukem - Plasma](https://www.youtube.com/watch?v=bgNiF37hn50&list=PLRo-v7L_Lr1MkblND7LAkRv8W-nBRAamW&index=17)
[Duke Nukem - Gotham](https://www.youtube.com/watch?v=AW1r2gTC6ng&list=PLRo-v7L_Lr1MkblND7LAkRv8W-nBRAamW&index=26)

[Groundislava - The Dig](https://www.youtube.com/watch?v=_ZUgK4Fngu8)
[Groundislava - Stealth River Mission](https://www.youtube.com/watch?v=2Nw2nDQP2-4&list=OLAK5uy_kQ2eVSR0l5qCRuig3vWOdPA35qGE-tbKQ&index=9)
[Groundislava - New Flesh](https://www.youtube.com/watch?v=YBmGU2WE1KM&list=OLAK5uy_kQ2eVSR0l5qCRuig3vWOdPA35qGE-tbKQ&index=10)
[Groundislava - Snow Dream](https://www.youtube.com/watch?v=-6UdWLM76yk&list=OLAK5uy_k1cPsh0jHpmr8hk51Zv2eaqYuGSYJU4p0&index=1)
[Groundislava - Bog](https://www.youtube.com/watch?v=-6UdWLM76yk&list=OLAK5uy_k1cPsh0jHpmr8hk51Zv2eaqYuGSYJU4p0&index=4)
[Groundislava - Micro Impasse](https://www.youtube.com/watch?v=-6UdWLM76yk&list=OLAK5uy_k1cPsh0jHpmr8hk51Zv2eaqYuGSYJU4p0&index=5)
[groundislava - On My Birthday](https://www.youtube.com/watch?v=wNgF2Yjc-QQ&list=OLAK5uy_mEKY5puZ6NwdYYFj57nmzGgRxz0CUGqZs&index=4)

[Mitski - The Only Heartbreaker](https://www.youtube.com/watch?v=LmXFF_whkVk)
[Matmos - Last Delicious Cigarette](https://www.youtube.com/watch?v=l5CzWc67aT8)

[Instupendo - Icarus](https://www.youtube.com/watch?v=kBEc7yWgqRc&list=PLpaoRVlpskLOud9xub4eRPtlvZtVjV1-X&index=39) liminal song
[Limerence - Yves Tumor](https://www.youtube.com/watch?v=nee9d0wqBTQ) liminal song
[IT'S OKAY, CALM DOWN](https://www.youtube.com/watch?v=1BU34QxeCqs&t=1349s)
