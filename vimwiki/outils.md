## aptitude
liste les paquets installés

---

## Baobab
Analyseur d'utilisation des disques

---

## bat
alternative à `cat` avec syntaxe colorée

---

## bpytop
moniteur de ressources

---

## exa
un `ls` coloré

---

## cmus
lecteur audio pour terminal

---

## duf
un `df` graphique

---

## Filius
simulation de réseaux

---

## Gigolo
est un outil qui permet de rendre accessible les serveurs distant à mes outils locaux.
éxemple : modifier le code de zephir2 sur le serveur root@zephir2 avec atom

---

## Gobuster
outil pour craquer des mots de passe avec une liste de mot de passe

---

## GNS3
simulation de réseaux ( avancée )

---

## Hexchat
chat

---

## Hydra
outil pour craquer des mots de passe avec une liste de mot de passe

---

## Jinja2
templates python ( définie la forme des messages retournés, des fichiers généré )

---

## Jp2a
conversion d'image en ASCII

---

## Katyusha
modélisation de base de donnée ( modèle merise )

---

## LinPeas
outil pour monter en privilège

---

## Mapascii
affiche la carte du monde dans le terminal
`telnet mapscii.me`

---

## Mermaid
bibliothèque Javascript pour faire des diagrammes

---

## Mocodo
modélisation de base de donnée

---

## Oh_my_bash
bash colorizing like zsh
##### installation
```bash
bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"
```
##### desinstallation
```bash
uninstall_oh_my_bash
```
[oh_my_bash](https://github.com/ohmybash/oh-my-bash)

---

## Protonvpn-cli
VPN Proton en ligne de commande
[installation](https://protonvpn.com/support/linux-vpn-tool/#debian)

---

## Revolt
alternative libre à Discord

---

## Screenfetch
affiche les infos principales conternant la machine

---

## Shotcut
montage vidéo

---

## tldr
alternative à `man` version courte

---

## transfer.sh
transfer de fichier en ligne de commande
[transfer.sh](https://transfer.sh/)

---

## nvm
node version manager
```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
```

---

## Yarn
installer de packet
[installation](https://chore-update--yarnpkg.netlify.app/fr/docs/install)

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
```
```bash
sudo apt-get update && sudo apt-get install yarn
```

---

## youtube-dl
téléchargement de piste vidéo

---

## border-only
un theme XFCE sans bordure
https://www.xfce-look.org/p/1016214/

---

## xclip
copie dans le presse papier en ligne de commande.

---

## fontforge
pour créer des fonts
