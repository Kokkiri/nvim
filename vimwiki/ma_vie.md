# AGE INCONNU

Kokkiri est né au pays de la terre vide et silencieuse, mais son âme vient d'un pays lointain, de l'autre côté de la planète. Son corps le rend prisonnier d'une identité qui l'oblige toujours à revenir là où il est né. Mais son âme le guide toujours vers le pays lointain. Kokkiri doit traverser l'océan de l'incertain qui sépare les deux pays, ce même océan qui sépare son âme et son corps.
Personne ne connaît le chemin du pays lointain. Personne ne l'a jamais vue. De toute façon personne ne se risquerait à traverser l'océan de l'incertain pour chercher un pays que personne n'a jamais vu. Au pays de la terre vide et silencieuse tous n'est qu'illusion. Aux yeux de ceux qui y vivent, la nature des choses est reconnaissable par leur forme. Mais Kokirri ne se fit pas à ses yeux, il se fit à la musique. La musique des êtres, la musique des paysages, la musique des choses. Cette terre, vide elle l'est, car silencieuse elle est. Celui qui sait écouter la musique de toutes choses trouvera le chemin qui mène au pays lointain.

Deux hommes se rencontrent sur une terre. L'un voit le monde en noir et blanc, et l'autre voit le monde en couleur. Ils parlent ensemble du monde autour d'eux.
L'homme qui voit le monde en noir et blanc regarde l'arbre devant lui et dit :
- Ceci est un arbre.
L'homme qui voit le monde en couleur regarde l'arbre et dit :
- Ceci est un arbre.
Et les deux hommes s'entendent.
L'homme qui voit le monde en couleur regarde l'arbre et dit :
- Regarde l'oiseau sur la branche.
L'homme qui voit le monde en noir et blanc regarde l'arbre et dit :
- Le quel ? Il y en a deux.
Alors l'homme qui voit le monde en couleur lui dit :
- Le rouge. Regarde l'oiseau rouge.
L'homme qui voit le monde en noir et blanc n'a jamais vu la couleur et ne soupçonne même pas ce qu'est la couleur. De là naissent deux possibilités :
La première suppose que le mot "rouge" ne signifie rien pour lui, et alors il demanderais ce que cela signifie.
La deuxième suppose que le mot "rouge" signifie pour lui quelque chose d'autre que la couleur. Supposons qu'il s'agisse d'une valeur en particulier, quelque chose de sombre par exemple. "Rouge" signifie pour lui ce qui est sombre.
Supposons que l'oiseau à droite est rouge et claire, et que l'oiseau à gauche est bleu et sombre.
L'homme qui voit le monde en noir et blanc regarde l'oiseau bleu et dit :
- Oui, je le vois.
Et l'homme qui voit le monde en couleur dit :
- Tu as vu ! Il tient un verre dans sa bouche.
Or l'oiseau bleu ne tient pas de verre dans sa bouche.
Et les deux hommes se disputèrent.
Parce que je vous parle de ce que vous ne connaissez pas à partir d'un vocabulaire que vous connaissez, si vous comprenez ce que je dis au regard de votre expérience, vous ne comprendrez jamais ce que je vous dis.
C'est là toute la difficulté de vous amenez à voir ce que vous ne voyez pas encore.
Aussi clair que je puisse être, comment pourrais je vous expliquer ce qu'est la couleur si vous ne la voyez pas ?
Rien ne saurais vous arracher à ce que vous savez et à ce que vous croyez savoir, car après tout, vos définitions du monde s'appuie sur une expérience qui est réel.
Alors vous prendrez pour un insensé celui qui voit le monde en couleur.

La nature de notre planète, sa place dans l'univers a sans aucun doute déterminée les formes de vie qui y habitent.
Notre corps, son A.D.N et sa physiologie a sans aucun doute déterminé nos comportements et nos relations.
Le monde serait-il plus beau si nous avions vécu sur une autre planète ?

Je coupe des planches de bois sur ma table.
Une petite fille me dit :
- Tu vas te marier avec Maman ?
- Je ne pense pas. Je suis trop fragile pour ça.
- Je veux un homme fragile.
- Tu m'apprécie peut-être parce que la relation actuelle avec ta mère ne révèle pas mes handicaps, mais dans une relation plus intime, tu ne me supporterais plus.

Je dis :
- j'ai l'impression que je peux un peu plus approcher les gens mais je ne peux toujours pas les toucher.
Elle me dit :
- Je croyais que j'étais la seule à ressentir ça.

Elle me dit :
- Quand j'étais à l'université, avec des étudiantes de ma classe on a visité une faculté de médecine. On nous a montré un corps humain. Il avait le ventre ouvert et on pouvait voir les organes à l'intérieur. Les autres filles étaient écœurées. Elles ont tourné la tête ou elles sont sorties, mais moi je suis resté comme ça. J'avais les yeux grands ouverts.

J'ai cinq ans, des éléphants passent devant moi et s'en vont.
Je suis dans une garderie. Je joue avec des jouets qui m'ennuient.
Je quitte la garderie et je me retrouve dans un parking souterrain.
Il est vide à part un homme derrière une table. Sur la table il y a des ballons.
J'en prends un que je gonfle, et il prend la forme d'un éléphant.
La société a prévu pour moi la sécurité et le loisir, mais elle m'éloigne de mes désirs,
et je marche seul, à la recherche des éléphants.
Le monde est rempli de baudruche qui ont pris la forme de mes désirs, mais elles ne portent pas le poids de l'éléphant.
Depuis petit jusqu'à maintenant, je cours après les éléphants et je rencontre des baudruches.

Ce fut la première condamnation. On lui brisa les ailes, et il quitta le ciel pour la terre.
Ce fut la deuxième condamnation. Face à son poids, les hommes l'abandonnèrent.
Ce fut la troisième condamnation. Du jour où il freina le mouvement des autres dans le temps, il fut banni de la surface de la terre, et on le jeta dans la mer où il coula.
L'homme traversa le ciel, la terre et la mer dans un mouvement verticale.

Dans ses peintures ses pieds ne touchent pas le sol, car la fille de papier est comme une montgolfière, à l'intérieur d'elle brûle un feu qui la porte dans les airs.
Ce qu'elle voit d'en haut est tout aussi réel que ce que les gens voient d'en bas, mais personne ne la croit.
Elle voulait voyager, ouvrir une boutique où elle vendrait les rêves qu'elle fabrique.
Mais elle s'est marié avec un homme d'en bas, alors elle quitte ses rêves pour pouvoir le toucher.
Un corps qui brûle a besoin d'air, mais dans sa solitude, elle s'asphyxiait.
L'air est en haut mais le combustible est en bas.
Prise entre ses besoins et ses désirs, elle reste en suspend, les pieds au dessus du sol.

Je suis dans une pièce vide. Devant moi il y a une porte. Derrière la porte il y a de l'eau, rien d'autre que de l'eau. Si j'ouvre la porte, l'eau s'engouffre et je me noie. Si je ne fait rien, l'air finira par me manquer et je mourrai.
Depuis que je suis petit, le choix m'est laissé entre une mort immédiate et l'attente passive devant la mort. J'ai choisi sans aucune volonté l'attente passive jusqu'à ce que mes ressources s'épuisent.

Comment tout cela a commencer ? Je ne m'en souviens pas.
Cette mécanique relationnelle ne se décrit pas au regard de ce qui ce passe dans les actes.
Ainsi en va t'il de certaine violence qui touche le cœur et dont la raison ne peut rien en dire.
C'est une histoire dont je ne me souviens pas, mais je sais avec mon cœur qu'elle est une vérité.

L'enfant fut stoppé car sur son chemin apparut un gros trou et il ne pouvait le traverser seul.
Il marchait vers d'autres enfants qui suivaient leur chemin, mais aucun ne voulait lui tendre la main, car si aucun ne voyaient le trou, l'immobilité de l'enfant présageait un danger. Et personne ne voulait ralentir pour marcher à son rythme alors que la vie devant eux semblait bien plus excitante.
L'enfant espérait donc que les adultes l'aident, mais les adultes ne voyaient pas le trou sur son chemin. Ils voyaient simplement qu'il n'avançait pas et prenaient ça pour un manque de volonté.
Certain se proposèrent de l'aider.
On lui proposa d'apprendre à conduire une voiture, mais il ne le fit pas. La voiture était de l'autre côté du trou.
On lui montra comment chercher du travail, mais il s'en moquait. Le travail se trouvait de l'autre côté du trou.
Il aurait aimé avoir une copine, mais aucune fille n'aurait traverser le trou pour le rejoindre. S'il traversait le trou pour approcher une fille, il tombait.
Les filles non plus ne voyaient pas le trou, mais elles voyaient un garçon qui ne bougeait plus et qui ne parlait plus. Alors elles fuyaient, car la mort fait fuir les gens.
Puisque l'enfant n'avançait pas et, semble t'il, ne faisait pas d'effort malgré les aides qu'on lui proposait, les adultes le stigmatisaient et le culpabilisaient.
Il y a une chose qui traverse le trou, c'est les mots. Et les adultes disaient des mots pour tirer l'enfant de force vers l'avant. Mais les mots tiraient l'enfant vers le trou que personne ne voit, alors l'enfant resista à la parole des adultes, et les adultes dirent de lui:
"C'est sa faute s'il refuse d'aller vers les autres.
"C'est sa faute s'il ne cherche pas de travail.
"C'est sa faute s'il n'a pas de copine."
Et ils diront encore:
"Nous avons essayé de l'aider, mais c'est lui qui ne veut pas de notre aide."

Tu es un homme qui donne à boire à une plante dont tu as toi-même stérilisé le sol.
Alors que la plante se dessèche, Tu lui dis:
"Malgré ce que je fais pour toi, tu ne fais aucun effort."
Mais tu as souillé le monde intérieur et invisible, et tu ne reconnais pas tes crimes.
Il est plus facile de donner à boire que de fertiliser le cœur stérile d'un homme.

L'expression "Je vis un cauchemar" signifie deux choses:
L'une que l'on vis l'horreur, l'autre que c'est irréel car peine croyable.
Comment puis-je convaincre qui que ce soit de ce que je vis alors que moi-même ne suis pas convaincu?

Pour rentrer ou sortir d'un vaisseau spatial, il faut passer par une chambre de dépressurisation. Mais une des deux portes est cassée, et je suis prisonnier de mon propre vaisseau.
J'ai développé des stratégies pour survivre aux circonstances intérieurs mais ces stratégies sont inadaptés aux circonstances extérieurs. Aujourd'hui, je suis autant prisonnier du vaisseau que des habitudes que j'ai prise pour survivre.

Vous qui regardez le monde de l'extérieur, vous me jugez lâche et passif, mais vous ignorez contre quelles circonstances intérieurs je fais fasse.
Je n'ai pas les moyens de réparer la porte de la chambre de dépressurisation depuis l'intérieur du vaisseau. Ces moyens sont à l'extérieur. et vu de l'extérieur, il semble qu'il ne se passe rien. Ce vaisseau est inactif. Je ne parle pas, je ne pleure pas, je ne fais pas appel.
Le problème, c'est le différentiel de pression entre deux mondes et aucune passerelle entre les deux.
Si la porte venait à lâcher, aucun mot ne sortirait sinon une gerbe d'émotion et la peur du rejet.

Pourquoi n'y a t'il pas de conflit ouvert ? Parce que chacun de ses regards, ses sourires, ses paroles cache une promesse cruelle, celle que je vais mourir.

Un corps sans bras ni jambe. Ni ne se porte, ni ne porte les autres.
Si lourd de culpabilité que les autres ne le porte pas non plus. Il ne bouge plus.
Le monde n'a pas les moyens de porter les mots qui sortent de sa bouche. Il ne parle plus.
Seul lui reste les yeux pour atteindre du regard ce qui du monde lui est inaccessible.

Les novas sont les étoiles les plus rare de l'univers. Elles sont si massives qu'elles s'effondrent sur elles-mêmes.
L'effondrement augmente la température du noyau qui, à son tour repousse les strates vers l'extérieur.

Plus la souffrance est intense, plus le désir de vivre est intense.

Le noyau se refroidit et l'effondrement reprend. L'intermittence entre l'effondrement et le rebond continue jusqu'à ce que le noyau manque de carburant hydrogène. Alors l'étoile implose.

Une fois j'ai entendu dire qu'une personne à fait cinq tentatives de suicide avant d'y arriver.

Tous les corps s'attirent. Si une planète possède un satellite, alors une partie de sa masse est attiré vers le satellite.
Si la pression qui s'exerce sur le noyau est moindre, est-ce que la durée de vie de l'étoile augmente ?
Si deux étoiles gravitent l'une autour de l'autre, est-ce que leur durée de vie à chacune augmente ?

Je me demande si deux novas peuvent graviter l'une autour de l'autre...

C'est un océan recouvert d'un tissu, et sur ce tissu il y a un motif. Tous ce qui a du poids s'enfonce
dans le tissu, et tous ce qui s'enfonce dans le tissu déforme le motif.
Le motif devient confus, et sa confusion est aussi profonde que la vérité qui s'y cache.
La gravité fait perdre de vue la vérité que représente le tissu, comme elle ralenti le temps.
Le traumatisme est un trou noir où le temps s'arrête.

Hier j'ai pleuré. Ça faisait longtemps que je n'avais pas pleuré.
Aujourd'hui j'écris. Ça faisais longtemps que je n'avais pas écrit.
Écrire et pleurer, voilà deux chose qui semblent liées.
Je ne me souviens plus très bien pourquoi j'ai pleuré, mais ça se terminait ainsi:
"il y a des besoins tout aussi fondamentaux que manger, tel que l'amour, la justice et le dialogue."
Il y a 3 mois j'ai trouvé un travail, et j'ai peine encore à le réaliser. J'ai si peu travaillé dans ma vie.
Pourtant j'ai bien du mal à me réjouir de ce qui m'est donné, car rien de ce qui m'est donné ne remplit mes besoins fondamentaux.

Les mots qui sortent de notre bouche sont un language linéaire, pourtant notre pensée ne saurait-être linéaire. Il y a donc un conversion entre la pensée et les mots.
Le language des mots semble si clair, si sensé en comparaison des mathématiques ou de la programmation.
Ce qui est difficile, c'est le chemin du retour. C'est la conversion du linéaire vers le dynamique.
Je veux rentrer.

Ce matin en me levant, la seule chose dont je me souvenait c'est: Tout n'est qu'illusion.
Au fond de moi je sais ce que cela signifit. Dans mon rêve tout avait l'air si clair. Mais à mon réveil plus aucun mot ne venait se poser dessus.

C'est une contradiction que j'ai peine à résoudre.Je porte l'univers en moi et une seule chose m'obsède, le connaître tout entier. Mais la vie est trop courte pour que cela arrive. Je consacre ma vie à marcher vers toi, pourtant je sais que je mourrais sans jamais t'avoir connu.

Dans ma tête il y a un océan, et je m'enfonce dedans. La lumière se fait rare, la pression augmente et l'oxygène me manque.
Dépouillé de toute ressource, je me demande de quel courage il faut s'armer pour traverser l'océan de l'incertain.
Au plus profond de cet océan, là où la vie a déserté, j'ai envie de croire qu'il existe une étincelle qui va embraser tous mon être, comme une source de vie infini. Mais la seule étincelle qui demeure est l'illusion que cela va arriver. Et c'est peut-être à cette illusion que tient mon courage.

Je sens une contradiction au fond de moi, comme une voiture sans essence qu'il faudrait que je conduise jusqu'à la prochaine station service.
J'ignore si l'epoire que je nourris m'aide à pousser la voiture jusqu'au prochain relais ou s'il me rappelle tout ce que je ne toucherais jamais.
l'espoire est la représentation d'un lendemain meilleurs dont on ignore complètement s'il existe.

Le goût est au vin ce que la matérialité est à l'objet.

Nommer un corps, c'est nier toute sa mécanique.
Nommer sa mécanique, c'est lui donner tout son sens.

L'art et les mathématiques sont identiques, au sens où on réduit un problème à sa forme minimum pour lui donner sa visibilité maximum.

Entre mourir et souffrir, le choix peut paraître évident, si seulement l'intensité du désir de vivre n'augmentait pas en même temps que l'intensité de la souffrance. Le suicide est synonyme du désir profond de vivre.

Difficile de se battre quand la reconnaissance qu'on attend est porté à un seuil dont on ignore la hauteur.

Parfois je me dis que Dieu est une équation. Il n'a aucune matérialité. On ne peut pas le représenter. Il est constant et omniprésent, et l'univers est l'expression matérielle de Dieu.

La beauté est partout, dans la joie, la tristesse et l'horreur. Elle est l'horlogerie du monde qui organise les scènes de la vie. Qui met en lumière cette mécanique, fait apparaître la beauté.

Qui a écrit cette histoire avant que je ne la vive ? C'est un film, il bouge, il s'anime, pourtant il est impossible de modifier le scénario.

D'un seul geste il perdit le droit de réclamer justice pour tout ce qui lui a été fait par le passé. Car si l'homme commence victime, il ne le finit jamais complètement.

Nous aimons notre vie quand nous acceptons le futur qui se rapproche de nous et le passé qui s'éloigne de nous. C'est-à-dire que nous acceptons le courant dans lequel nous emporte le mouvement du monde.

Tu avais des gestes a mon égard. Je les trouvais maladroit mais ils existaient, alors je trouvais que tu étais plus forte que moi.

Je ne sais pas si je crois en Dieu mais je sens que je m'en rapproche.

le monde n'est pas en quête de vérité mais en quête de légèreté, car la vérité est liée à la gravité.

Le désir de vitesse, c'est la sensation d'avancer quand rien ne pousse à l'intérieur de soi.

Nous avons été dépouillé de notre identité comme le sol de sa fertilité. Nous sommes toujours là, chez nous, pourtant nous avons tout perdu.

Je le sais c'est trop tard. Tout est trop tard. Alors je ne fais rien, et le retard s'accumule.

Comment tenir une quelconque affirmation quand tous ce qui est devant nous reste toujours plus flou que ce qui est derrière nous, et que tous ce que nous mettons en lumière remet en question tout ce que nous avons découvert.

Si l'art est un chemin, la programmation est un véhicule.

Je pose des questions pour approfondir, et à chaque réponses nouvelles, pas de sens. Et en moi demeurent toujours les mêmes questions.

Ce que vous saisissez du réel vous échappe aussitôt que vous le saisissez, car le réel est insaisissable.

Observer le monde c'est tenter de comprendre le language de Dieu.

Je ne sais pas ce qu'est Dieu, et je crois que je le saurais encore moins quand je découvrirais que la volonté n'a jamais précédée aucun changement, mais c'est un changement dans le coeur de l'homme qui a précédé sa volonté.

Cadrer le réel, c'est donner au réel les moyens de son expression.

L'information dit quelque-chose de ce qui est. L'expression est.
Une équation ne dit rien du réel. Elle n'informe pas sur le réel, elle est le réel.

La recherche est un mélange de doute et de conviction. La conviction du coeur, le doute de la raison. Le résultat de la recherche confirme ou démentit les théories mais il éclaire toujours la raison.

On nous donnes la parole mais on ne nous donnes pas les moyens de notre expression. Tout est mensonge.

La vérité confuse du cœur contre l'assurance de la raison arrogante.

Se satisfaire de l'humiliation, n'est-ce pas jouir de l'impuissance que l'on fait subir ?
Jouir et rendre impuissant, n'est-ce pas du viol ?

# 30 ANS

- l'horreur je ne vous la dirais pas. Je souffrirais moi-même une seconde fois de m'en souvenir.

- Comment remettre en question nos propres outils de pensée à partir de ces mêmes outils de pensée ?

- On peut être éloigné de Dieu dans la façon dont on croit en lui et proche de Dieu dans notre attitude, que l'on croit en lui ou non.

- On traitre l'autre être humain comme un objet jetable. __Christianne Singer__
- On meurt affamé dans des montagnes de chose. __Christianne Singer__

- La foi est un phénomène de convection entre le cœur et la raison. La raison donne au cœur les moyens de son expression, et ce que révêle le cœur transforme la raison.

- Les yeux te disent par où tu peux passer, le cœur te dit où tu dois aller.

- Quand la feuille tombe, tout participe à ce qu'elle tombe à l'endroit de sa chute. Mais qui l'a voulu ? personne. Qui a participé ? Tout le monde. Qui est coupable ? La feuille.
L'univers conspire à ce que chacun suivent sa trajectoire, la seule possible.

# 31 ANS

J'ai cinq ans. Je suis assis sur le trottoir avec un ami.
Je lui dis :
- Parfois j'ai l'impression que je suis dans un rêve, et j'imagine qu'un jour je vais me réveiller, et je vais découvrir un monde nouveau.
Il rigole.

- Le diable est le paradoxe qui s'immisce dans le coeur de l'homme, là où sa conscience n'y a pas de vue.

- Je ne sais pas ce qu'est Dieu, et je crois que je le saurais encore moins quand je découvrirais que la volonté n'a jamais précédé aucun changement mais que c'est un changement dans le cœur de l'homme qui a précédé sa volonté.

- Pourquoi n'y a t-il pas de conflit ? Parceque chacun de ses regards, ses sourrires, ses paroles cache une promesse, celle que je vais mourir.

- Je crois que Dieu est d'une profonde simplicité mais si difficile à retrouver dans la confusion du monde.

# 33 ANS

- C'est un océan recouvert d'un tissu, et sur ce tissu il y a un motif.
Tout ce qui a du poids s'enfonce dans le tissu, et tout ce qui s'enfonce dans le tissu déforme le motif.
Le motif devient confus, et sa confusion est aussi profonde que la vérité qui s'y cache.
La gravité ralenti le temps comme elle fait perdre de vue la vérité que représente le motif.
Le traumatisme est un trou noir où le temps s'arrête.

- Les mots qui sortent de notre bouche sont un language linéaire, pourtant notre pensée ne saurait-être linéaire.
Il y a donc une conversion entre la pensée et les mots.
Le language des mots semble si clair, si sensé en comparaison des mathématiques ou de la programmation.
Ce qui est difficile, c'est le chemin du retour. C'est la conversion du linéaire vers le dynamique.
Je veux rentrer.

- Je pense à Bo-Kyung. Et si tout avait été différent lors de notre première rencontre.
Je lui aurait donné mon numéro de téléphone, nous nous serions revu à Séoul et un jour nous nous serions marié.
Je lui dit :
- Et si nous adoptions un enfant ?
- Je ne sais pas, je veux un enfant qui vienne de moi. Et si on avait 2 enfants ?
- Je ne sais pas, mais si tu veux que l'enfant vienne de toi, je l'accepterais.
Un jour nous avons un enfant, et un autre jour elle a quatre ans, et un autre jour elle meurt.
C'est la première fois que je ressens le manque d'une personne que j'aime.
Je suis dans un restaurant avec Bo-Kyung.
Je lui dit :
- Je sais que la douleur ne nous quitte pas, mais je t'aime. Je ne sais pas ce que tu ressens au fond de toi, mais je t'aime.
Je vois les larmes qui coulent sur ses joues.
- Moi aussi je t'aime.
Je pose la peaume de mes mains sur ses yeux et le bout de mes doigts viennent se glisser dans ses cheveux.
Ses mains reproduisent mon geste de façon symétrique et viennent se poser sur mes yeux.
Ce qui m'étonne, c'est la synchronicité des émotions. Les être humains semblent réglé comme des horloges, les circonstances faisant, il affichent la même heure.
Nous pleurons.

- Moi je t'aime parce-que je n'ai pas de papa, mais toi tu m'aimes pourquoi ?
- Je t'aime parce-que je n'ai personne à aimer.
- Si tu trouvais quelqu'un d'autre à aimer tu ne m'aimerai plus ?
- Si tu retrouvais ton papa tu ne m'aimerais plus ?
- Non je t'aimerais toujours.
- Moi c'est pareil, si un jour j'aime une autre femme, je t'aimerais toujours.

- J'aimerais me receuillir sur une tombe pour pleurer, mais je ne connais pas de tombe qui m'attend.

- Je suis devant une tombe. Je pleure et je me dis :
Le passé est le passé. Je reste convaincu que le temps n'existe pas. Si cela est vrai et que le passé est définitif, alors je suppose que le présent et le futur sont définitif. Mais si cela est vrai et que le présent et le futur ne sont pas définitif, alors je suppose que le passé n'est pas définitif.
Le passé n'est peut-être pas le passé.

- Si mes choix sont le fait d'un processus sous-jacent à ma conscience, sont-ils des choix ?
Si le changement n'est que la transition entre 2 phases à l'intérieur d'un cycle, est-ce un changement ?
Aucun changement n'est prévu sinon l'infatigable dynamique de la vie.
Nous pensons que le temps engendre la dynamique du monde, mais c'est la dynamique du monde qui donne l'illusion du temps.
Nous pensons que nos choix transforme la dynamique du monde, mais c'est la dynamique du monde qui donne l'illusion du choix.

- À un moment ou le tissu social de ma vie est réduit à un fil, une idée effroyable me traverse l'esprit :
Et si tout ce que l'on vit et tout ce qui a été vécu se répète à l'infini ?
Ce qui rendait cette idée effroyable, c'est l'intime conviction qu'elle était probable. Elle ne rentrait en contradiction avec aucune de mes croyances et de mes convictions. Elle était même une déduction logique de mes croyances et de mes convictions.

- Je pense à mes regrets. Toutes ces occasions manquées de rencontrer une femme.
Je pense à la quête de vérité. Toutes ces questions que je me pose dont les réponses seraient des solutions aux problèmes de la vie.
Je pense au temps qu'il me manque pour penser et créer.
Je consume chaque jour comme une cigarette dont les cendres sont un amas de présent mort et l'avenir une succession de mort à venir.
Rien de bon ne me retiens, rien de bon ne m'attend.

- Je croyais que la poésie m'éloignerait de la réalité, et que si je m'y enfonçais, je ne trouverais pas le chemin du retour. Mais je me suis trompé. Ce que nous appellons la réalité est une illusion. La poésie est un pas qui nous rapproche du réel.

- La création est égale à l'enfant dans le ventre d'une femme. Elle est en nous, mais se développe indépendemment de nous.

- Ma vie n'a pas d'importance, mais la vie n'a t'elle pas d'importance ?

- Je suis content et triste à la fois. Je suis content de te revoir et si triste de voir sur ton visage tout ce temps que nous avons passé éloigné.
Je ne suis pas passé à côté de la vie parceque je me suis trompé, mais parceque je n'ai rien pu faire. Comme un raz de marrée à l'horizon, rien ne sert de courrir.
J'ai mal de te voir heureuse car tout nous sépare. J'ai été emporter par la violence de la vie, et toute l'expérience que j'ai acquise dans les profondeurs du coœur humain serait inutile  à la surface du monde, comme un poisson naufragé sur le rivage. Mes talents ne se révêlent que là où le regard des hommes ne portent pas.

- Je t'écrirais toutes les semaines.
- Je ne crois pas que tu m'écriras. Mais on continuera de penser l'un à l'autre même dans plusieurs années, je le sais. Un jour tu te marieras et tu finiras par m'oublier.
- Non tu te trompe, je ne t'oubliai pas.
- Il le faut.
- Pourquoi pleures-tu ?

- Je porte l'univers en moi et une seule chose m'obsède, le connaître tout entier. Mais la vie est trop courte pour que cela arrive.
Je consacre ma vie à marcher vers toi, pourtant je sais que je mourrais sans jamais t'avoir connu.

- Chez moi est quelque part en moi, mais je n'ai toujours pas trouvé le chemin du retour.
Les rêves me sondent là où je suis incapable d'aller les yeux ouvert.

# 35 ANS

- "... Ces femmes qui ont été violé, qui sont entre guillemet salies, cassées, dont personnes ne veut parcequ'elles sont salies et elles sont cassées" __Stéphanie Gibaud__

# 36 ANS

- J'ai rêvé de Fanny. Mais dans ce monde là on s'entendais bien. On sortais ensemble, on discutais, j'étais détendu. Je n'ai pas fui.
Au réveil la réalité dans la-quelle j'étais de retour contrastais. Comme si l'espace d'un rêve, ma conscience avait été celle d'un homme heureux, heureux de pouvoir construire des relations comme je n'ai jamais pu le faire.
Mais ce n'est pas la seule différence. Je me regardais à travers les yeux de Fanny, et elle n'avais rien vu en moi de pathétique. Au réveil le contraste était double: la réalité et la conscience de moi.
J'ai pensé :
- Je n'ai jamais construit de relation avec personne car j'ai trop honte de moi.
La honte, c'est le déplacement de la responsabilité. La responsabilité collective d'un drame.
Pour moi, c'est celui du viol et de la maltraitance. Mais je n'ai pas honte d'avoir été violé ou maltraité. J'ai honte de n'avoir jamais construit de relation avec personne. Car si la honte désignais le drame, le drame serait entendu et la responsabilité serait partagée.
La honte, c'est le manteau du pathétique qui couvre le dramatique. C'est la reponsabilité collective porté par un seul homme.
Finalement, tout ce que j'ai à raconter de ma vie, ce ne sont que des fantasmes, le reste n'a jamais existé.

- L'écriture est une action si lente et fastidieuse en comparaison de tout ce que j'aimerais vomir instantanément.
Si la parole est une action plus rapide, elle est traitre. Car une parole qui s'accorde avec le cœur est une parole dont les mots se laissent murir.
L'écoute est égale. C'est laisser murir les mots que l'on reçoit. C'est croire que les mots ne sont pas les mots.
Le pathétique, c'est l'impacte futile d'un geste déterminé.
Je veux crier, mais je ne veux pas être pathétique.
Je veux parler, mais je ne veux pas me trahir.
Je veux être entendu, mais je ne veux pas être trahi.
Alors j'écris, mais personne ne me lis.
Au fond, tout ce que je fais est pathétique.

# 37 ANS

- Je ne sais pas ce que je cherche, mais je le saurais quand je le trouverais.

- Il y aura toujours une grande différence entre soumettre la multitude et régir une société. __Jean-Jacques Rousseau__

- Faire des mathématiques c'est donner le même nom à des choses différentes. __Henri Poincaré__

- Ce qui me déchire le cœur c'est l'irréversibilité du temps passé sans amour.

- Aujourd'hui j'ai un speed-dating dans une brasserie. Je pense à Daphnée.
Arrivé sur place, je commande un verre et je monte à l'étage. Là, sept hommes et une femme attendent. Les hommes sont candidats, la femme est organisatrice. Les hommes ont plus de quarante ans. Ils ont l'air de se connaitre pour certain. Ils discutent.
Il est prévu une dizaine d'hommes et quatre femmes. Deux femmes viennent d'entrer. Elles ont une quarantaine d'année.
Je ne me sens pas à l'aise. Je n'aime pas le regard de tous ces gens. Je quitte la pièce et je termine mon verre en bas.
Je rentre chez moi, je m'assoie sur le bord de la baignoire et je pleure.
Ce n'est pas une émotion ponctuelle. C'est une émotion constante que je porte silencieusement chaque jour.
Daphnée ou le visage de ces gens, c'est le rappel du temps qui passe. C'est comme avoir traversé un désert pour atteindre une terre ou personne ne m'attend.

- La sensibilité est le soin que l'on met à éduquer sa perception afin de toucher le réel.

# 39 ANS

- On devient un homme quand on couche avec une femme, mais un homme au sens de la virilité.
On devient un homme quand on s'humilie, mais un homme qui a gagner son humanité.

- Le désir est plus fort que la conscience. Et quand ce dernier n'est pas moral, apparaît dans notre esprit une instance qui dupe notre conscience pour assouvir notre désir.

- Ce que je crains. C'est que je me trompe sur qui elle est et ce dont elle a besoin. On peut-être n'a-t-elle besoin de rien, ou plutôt n'a t'elle pas besoin de quelqu'un de fragile. Et alors je me dirais, personne n'a besoin de moi. Je sens dans tête le poids de la gravité et je n'ai pas envie de sourire. Je ne suis même pas capable de lui rendre son sourire. Peut-être cela la fait elle douter.
Quelque-chose s'épuise.
Ce que je comprends, c'est que nos relations sont determinés par des processus chimique contre les-quelles on ne peut rien.
L'amour me confronte à l'impuissance, et en dehors de cela, la vie est d'un profond ennuie.
Ce n'est pas moi qui l'aimerais et ce n'est pas moi qu'elle aimera. Elle trouveras quelqu'un d'autre.
J'ai attend cette phase où je n'ai plus envie de pleurer.

- Que l'on m'explique comme on crie pour vomir ce qui nous tiraille.
Quelles sont ces équations qui régissent ma vie et contre les-quelles je bataille,
mais qui me rappellent dans chaque scène où elles se répètent l'impuissance qui me tenaille.

Je me rappelle du jour où me voyant vous riiez,
car sur mon front, de ma casquette, les traces étaient restés.

Je me rappelle du jour où, dans la précipitation de me voir, vous fîtes un amalgame.
Les mots n'ayant suivi l'élan de votre hâte, vous m'appelâtes "Madame".

Je me rappelle du jour où, sur un lieu innatendu,
je vous voyais travailliez,
c'était derrière un comptoir.
"On s'est déjà vu !"
vous êtes vous exclamez,
ne vous attendant pas à me voir.

Et où deux ans plus tard,
accompagné d'an ami et profitant de l'occasion,
à tous les deux vous nous dîtes, et non sans émotions:

"Cela fait longtemps que l'on ne s'est pas vu !
Cela me fait plaisir de vous revoir."
d'un même sentiment protégé du même alibi je vous ai répondu:
"À nous aussi cela nous fait plaisir de vous revoir."

Et la semaine suivante, alors qu'un autre client vous serviez,
derrière la vitre du présentoir, un sourire fugace vous m'offriez.

Alors que de nous deux, votre regard sur moi le premier s'est posé,
Ces images dans ma tête se sont mélangées,
et de vous amoureux je suis tombé.


- Boujour je suis venu voir la patrone.

- Bonjour c'est pourquoi.
- Je suis désolé de ne pas être venu vous voir plus tôt
- qu'est-ce qui ce passe, vous me faites peur
- Ben je voudrais qu'on se revoit
- je ne comprends pas
- je voudrais vous inviter à boire un verre.
- je ne suis pas dans cette optique.
- Je me suis trompé...
- Je suis désolé si je vous ai laissé pensé le contraire.
- C'est pas de votre faute. C'est la mienne.
- Je ne vous importunerais plus.
- Je suis désolé, au revoir.

Un jour un homme à tenté de se suicidé après qu'une femme est refusé ces avances.
Suis-je destiné à vivre éternellement ce genre de situation humiliante.
Tout ce que j'écris était faux depuis le début. Toutes mes croyances s'effondrent.
Ma grille de lecture est complètement erronée.
J'aimerais rentré. Mais rentré où, J'ai nulle part où rentrer. Nul endroit où qui me ressource.
La solitude est la porte ouverte à la dépression. Je ne sais même plus si le fait d'écrire me soigne ou me rend fou.
Je ne sais pas où est la réalité, mais si je vis dans une illusion, je n'ai pas le courage de m'en échapper.
Il n'y a rien a attendre de la vie si je suis fou.

Je n'ose pas regarder mon comportement en face car il me fait honte.

J'ai l'impression d'avoir dépasser le seuil où seule l'humiliation m'attends.

- Mardi 16 04 2024, j'ai revu une femme que j'avais croisé dans mon ancienne entreprise. Nous nous sommes vu que Deux fois. Mais si mon cerveau ne m'a pas menti, j'avais senti une attirance mutuelle entre nous.
Alors que nous marchions chacun dans un sens opposé, je l'ai reconnu et j'ai eu une importante accélération cardiaque. (L'impuissance). Elle n'a pas tourné le regard vers moi.
Mercredi 17 04 2024, j'écoute "[Le grand dimanche soir](https://www.youtube.com/watch?v=6riVyOibnTU)". Est présenté un livre de William Irish - La mariée était en noir. Histoire d'une femme qui tue des hommes, présentés comme médiocre.
S'ils sont médiocre. Qu'est-ce que je suis me dis je ?

Vient après une humoriste (30:30 - "tant qu'on me fait sauter l'opercule de temps en temps") qui dit que si les falus poussait sur les arbres elle serait la reine de l'acrobranche.
Je pense à Aurélia "je ne compte pas le nombre de fois ou j'ai baiser".
Qui reconnaît ne pas imaginer une vie sans sexe.
Qui reconnaît que la plus grande souffrance de son meilleur ami (Guillaume) atteind de la maladie des os de verre était de n'avoir jamais eu de relation avec une femme.
Mais qui dit aussi que ce n'est pas engageant de coucher avec un puceau.
Je pense à Emmanuelle "Comment tu fais sans baiser ?"
Je pense à ce garçon que j'ai rencontrer dans la résidence en Corée, qui après m'avoir demandé si j'avais déjà coucher avec une fille, me demande: "Est-ce que t'es un looser ?"
Je pense au masculinisme et aux incels.
Je pense à ma grand-mère qui avait refuser les avances des hommes après avoir quitter son mari. À ma mère qui lui disait de nous attendre à la maison pendant qu'on allait à la plage car elle était trop âgé pour traverser la dune et elle de répondre: "Et ben je vais me tuer". Et ma sœur sur le chemin: "Ah bah elle parle quand elle veut!".
Le mépris n'est rien si l'on ignore la souffrance dont il fait la négation.
N'y a t'il pas un rapport d'équivalence entre le mépris de la classe bourgeoise à l'égard des prolétaires, comme du féminisme à l'égard de ceux qui n'ont pas de relation sexuelle ?
Nous daignons offrir un peu d'attention à ceux qui souffre dés lors qu'ils ne représentent pas un danger, comme le transfert de classe n'est la volonté que de la classe la plus haute.
Mais dés que l'on devient, haineux, frustrer, pitoyable, risible; toute déconsidération pour la souffrance d'autrui devient justifier.
Pourtant je crois que nous devrions traiter les malades et les criminels avec le même égard.
Note jugement et nos émotions nous obligent à nous séparer d'eux, pourtant tout le monde doit être soigné.

À mettre cela en parallèle avec le dernier refus que j'ai essuyé, je me demande:
Le viol et ses conséquences doit’il nous obliger, les hommes, à nous humilier et à être humilié ?

On nous apprends que les hommes devraient apprendre à exprimer leurs émotions. Mais lorsque l'on parle des incels, on dit qu'ils cherchent à apitoyer.
Ils semblent que les expressions employer pour verbaliser un fait divergent en fonction de l'attention que l'on daigne accordé et non en fonction du dit fait.

Rêve 15/07/2024:
Une femme drogué avec un groupe d’ami au bord d’une falaise perd l’équilibre et tombe. En bas elle semble inconsciente enfoncé dans un trou. Elle bouge et à force de bouger la terre la recouvre. Elle fini par se relever au bout d’un moment et s’en va. Pourtant je me demande si elle est encore vivante.

En mathématique des symboles sont attribués à des formules / pattern pour simplifier l'écriture ou mettre en lumière des concepts.
Je suis convaicu que l'on peut faire de même avec des émotions ou des processus de communication ce qui permettrait de définir de façon algorithmique des situations ou micro-évènement.Car si toute situation semble particulière, elle n'est qu'un agencement de pattern commun et redondant à tout autre situation.

Aurélia:
- L'amour c'est la cerise sur le gateau
- Je ne peux pas accepter une relation sans sexe

Les politiques appellent à la responsabilité individuel, eux qui ont des capacités d'action plus importante que nous, les pauvres, et qui ne rendent de compte à personnes.
Et nous qui n'avons pas de capacité d'action individuelle et qui devont rendre des comptes du moindre de nos actes.
La responsabilité individuel nous deresponsabilise collectivement.

Je ne choisi pas les idées qui me traversent l'esprit.

Et si les plus infimes croyances qui passent par le vocabulaire sont sources de contradiction et que ces contradiction s'incarne dans la criminalité. S'il est plus facile de briser un atome qu'un préjugé. Nous comprenons mieux en quoi Tout homme est un criminel qui s'ignore.

# 2025

L'art, c'est la mise en équation de la matière.

Une défignition, c'est la mise en équation des mots.

L'art, c'est utilisé des moyens immanent pour mettre en lumière ce qui est transcendant.
L'immanent c'est ce qui est matériel et temporel.
Le transcendent, c'est ce qui est immatériel et intemporel.
Ce qui est transcendant, ce sont les mathématiques.

Une équation, c'est l'interdépendance des relation entre différents facteurs par l'intermédiaire d'opérateur.
Une peinture, c'est l'interdépendance des relations entre différents facteurs qui sont: le support, le médium, l'outil et le geste.

Des croyances symétrique entre les hommes et les femmes peuvent conduire à des comportements dysymétriques entre les hommes et les femmes.
exemple: L'homme doit faire le premier pas.

La violence, le sexe et la violence sexuelle.

