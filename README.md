When you first clone this config of neovim on your machine:
- Create a folder called: **plugged** in **autoload** folder
- Then launch **:PlugInstall** in Vim console.

For **Autoformat** plugin you need to install pynvim:
Neovim does not come with python support by default, and additional setup is required.
`python3 -m pip install pynvim`

For **Conquer Of Completion** you should need some extra:
`:CocInstall coc-json coc-tsserver coc-snippets coc-rust-analyzer coc-eslint`
nodejs > v14 needed

airline_powerline_fonts will work depending of terminal's police.
I use Hack font.
fonts could be install in : `~/.local/share/fonts/`
