" while insert mode, delete from cursor to the end of line
inoremap <C-k> <C-o>C

noremap <C-T>				:NERDTreeToggle<CR>
vnoremap <C-T>      <C-C>:NERDTreeToggle<CR>
inoremap <C-T>      <Esc>:NERDTreeToggle<CR>gi

map <C-c> :q!<CR>
" save
noremap <C-S>       :wa<CR>
vnoremap <C-S>      <C-C>:wa<CR><ESC>
inoremap <C-S>      <Esc>:wa<CR>gi<ESC>

" move line up and down
nnoremap <C-Up> :m .-2<CR>==
nnoremap <C-Down> :m .+1<CR>==
inoremap <C-Up> <Esc>:m .-2<CR>==gi
inoremap <C-Down> <Esc>:m .+1<CR>==gi
vnoremap <C-Up> :m '<-2<CR>gv=gv
vnoremap <C-Down> :m '>+1<CR>gv=gv

" window navigation
nnoremap <tab> <c-w><c-w>

" autocomplete when Enter key is pressed
inoremap <silent><expr> <cr> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"

" move tab
noremap <A-Left> :tabm -1<CR>
noremap <A-Right> :tabm +1<CR>

" copy the word under the cursor ( but don’t be at the first character )
noremap tt byw

" add text forward log variable
noremap ss bywi'<esc>pa',<space><esc><left><left><left>vb<S-u>

" remap ESC key
inoremap aa <esc>
vnoremap aa <esc>

" copy in clipboard
vnoremap cc "+y

" create tag
nmap ,, <C-y>,
imap ,, <C-y>,

nmap èè :Goyo<CR>
