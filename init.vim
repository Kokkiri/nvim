source ~/.config/nvim/vim-plug/plugins.vim
source ~/.config/nvim/mapping.vim

" add line number
set number

" cut line by words if too long
set linebreak

" mouse enabled
set mouse=a

" background-color
let g:sonokai_colors_override = {'bg0': ['#1e222a', '236']}
" color theme
colorscheme sonokai

" line number color
:hi LineNr ctermfg=darkyellow

" match parenthesis color
:hi MatchParen ctermbg=darkred ctermfg=darkyellow

" selection in visual mode
:hi Visual  ctermfg=yellow ctermbg=darkred
" match color html tag
" :set mps+=<:>

" top-bar and bottom-bar theme
let g:airline_theme='badwolf'

" quit tree when open file
let NERDTreeQuitOnOpen=1

" show hidden files by default
let NERDTreeShowHidden=1

" show bookmark by default
let NERDTreeShowBookmarks=1

" show current file name on top-bar but no buffers
let g:airline#extensions#tabline#enabled = 1					 " show tab with file name
let g:airline#extensions#tabline#show_buffers = 0			 " hide useless label closed files
let g:airline#extensions#tabline#show_tabs = 1
let g:airline#extensions#tabline#fnamemod = ':t'       " disable file paths in the tab
let g:airline#extensions#tabline#show_close_button = 0 " remove 'X' at the end of the tabline
let g:airline#extensions#tabline#show_tab_count = 0    " dont show tab numbers on the right

" let g:airline#extensions#tabline#show_tab_nr = 0       " disable tab numbers
" let g:airline#extensions#tabline#show_tab_type = 0     " disables the weird ornage arrow on the tabline
" let g:airline#extensions#tabline#tabs_label = ''       " can put text here like BUFFERS to denote buffers
" let g:airline#extensions#tabline#buffers_label = ''    " can put text here like TABS to denote tabs
" let g:airline#extensions#tabline#tab_min_count = 2     " minimum of 2 tabs needed to display the tabline
" let g:airline#extensions#tabline#show_splits = 0       " disables the buffer name that displays on the right of the tabline

" airline background transparent
highlight airline_c  ctermbg=NONE guibg=NONE
highlight airline_tabfill ctermbg=NONE guibg=NONE
" if you want to disable vim-airline
" let g:airline#extensions#tabline#show_splits = 0

" comments
let g:inline_comment_dict = {
			\'//': ["js", "ts", "cpp", "c", "dart", "rs", "jsx"],
			\'#': ['py', 'sh'],
			\'"': ['vim'],
			\}
let g:block_comment_dict = {
			\'/*': ["js", "ts", "cpp", "c", "dart", "css", "jsx"],
			\'"""': ['py'],
			\}

" more graphic airline
let g:airline_powerline_fonts = 1

" Use <tab> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<tab>'

" icons for NERDTree
highlight! link NERDTreeFlags NERDTreeDir
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1
let g:DevIconsEnableFolderExtensionPatternMatching = 1
let g:NERDTreeFileExtensionHighlightFullName = 1

" color for NERDTree
highlight NERDTreeCWD ctermfg=178
highlight CursorLine ctermbg=235

" function! NERDTreeHighlightFile(extension, fg, bg)
"    exec 'autocmd filetype nerdtree syn match NERDTreeTxtFile #^\s\+.*'. a:extension .'$#'
"    exec 'autocmd filetype nerdtree highlight NERDTreeTxtFile ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:bg .' guifg='. a:fg
" endfunction
"
" call NERDTreeHighlightFile('txt', 'blue', 'black')

" auto refresh
augroup DIRCHANGE
	au!
	autocmd DirChanged global :NERDTreeCWD
augroup END

let g:coc_global_extensions = [
			\ 'coc-json',
			\ 'coc-html',
			\ 'coc-css',
			\ 'coc-yaml',
			\ 'coc-angular'
			\ ]

" 			\ 'coc-tsserver',
" 			\ 'coc-rust-analyzer',
" 			\ 'coc-pairs'

let g:vimwiki_list = [{'path': '~/.config/nvim/vimwiki/',
			\ 'syntax': 'markdown', 'ext': '.md'}]

" goyo config
let g:goyo_width = 150
let g:goyo_linenr = 1

" indentation colorée
let g:indent_guides_auto_colors = 1
hi IndentGuidesEven  ctermbg=236
hi IndentGuidesOdd   ctermbg=234
" auienrst
" Linter config
let g:ale_fixers = {
			\ 'javascript': ['eslint']
			\ }
let g:ale_sign_error = '❌'
let g:ale_sign_warning = '⚠️'
let g:ale_fix_on_save = 1

" highlight code is markdown
let g:markdown_fenced_languages = ['html', 'javascript', 'json=javascript', 'js=javascript', 'jsx=javascript', 'python', 'bash=sh']

" color indentation
call togglerb#map("<F9>")
" let g:rainbow_colors_black= [ 234, 235, 236, 237, 238, 239 ]
let g:rainbow_colors_black= [ 196, 202, 208, 214, 220, 226 ]

:cnoreabbrev note VimwikiTabIndex
:cnoreabbrev vinit tabnew ~/.config/nvim/init.vim
:cnoreabbrev vplug tabnew ~/.config/nvim/vim-plug/plugins.vim
:cnoreabbrev vsnip tabnew ~/.config/nvim/snippets/
:cnoreabbrev vmapping tabnew ~/.config/nvim/mapping.vim
:cnoreabbrev G Goyo
:cnoreabbrev lime Limelight!!
:cnoreabbrev indent normal gg=G <BAR> <S-g>
:cnoreabbrev lint CocCommand eslint.executeAutofix
:cnoreabbrev nospace :%s/\s\+$//e
:cnoreabbrev tree AsciiTree
:cnoreabbrev notree AsciiTreeUndo
:cnoreabbrev reset source ~/.config/nvim/init.vim
:cnoreabbrev select %normal! ggVG"aty

