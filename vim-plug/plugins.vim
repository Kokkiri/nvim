" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " git command
"     Plug 'tpope/vim-fugitive'

    "
    Plug 'adi/vim-indent-rainbow'

    " ascii tree
    Plug 'riddlew/asciitree.nvim'

    " Pour créer des tableaux markdown
    Plug 'dhruvasagar/vim-table-mode'

    " highlight current paragraph
    Plug 'junegunn/limelight.vim'
    
    " indentation grey
"     Plug 'preservim/vim-indent-guides'
    
    " mode zen
    Plug 'junegunn/goyo.vim'

    " notebook
    Plug 'vimwiki/vimwiki'

    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'

    " File Explorer
    Plug 'scrooloose/NERDTree'

    
    Plug 'vim-airline/vim-airline'


    Plug 'vim-airline/vim-airline-themes'

    " color theme sonakai
    Plug 'sainnhe/sonokai'

    " emmet for html
    Plug 'mattn/emmet-vim'


    Plug 'KarimElghamry/vim-auto-comment'


    Plug 'neoclide/coc.nvim', {'branch': 'release'}

    " show git diff
    Plug 'airblade/vim-gitgutter'

    " code formator
    Plug 'vim-autoformat/vim-autoformat'

    " add icons
    Plug 'ryanoasis/vim-devicons'

call plug#end()
